<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'phone_number' => "07000000001",
                'first_name'=>"Super",
                'last_name'=>"Admin",
                'email_verified_at'=>Carbon::now(),
                'email'=>"pintoptechnologies@gmail.com",
                'password'=> Hash::make('1234567000@1'),
                'is_admin'=>true,
                'user_type'=>'admin'
            ]
        ];

        foreach ($data as $dat) {
            $user = User::create($dat);
            $user->assignRole(['super admin']);
        }
    }
}
