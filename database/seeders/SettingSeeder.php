<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'label'=>'load_connect_commission',
                'value'=>20,
                'description'=>'Load\'s connect commission before paying rider per order',
                'pre'=>'%'
            ],
            [
                'label'=>'full_capacity_load',
                'value'=>100,
                'description'=>'Rate at which price is presented for full load order',
                'pre'=>'%'
            ],
            [
                'label'=>'half_capacity_load',
                'value'=>67,
                'description'=>'Rate at which price is presented for full load order',
                'pre'=>'%'
            ]
        ];

        foreach ($data as $dat) {
            Setting::create($dat);
        }
    }
}
