<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin = Role::where('name', 'super admin')->first();
        $data = [
            ['name'=>'view dashboard'],
            ['name'=>'view dashboard logs'],
            ['name'=>'view account'],
            ['name'=>'view users'],
            ['name'=>'view dispatches'],
            ['name'=>'view customers'],
            ['name'=>'restrict user'],
            ['name'=>'view user data'],
            ['name'=>'update user'],
            ['name'=>'update wallet'],
            ['name'=>'create user'],
            ['name'=>'view notifications'],
            ['name'=>'send notifications'],
            ['name'=>'send sms'],
            ['name'=>'view coupons'],
            ['name'=>'create coupons'],
            ['name'=>'edit coupons'],
            ['name'=>'delete coupons'],
            ['name'=>'view transactions'],
            ['name'=>'view successful transactions'],
            ['name'=>'view pending transactions'],
            ['name'=>'view failed transactions'],
            ['name'=>'view payouts'],
            ['name'=>'view approved payouts'],
            ['name'=>'view pending payouts'],
            ['name'=>'view declined payouts'],
            ['name'=>'process payouts'],
            ['name'=>'view vehicles'],
            ['name'=>'approve vehicles'],
            ['name'=>'blacklist vehicles'],
            ['name'=>'view available vehicles'],
            ['name'=>'view unavailable vehicles'],
            ['name'=>'view blacklisted vehicles'],
            ['name'=>'create vehicle types'],
            ['name'=>'edit vehicle types'],
            ['name'=>'delete vehicle types'],
            ['name'=>'view ratings'],
            ['name'=>'view complaints'],
            ['name'=>'delete ratings'],
            ['name'=>'view orders'],
            ['name'=>'view accepted orders'],
            ['name'=>'view pending orders'],
            ['name'=>'view cancelled orders'],
            ['name'=>'view scheduled orders'],
            ['name'=>'view delivered orders'],
            ['name'=>'view picked orders'],
            ['name'=>'view picking orders'],
            ['name'=>'view failed orders'],
            ['name'=>'view administration'],
            ['name'=>'view administrators'],
            ['name'=>'create administrators'],
            ['name'=>'edit administrators'],
            ['name'=>'delete administrators'],
            ['name'=>'view roles'],
            ['name'=>'create roles'],
            ['name'=>'edit roles'],
            ['name'=>'delete roles'],
            ['name'=>'view settings'],
            ['name'=>'edit settings'],
        ];

        foreach ($data as $dat) {
        	$perm = Permission::create($dat);
            $superAdmin->givePermissionTo($perm->name);
        }
    }
}
