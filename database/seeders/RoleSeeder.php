<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$data = [
			['name'=>'super admin'],
			['name'=>'user'],
			['name'=>'rider'],
		];


        foreach($data as $info){
        	Role::create($info);
        }
    }
}
