<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name');
            $table->string('status')->default('pending');
            $table->longText('notes')->nullable();
            $table->json('details')->nullable();
            $table->boolean('is_available')->default(false);
            $table->boolean('is_blacklisted')->default(false);
            $table->double('current_latitude')->nullable();
            $table->double('current_longitude')->nullable();
            $table->text('current_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
