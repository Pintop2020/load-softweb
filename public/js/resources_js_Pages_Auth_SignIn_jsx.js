"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Auth_SignIn_jsx"],{

/***/ "./resources/js/Layouts/AuthLayout.jsx":
/*!*********************************************!*\
  !*** ./resources/js/Layouts/AuthLayout.jsx ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Reuseable_app_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Reuseable/app_config */ "./resources/js/Reuseable/app_config.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








var AuthLayout = function AuthLayout(_ref) {
  var title = _ref.title,
      page = _ref.page,
      children = _ref.children;
  var appName = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.usePage)().props.appName;
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    document.body.classList.add("bg-body");
    document.body.setAttribute("id", "kt_body");
  }, []);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.Head, {
      title: [title, appName].join(" > ")
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
      className: "d-flex flex-column flex-root",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed",
        style: "background-image: url(/metronic8/demo13/assets/media/illustrations/unitedpalms-1/14.png",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
          className: "d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("a", {
            href: "/metronic8/demo13/../demo13/index.html",
            className: "mb-12",
            children: [" ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("img", {
              alt: "Logo",
              src: "/metronic8/demo13/assets/media/logos/logo-1.svg",
              className: "h-40px"
            }), " "]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
            className: "w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("form", {
              className: "form w-100",
              novalidate: "novalidate",
              id: "kt_sign_in_form",
              action: "#",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                className: "text-center mb-10",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("h1", {
                  className: "text-dark mb-3",
                  children: "Sign In to Metronic"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                  className: "text-gray-400 fw-bold fs-4",
                  children: ["New Here? ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("a", {
                    href: "/metronic8/demo13/../demo13/authentication/layouts/basic/sign-up.html",
                    className: "link-primary fw-bolder",
                    children: "Create an Account"
                  })]
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                className: "fv-row mb-10",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
                  className: "form-label fs-6 fw-bolder text-dark",
                  children: "Email"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("input", {
                  className: "form-control form-control-lg form-control-solid",
                  type: "text",
                  name: "email",
                  autocomplete: "off"
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                className: "fv-row mb-10",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                  className: "d-flex flex-stack mb-2",
                  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
                    className: "form-label fw-bolder text-dark fs-6 mb-0",
                    children: "Password"
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("a", {
                    href: "/metronic8/demo13/../demo13/authentication/layouts/basic/password-reset.html",
                    className: "link-primary fs-6 fw-bolder",
                    children: "Forgot Password ?"
                  })]
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("input", {
                  className: "form-control form-control-lg form-control-solid",
                  type: "password",
                  name: "password",
                  autocomplete: "off"
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                className: "text-center",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("button", {
                  type: "submit",
                  id: "kt_sign_in_submit",
                  className: "btn btn-lg btn-primary w-100 mb-5",
                  children: [" ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("span", {
                    className: "indicator-label",
                    children: "Continue"
                  }), " ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("span", {
                    className: "indicator-progress",
                    children: ["Please wait...", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("span", {
                      className: "spinner-border spinner-border-sm align-middle ms-2"
                    })]
                  })]
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
                  className: "text-center text-muted text-uppercase fw-bolder mb-5",
                  children: "or"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("a", {
                  href: "#",
                  className: "btn btn-flex flex-center btn-light btn-lg w-100 mb-5",
                  children: [" ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("img", {
                    alt: "Logo",
                    src: "/metronic8/demo13/assets/media/svg/brand-logos/google-icon.svg",
                    className: "h-20px me-3"
                  }), "Continue with Google"]
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("a", {
                  href: "#",
                  className: "btn btn-flex flex-center btn-light btn-lg w-100 mb-5",
                  children: [" ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("img", {
                    alt: "Logo",
                    src: "/metronic8/demo13/assets/media/svg/brand-logos/facebook-4.svg",
                    className: "h-20px me-3"
                  }), "Continue with Facebook"]
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("a", {
                  href: "#",
                  className: "btn btn-flex flex-center btn-light btn-lg w-100",
                  children: [" ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("img", {
                    alt: "Logo",
                    src: "/metronic8/demo13/assets/media/svg/brand-logos/apple-black.svg",
                    className: "h-20px me-3"
                  }), "Continue with Apple"]
                })]
              })]
            })
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
          className: "d-flex flex-center flex-column-auto p-10",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "d-flex align-items-center fw-bold fs-6",
            children: [" ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("a", {
              href: "https://keenthemes.com",
              className: "text-muted text-hover-primary px-2",
              children: "About"
            }), " ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("a", {
              href: "mailto:support@keenthemes.com",
              className: "text-muted text-hover-primary px-2",
              children: "Contact"
            }), " ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("a", {
              href: "https://1.envato.market/EA4JP",
              className: "text-muted text-hover-primary px-2",
              children: "Contact Us"
            }), " "]
          })
        })]
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (AuthLayout);

/***/ }),

/***/ "./resources/js/Layouts/Layouts.jsx":
/*!******************************************!*\
  !*** ./resources/js/Layouts/Layouts.jsx ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthLayout": () => (/* reexport safe */ _AuthLayout__WEBPACK_IMPORTED_MODULE_0__["default"])
/* harmony export */ });
/* harmony import */ var _AuthLayout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./AuthLayout */ "./resources/js/Layouts/AuthLayout.jsx");



/***/ }),

/***/ "./resources/js/Pages/Auth/SignIn.jsx":
/*!********************************************!*\
  !*** ./resources/js/Pages/Auth/SignIn.jsx ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Layouts_Layouts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../Layouts/Layouts */ "./resources/js/Layouts/Layouts.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }


 // import { Form } from 'react-bootstrap'




var Login = function Login(_ref) {
  var canResetPassword = _ref.canResetPassword;

  var _useForm = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.useForm)({
    email: '',
    password: '',
    remember: false
  }),
      data = _useForm.data,
      setData = _useForm.setData,
      post = _useForm.post,
      processing = _useForm.processing,
      errors = _useForm.errors;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)({
    email: null,
    password: null
  }),
      _useState2 = _slicedToArray(_useState, 2),
      signinErrors = _useState2[0],
      setSigninErrors = _useState2[1];

  var validateInput = function validateInput() {
    var validMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var errorsInit = {};

    var fields = _objectSpread({}, data);

    for (var key in fields) {
      if (!fields[key] && key != 'remember') {
        errorsInit[key] = "This field is required";
      }

      if (fields.email && !fields.email.match(validMail)) {
        errorsInit.email = "Please enter a valid email address";
      }

      if (fields.password && fields.password.length < 8) {
        errorsInit.password = "Password must be at least 8 characters";
      }
    }

    setSigninErrors(errorsInit);

    if (Object.entries(errorsInit).length === 0) {
      return true;
    } else {
      return false;
    }
  };

  var handleChange = function handleChange(e) {
    var target = e.currentTarget;
    var value = target.type === 'checkbox' ? target.checked : target.value;
    setData(target.name, value);
    setSigninErrors(target.name, null);
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    var validated = validateInput();

    if (validated) {
      post('login');
    }
  };

  var handleSubmitWithKeyPress = function handleSubmitWithKeyPress(e) {
    if (e.key.toLowerCase() === "enter" || e.code.toLowerCase() === "enter") {
      handleSubmit();
    }
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Layouts_Layouts__WEBPACK_IMPORTED_MODULE_2__.AuthLayout, {
    title: 'Sign In',
    page: "Login to get a loan now!"
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Login);

/***/ }),

/***/ "./resources/js/Reuseable/app_config.jsx":
/*!***********************************************!*\
  !*** ./resources/js/Reuseable/app_config.jsx ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "logoFile": () => (/* reexport safe */ _assets_media_logos_logo_1_svg__WEBPACK_IMPORTED_MODULE_0__["default"]),
/* harmony export */   "checkPermission": () => (/* binding */ checkPermission),
/* harmony export */   "site": () => (/* binding */ site),
/* harmony export */   "mainSite": () => (/* binding */ mainSite)
/* harmony export */ });
/* harmony import */ var _assets_media_logos_logo_1_svg__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../assets/media/logos/logo-1.svg */ "./resources/assets/media/logos/logo-1.svg");


var checkPermission = function checkPermission(user, checking) {
  var permissions = user.roles[0].permissions;
  var response = false;

  if (user.roles[0].name == "super admin") {
    response = true;
  } else {
    for (var i = 0; i < permissions.length; i++) {
      if (permissions[i].name == checking) {
        response = true;
        break;
      }
    }
  }

  return response;
};

var site = "https://load.test";
var mainSite = "https://load.test";


/***/ }),

/***/ "./resources/assets/media/logos/logo-1.svg":
/*!*************************************************!*\
  !*** ./resources/assets/media/logos/logo-1.svg ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("/images/logo-1.svg?dd5c713c15df3d37957df7130c9622ae");

/***/ })

}]);