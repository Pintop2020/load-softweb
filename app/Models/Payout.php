<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Payout extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = [
        'uid', 'more',
    ];

    protected $hidden = [
        'details'
    ];

    public function getMoreAttribute()
    {
        if(isset($this->details) && $this->details != null) return json_decode($this->details);
        return null;
    }

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'payout_transactions')->withTimestamps();
    }
}
