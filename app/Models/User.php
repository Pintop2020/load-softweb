<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Traits\HasRoles;
use App\Notifications\GeneralNotification;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use HasTeams;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
        'details'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url', 'more', 'uid', 'full_ratings'
    ];

    public function getMoreAttribute()
    {
        if(isset($this->details) && $this->details != null) return json_decode($this->details);
        return null;
    }

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }

    public function driver_ratings()
    {
        return $this->belongsToMany(Rating::class, 'driver_rated')->withTimestamps();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function verification_tokens()
    {
        return $this->hasMany(VerificationToken::class);
    }

    public function payouts()
    {
        return $this->hasMany(Payout::class);
    }

    public function rides()
    {
        return $this->belongsToMany(Order::class, 'order_vehicles')->withTimestamps();
    }

    public function negotiated_prices()
    {
        return $this->belongsToMany(OrderPrice::class, 'negotiated_price_user')->withTimestamps();
    }

    public function complaints()
    {
        return $this->hasMany(Complaint::class);
    }

    public function favorite_trucks()
    {
        return $this->belongsToMany(Vehicle::class, 'favorite_vehicles')->withTimestamps();
    }

    public function tokenize($type='all')
    {
        $code = \random_int(100000, 999999);
        $this->verification_tokens()->save(new VerificationToken(['token'=>$code, 'type'=>$type]));
    }

    public function getFullRatingsAttribute()
    {
        $reviewT = 0;
        if($this->user_type == 'dispatch'){
            $five = $this->driver_ratings()->where('ratings', 5)->count();
            $four = $this->driver_ratings()->where('ratings', 4)->count();
            $three = $this->driver_ratings()->where('ratings', 3)->count();
            $two = $this->driver_ratings()->where('ratings', 2)->count();
            $one = $this->driver_ratings()->where('ratings', 1)->count();
            if(($five+$four+$three+$two+$one) > 0){
                $reviewT += round(((5*$five) + (4*$four) + (3*$three) + (2*$two) + (1*$one))/($five+$four+$three+$two+$one),1);
            }
        }else if($this->user_type == 'user'){
            $five = $this->ratings()->where('ratings', 5)->count();
            $four = $this->ratings()->where('ratings', 4)->count();
            $three = $this->ratings()->where('ratings', 3)->count();
            $two = $this->ratings()->where('ratings', 2)->count();
            $one = $this->ratings()->where('ratings', 1)->count();
            if(($five+$four+$three+$two+$one) > 0){
                $reviewT += round(((5*$five) + (4*$four) + (3*$three) + (2*$two) + (1*$one))/($five+$four+$three+$two+$one),1);
            }
        }
        return $reviewT;
    }

    public static function boot() {
        parent::boot();
        self::created(function($user) {
            if(!$user->is_admin){
                $user->wallet()->save(new Wallet(['amount'=>0.0]));
                if($user->user_type == 'user'){
                    $user->notify(new GeneralNotification("Welcome to ".getenv('APP_NAME').".The rest of welcome messages continue.", 'Account Registration', $user, 'register'));
                    $user->assignRole(['user']);
                }else if($user->user_type == 'dispatch'){
                    $user->notify(new GeneralNotification("Welcome to ".getenv('APP_NAME').".The rest of welcome messages continue.", 'Account Registration', $user, 'register'));
                    $user->assignRole(['rider']);
                }
                // $user->tokenize("register");
            }
        });
    }

}
