<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Rating extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = [
        'uid',
    ];

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function driver_rated()
    {
        return $this->belongsToMany(User::class, 'driver_rated')->withTimestamps();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_ratings')->withTimestamps();
    }
}
