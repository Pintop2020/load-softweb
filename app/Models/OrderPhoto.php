<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class OrderPhoto extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = [
        'uid',
    ];

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
