<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'selected_vehicle_types')->withTimestamps();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_vehicle_types')->withTimestamps();
    }
}
