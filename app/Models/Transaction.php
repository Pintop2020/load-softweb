<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;

class Transaction extends Model
{
    use HasFactory;
    use PivotEventTrait;

    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'details'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'more', 'uid',
    ];

    public function getMoreAttribute()
    {
        if(isset($this->details) && $this->details != null) return json_decode($this->details);
        return null;
    }

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cards()
    {
        return $this->belongsToMany(Card::class, 'transaction_cards')->withTimestamps();
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_transactions')->withTimestamps();
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'coupon_transactions')->withTimestamps()->withPivot('amount_discounted');
    }

    public function payouts()
    {
        return $this->belongsToMany(Payout::class, 'payout_transactions')->withTimestamps();
    }

    public static function boot() {
        parent::boot();
        static::pivotAttached(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            if($relationName == "orders"){
                $order = $model->orders()->first();
                $settings = Setting::where('label', 'load_connect_commission')->first();
                if($order){
                    $vehicle = $order->vehicles()->first();
                    $rider = $vehicle->user;
                    $orig_amount = $model->more->amount;
                    $amount = $orig_amount - ($orig_amount*($settings->value/100));
                    $rider->wallet->update(['amount'=>DB::raw('amount + '.$amount)]);
                }
            }
        });
    }
}
