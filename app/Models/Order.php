<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Order extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $hidden = [
        'details'
    ];

    protected $appends = [
        'more', 'uid',
    ];

    public function getMoreAttribute()
    {
        if(isset($this->details) && $this->details != null) return json_decode($this->details);
        return null;
    }

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function vehicle_types()
    {
        return $this->belongsToMany(VehicleType::class, 'order_vehicle_types')->withTimestamps();
    }

    public function setDetailsAttribute($value) {
        $this->attributes['details'] = json_encode($value);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function photos()
    {
        return $this->hasMany(OrderPhoto::class);
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'order_transactions')->withTimestamps();
    }

    public function ratings()
    {
        return $this->belongsToMany(Rating::class, 'order_ratings')->withTimestamps();
    }

    public function vehicles()
    {
        return $this->belongsToMany(Vehicle::class, 'order_vehicles')->withTimestamps()->withPivot('time_picked', 'time_delivered', 'time_arrived', 'time_cancelled', 'reason', 'user_id');
    }

    public function riders()
    {
        return $this->belongsToMany(User::class, 'order_vehicles');
    }

    public function rejected_orders()
    {
        return $this->belongsToMany(Vehicle::class, 'rejected_orders')->withTimestamps()->withPivot('by_who');
    }

    public function prices()
    {
        return $this->hasMany(OrderPrice::class);
    }
}
