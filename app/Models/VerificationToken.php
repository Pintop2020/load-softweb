<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Notifications\VerificationTokenNotification;

class VerificationToken extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function boot() {
        parent::boot();
        self::created(function($token) {
            $user = $token->user;
            $user->notify(new VerificationTokenNotification($user, $token));
        });
    }
}
