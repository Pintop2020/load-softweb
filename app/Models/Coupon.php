<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

class Coupon extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = [
        'uid',
    ];

    public function getUidAttribute()
    {
        return Crypt::encryptString($this->id);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->belongsToMany(Transaction::class, 'coupon_transactions')->withTimestamps()->withPivot('amount_discounted');
    }
}
