<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;
use Illuminate\Pagination\Paginator;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Route;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Paginator::useBootstrap();
        Fortify::loginView(function () {
            return Inertia::render('Auth/SignIn', [
                'canResetPassword' => Route::has('password.request'),
                'status' => session('status'),
                'message' => session('message'),
            ]);
        });
        Fortify::requestPasswordResetLinkView(function () {
            return Inertia::render('Auth/ForgotPassword', [
                'status'=>session('status')
            ]);
        });
        Fortify::registerView(function () {
            return Inertia::render('Errors/404');
        });
        Fortify::verifyEmailView(function () {
            return Inertia::render('Errors/404');
        });
    }
}
