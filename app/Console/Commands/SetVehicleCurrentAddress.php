<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Vehicle;
use App\Http\Controllers\GlobalMethods as SuperM;

class SetVehicleCurrentAddress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vehicle:current_address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $vehicles = Vehicle::where('is_available', true)->where('current_longitude', '!=', null)->where('current_latitude', '!=', null)->get();
        foreach($vehicles as $vehicle){
            $address = SuperM::getAddress($vehicle->current_latitude, $vehicle->current_longitude);
            $vehicle->update(['current_address'=>$address]);
        }
    }
}
