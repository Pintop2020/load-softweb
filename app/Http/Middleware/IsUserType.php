<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsUserType
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->user_type != 'user') {
            return response()->json([
                'code' => 201,
                'message' => 'Only customers allowed is allowed to access this page!',
                'status' => false,
                'data'=>null
            ], 201);
        }
        return $next($request);
    }
}
