<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsComplete
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->more == null) {
            return response()->json([
                'code' => 201,
                'message' => 'Please complete your profile to continue',
                'status' => false,
                'data'=>null
            ], 201);
        }
        return $next($request);
    }
}
