<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::user()->is_admin) {
            if(Auth::user()->is_active){
                return $next($request);
            }
            $request->session()->invalidate();
            $request->session()->flush();
            $request->session()->regenerateToken();
            auth('web')->logout();
            return response()->json([
                'code' => 201,
                'message' => 'You have been restricted from our platform. Please contact our customer support centre!',
                'status' => false,
                'data'=>null
            ], 201);
        }
        return $next($request);
    }
}
