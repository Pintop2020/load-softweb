<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check()) {
            if (Auth::user()->is_admin) {
                if(Auth::user()->is_active){
                    return $next($request);
                }
                $request->session()->invalidate();
                $request->session()->flush();
                $request->session()->regenerateToken();
                return redirect('login')->with('message', 'You have been restricted from our administratove platform.<br> Please contact your CO!');
            }
            $request->session()->invalidate();
            $request->session()->flush();
            $request->session()->regenerateToken();
            return redirect('login');
        }
        return redirect()->route('login');
    }
}
