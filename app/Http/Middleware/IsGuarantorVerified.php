<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsGuarantorVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->more != null && Auth::user()->more->is_guarantor_confirmed) {
            return $next($request);
        }
        return response()->json([
            'code' => 201,
            'message' => 'Please wait while we verify the details provided',
            'status' => false,
            'data'=>null
        ], 201);
    }
}
