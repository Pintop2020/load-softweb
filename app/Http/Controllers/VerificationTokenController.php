<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VerificationToken;
use App\Traits\AppResponse;

class VerificationTokenController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new VerificationToken();
    }

    public function verify(Request $request)
    {
        $req = $request->only(['user_id', 'token']);
        $myT = $this->model->where($req)->first();
        $user = $this->model->find($request->user_id);
        if(!$myT) {
            return $this->error("Invalid Token");
        }else {
            $user = $myT->user;
            if($myT->type == 'register') {
                $user->update(['email_verified_at'=>now()]);
                $myT->delete();
                return $this->success('Account verification successful', $user);
            }
            // else if($myT->type == "2fa"){
            //     if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            //         $user = Auth::user();
            //         $myT->delete();
            //         $token = $user->createToken($user->name."_token_".time());
            //         $data['token'] =  $token->plainTextToken;
            //         $data['user'] = $user;
            //         if($user->is_admin){
            //             $data['user']['roles'] = $user->roles()->with('permissions')->first();
            //         }
            //         return $this->success('Account authentication successful', $data);
            //     }
            // }
            elseif($myT->type == 'reset'){
                return $this->success('Account verification successful', $user);
            }
        }
    }

    public function store(Request $request)
    {
        $data = $request->only(['user_id', 'type']);
        $data['token'] = \random_int(100000, 999999);
        $this->model->create($data);
        return $this->success("New authorization token created");
    }
}
