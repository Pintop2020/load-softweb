<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rating;
use App\Models\Order;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Traits\AppResponse;

class RatingController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new Rating();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entries'] = $this->model->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'driver_rated', 'orders')->get();
        return Inertia::render('Rating/Index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $order = Order::find($request->order_id);
        if($order->status != 'delivered'){
            return $this->error("You can't rate this rider yet.");
        }
        $rider = $order->vehicles()->first()->user;
        $rating = Rating::create(['user_id'=>$user->id, 'ratings'=>$request->ratings, 'comments'=>$request->comments]);
        $rating->orders()->attach($order->id);
        $rating->driver_rated()->attach($rider->id);
        return $this->success("Your ratings has been sent.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = $this->model->find($id);
        $entry->delete();
        return redirect()->back()->with('success', 'Ratings removed');
    }
}
