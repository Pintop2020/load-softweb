<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VehicleType;
use App\Traits\AppResponse;
use Inertia\Inertia;

class VehicleTypeController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new VehicleType();
    }

    public function index()
    {
        if(request()->wantsJson()){
            $data = $this->model->with('vehicles.user')->get();
            return $this->success("Entries retrieved", $data);
        }else {
            $data['entries'] = $this->model->withCount('vehicles')->get();
            return Inertia::render('VehicleType/Index', $data);
        }
    }

    public function create()
    {
        return Inertia::render('VehicleType/Create');
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $entry = $this->model->create($data);
        return redirect()->route('vehicle_types.show', $entry->id)->with('success', 'You successfully added a new truck type');
    }

    public function show($id)
    {
        $data['entry'] = $this->model->with('vehicles')->findOrFail($id);
        return Inertia::render('VehicleType/Show', $data);
    }

    public function edit($id)
    {
        $data['entry'] = $this->model->findOrFail($id);
        return Inertia::render('VehicleType/Edit', $data);
    }

    public function update(Request $request, $id)
    {
        $entry = $this->model->findOrFail($id);
        $entry->update(['name'=>$request->name, 'icon'=>$request->icon]);
        return redirect()->route('vehicle_types.show', $entry->id)->with('success', 'Truck type updated successfully');
    }

    public function destroy($id)
    {
        $entry = $this->model->findOrFail($id);
        $vehicles = $entry->vehicles()->count();
        if($vehicles > 0){
            return redirect()->back()->with('error', 'This vehicle has an active order.');
        }
        $entry->delete();
        return redirect('/vehicle_types')->with('success', 'Truck types deleted successfully');
    }
}
