<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\User;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Validator;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Hash;
use App\Traits\FileUploadManager;
use App\Rules\MatchOldPassword;

class UserController extends Controller
{
    use AppResponse, FileUploadManager;

    protected $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function single($type)
    {
        if($type == 'user'){
            $data['title'] = "Customers";
            $data['entries'] = $this->model->where('is_admin', false)->where('user_type', 'user')->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('wallet')->get();
        }else if($type == 'dispatch'){
            $data['title'] = "Dispatch";
            $data['entries'] = $this->model->where('is_admin', false)->where('user_type', 'dispatch')->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('wallet', 'vehicles')->get();
        }else return redirect()->back()->with('error', 'Unknown server request');
        return Inertia::render('User/Index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email', 'string', 'unique:users', 'max:255'],
            'phone_number' => ['required', 'string', 'unique:users', 'max:11'],
            'password' => ['required', 'string', 'min:8'],
            'user_type'=>['required']
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }
        $regData = $request->except(['password']);
        $regData['password'] = Hash::make($request->password);
        $user = $this->model->create($regData);
        return $this->success('Account created sucessfully', $user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = $this->model->with('wallet', 'cards', 'coupons.transactions', 'driver_ratings.user', 'orders.vehicles', 'orders.transactions', 'orders.vehicle_types', 'ratings', 'transactions.orders', 'transactions.payouts', 'transactions.coupons', 'vehicles.vehicle_types', 'rides.user', 'rides.vehicle_types', 'payouts', 'complaints.vehicles')->withCount('ratings')->withCount('driver_ratings')->findOrFail($id);
        $data['entry'] = $user;
        $data['transactions'] = $user->transactions()->where('details->status', 'success')->sum('details->amount');
        $data['payouts'] = $user->payouts()->where('status', 'approved')->sum('details->amount');
        if($user->user_type == 'dispatch') $data['vehicles'] = Vehicle::doesntHave('user')->get();
        return Inertia::render('User/Show', $data);
    }

    public function reset(Request $request){
        $user = $this->model->findOrFail($request->user_id);
        $token = $user->verification_tokens()->where('token', $request->token)->where('type', 'forget')->first();
        if($token){
            $user->update(['password'=>Hash::make($request->password)]);
            $token->delete();
            return $this->success("Password changed successfully!");
        }
        return $this->error("Invalid token provided");
    }

    public function forget(Request $request)
    {
        $user = $this->model->where(['email'=>$request->email])->first();
        if(!$user) return $this->error("These credentials do not match our records.");
        $user->tokenize("forget");
        return $this->success("Authentication code sent to your email address.", $user);
    }

    public function login(Request $request)
    {
        $entry = $this->model->where('email', $request->user_id)->orWhere('phone_number', $request->user_id)->first();
        if($entry){
            if(Auth::attempt(['email' => $entry->email, 'password' => $request->password])){
                $user = Auth::user();
               if($request->firebase_token) $user->update(['firebase_token'=>$request->firebase_token]);
                if($user->is_admin){
                    Auth::logout();
                    return $this->error('You cannot access the mobile application with a staff account!');
                }else {
                    if($user->is_active){
                        if($user->email_verified_at != null){
                            $token = $user->createToken($user->name."_token_".time());
                            $data['token'] =  $token->plainTextToken;
                            $data['user'] = $user;
                            return $this->success('Account authentication successful', $data);
                        }else{
                            return $this->error('unverified', ['user'=>$user]);
                        }
                    }else {
                        Auth::logout();
                        return $this->error('Your account has been restricted. Please contact support for assistance.');
                    }
                }
            } else {
                return $this->error('These credentials do not match our records.');
            }
        }
        return $this->error("These credentials do not match our records.");
    }

    public function profile()
    {
        try {
            $user = Auth::user();
            $data = [
                'user'=>$user->user_type == 'dispatch' ? $this->model->where('id', $user->id)->with(['wallet', 'driver_ratings.user', 'driver_ratings.orders.user', 'driver_ratings.orders.photos', 'driver_ratings.orders.prices', 'transactions.payouts', 'vehicles.vehicle_types', 'vehicles.orders.user', 'vehicles.orders.photos', 'vehicles.orders.prices', 'vehicles.orders.transactions.coupons', 'vehicles.orders.ratings.user', 'vehicles.rejected_orders.photos', 'payouts', 'rides.user', 'rides.photos', 'rides.prices', 'rides.transactions.coupons'])->first() : $this->model->where('id', $user->id)->with(['wallet', 'cards.transactions.orders.user', 'cards.transactions.coupons', 'coupons.transactions.orders.user', 'orders.photos', 'orders.transactions.coupons', 'orders.ratings', 'orders.vehicles.user', 'orders.prices', 'orders.rejected_orders.user', 'ratings.orders.user', 'ratings.driver_rated', 'transactions.coupons', 'transactions.cards', 'transactions.orders.vehicles.user', 'payouts', 'complaints.vehicles.user', 'favorite_trucks'])->first(),
                'total_transaction'=>$user->transactions()->sum('details->amount'),
                'unread_notifications'=>$user->unreadNotifications,
                'notifications'=>$user->notifications
            ];
            return $this->success('Entries retrieved successfully', $data);
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }
    }

    public function completeRider(Request $request)
    {
        $user = Auth::user();
        $data['details'] = $request->except('passport', 'driver_license', 'guarantor_passport');
        $data['details']['is_guarantor_confirmed'] = false;
        $data['details']['driver_license'] = $this->uploadSingle($request->driver_license, 'driver-licenses');
        $data['details']['guarantor_passport'] = $this->uploadSingle($request->guarantor_passport, 'guarantor-licenses');
        $data['profile_photo_path'] = $this->uploadSingle($request->passport, 'profile-photos');
        $user->update($data);
        return $this->success('Account updated successfully.');
    }

    public function completeUser(Request $request)
    {
        $user = Auth::user();
        $data['details'] = $request->except('passport');
        if($request->passport && $request->hasFile('passport')){
            $data['profile_photo_path'] = $this->uploadSingle($request->passport, 'profile-photos');
        }
        $user->update($data);
        return $this->success('Account updated successfully.');
    }

    public function logout()
    {
        $user = Auth::user();
        $user->tokens->each(function($item){
            $item->delete();
        });
        return $this->success("Access rovoked");
    }

    public function perm($id)
    {
        $user = $this->model->find($id);
        $user->is_active = !$user->is_active;
        $user->tokens->each(function($item){
            $item->delete();
        });
        $user->save();
        return redirect()->back()->with('success', "You have updated {$user->first_name}'s status!");
    }

    public function addVehicle(Request $request)
    {
        $user = $this->model->findOrFail($request->entry_id);
        $vehicle = Vehicle::findOrFail($request->vehicle_id);
        if($user->details == null) return redirect()->back()->with('error', 'Account registration not complete yet.');
        if(!$user->more->is_guarantor_confirmed) return redirect()->back()->with('error', 'Guarantor is not confirmed yet.');
        if($user->vehicle) $user->vehicle->update(['user_id'=>null]);
        $activeOrders = $vehicle->orders()->where('status', 'accepted')->orWhere('status', 'picked')->count();
        if($activeOrders > 0) return redirect()->back()->with('error', 'There is an active order on this vehicle. Please try later');
        $user->vehicle()->save($vehicle);
        $user->vehicle_histories()->attach($vehicle->id);
        return redirect()->back()->with('success', 'Rider updated successfully.');
    }

    public function confirmGuarantor($id)
    {
        $user = $this->model->find($id);
        if($user->more){
            $user->update(['details->is_guarantor_confirmed'=>true]);
            return redirect()->back()->with('success', 'Rider updated successfully');
        }
    }

    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);
        if ($validator->fails()) {
            return $this->error($validator->errors());
        }
        $this->model->find($user->id)->update(['password'=> Hash::make($request->new_password)]);
        return $this->success("Your password has been change successfully", $user);
    }

    public function editProfile(Request $request)
    {
        $user = Auth::user();
        $data = [
            'first_name'=>$request->first_name ?? $user->first_name,
            'last_name'=>$request->last_name ?? $user->last_name,
            'details'=> [
                'address'=>$request->address ?? '',
                'state'=>$request->state ?? ''
            ]
        ];
        $user->update($data);
        if($request->passport && $request->hasFile('passport')){
            $file = $this->uploadSingle($request->passport, 'profile-photos');
            $user->update(['profile_photo_path'=>$file]);
        }
        return $this->success("Account updated successfully", $user);
    }
}
