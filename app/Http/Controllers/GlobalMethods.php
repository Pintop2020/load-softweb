<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

class GlobalMethods extends Controller
{


    public static function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $unit = 'kilometers')
    {
        $theta = $longitudeFrom - $longitudeTo;
        $distance = (sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo))) + (cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;
        switch($unit) {
          case 'miles':
            break;
          case 'kilometers' :
            $distance = $distance * 1.609344;
        }
        return (round($distance,2));
    }

    public static function getAddress($latitude, $longitude)
    {
        $key = getenv('GOOGLE_MAP_API_KEY');
        $url = "https://maps.google.com/maps/api/geocode/json?latlng={$latitude},{$longitude}&key={$key}";
        $geocode = file_get_contents($url);
        $json = json_decode($geocode);
        $address = $json->results[0]->formatted_address;
        return $address;
    }

    public function testing(){
        $data = self::paystackGet("https://api.paystack.co/transaction/verify/3rch6tadc7");
        dd($data);
    }

    public static function sendSms($message, $number){
        //
    }

    public static function sendPush($user, $title, $message)
    {
        if($user->firebase_token != null){
            $url = "https://fcm.googleapis.com/fcm/send";
            $serverKey = 'AAAA1uCISTk:APA91bFRYsd7uo6Dcf7dqEER9ILzrVArUOY7eAqibiRRncKOGHkOraMNP2n2-Kdt0keTxTi_0jiK-qUf_A6TZRXC2N3dByIPPTVaUPSa1gmBpQJVi2RqtQGtrbDiJWK0k_Ob79UUSEht';
            $notification = array('title'=>$title, 'body'=>$message, 'sound'=>'default', 'badge'=>'1');
            $arrayToSend = array('to' => $user->firebase_token, 'notification'=>$notification, 'data'=>$notification,'priority'=>'high');
            $json = json_encode($arrayToSend);
            $headers = array();
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Authorization: key='. $serverKey;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
            curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            $response = curl_exec($ch);
        }
    }

    public static function paystackGet($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
            "authorization: Bearer ".getenv('PAYSTACK_SECRET_KEY'),
            "content-type: application/json",
            "cache-control: no-cache",
            ],
        ));
        $response = curl_exec($curl);
        return json_decode($response, true);
    }

    public static function paystackPost($url, $param){
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($param),
        CURLOPT_HTTPHEADER => [
            "authorization: Bearer ".getenv('PAYSTACK_SECRET_KEY'),
            "content-type: application/json",
            "cache-control: no-cache",
            ],
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $tranx = json_decode($response,true);
        return $tranx;
    }

    public static function transCharge($amount, $isAddFees=true){
        $final = 0;
        if($isAddFees){
            if($amount >= 2500){
                $final += ($amount+100)/(1-0.015);
            }else {
                $final += $amount/(1-0.015);
            }
        }else {
            $final += $amount;
        }
        // $final = $final*100;
        return round($final);
    }

    public static function randomToken($table, $column, $length=8){
        $id = str_random($length);
        $validator = \Validator::make([$column=>$id],[$column=>'unique:'.$table]);
        if($validator->fails()){
            return self::randomToken($table, $column, $length);
        }
        return $id;
    }

    public static function getEstimatedPrice($order, $vehicle)
    {
        $full = Setting::where('label', 'full_capacity_load')->first();
        $half = Setting::where('label', 'half_capacity_load')->first();
        if($order->more->capacity == 'full'){
            $amount = $vehicle->more->capacity_per_kilometre_price * ($full->value/100);
            $price = $amount * $order->more->distance;
            $order->update(['details->price'=>$price]);
            if(!isset($order->more->original_price)) $order->update(['details->original_price'=>$price]);
        }else {
            $amount = $vehicle->more->capacity_per_kilometre_price * ($half->value/100);
            $price = $amount * $order->more->distance;
            $order->update(['details->price'=>$price]);
            if(!isset($order->more->original_price)) $order->update(['details->original_price'=>$price]);
        }
    }

    public static function getEstimatedPriceEstimate($order)
    {
        $full = Setting::where('label', 'full_capacity_load')->first();
        $half = Setting::where('label', 'half_capacity_load')->first();
        $type = $order->vehicle_types()->first();
        if($order->more->capacity == 'full'){
            $amount = $type->estimated_capacity_per_kilometre_price * ($full->value/100);
            $price = $amount * $order->more->distance;
            $order->update(['details->estimated_price'=>$price]);
        }else {
            $amount = $type->estimated_capacity_per_kilometre_price * ($half->value/100);
            $price = $amount * $order->more->distance;
            $order->update(['details->estimated_price'=>$price]);
        }
    }
}
