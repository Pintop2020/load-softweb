<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Inertia\Inertia;

class TransactionController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Transaction();
    }

    public function single($type)
    {
        $data['title'] = ucwords($type);
        $data['entries'] = $this->model->where('details->status', $type)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'orders', 'coupons', 'payouts')->get();
        return Inertia::render('Transaction/Index', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entry = $this->model->with('user', 'cards', 'orders.vehicles.user', 'coupons', 'payouts')->findOrFail($id);
        $data['entry'] = $entry;
        $data['title'] = $entry->more->status == 'success' ? "Successful" : ucwords($entry->more->status);
        return Inertia::render('Transaction/Show', $data);
    }
}
