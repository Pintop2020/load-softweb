<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\AppResponse;
use App\Traits\FileUploadManager;
use App\Http\Controllers\GlobalMethods as SuperM;

class CardController extends Controller
{
    use AppResponse;
    use FileUploadManager;

    protected $model;

    public function __construct()
    {
        $this->model = new Card();
    }

    public function payNow(Request $request)
    {
        $user = Auth::user();
        $card = $user->cards()->findOrFail($request->card_id);
        $order = $user->orders()->findOrFail($request->order_id);
        $amount = $order->more->price;
        $coupon_id = 0;
        if($request->use_coupon){
            $coupon = $user->coupons()->where('status', 'pending')->first();
            if($coupon){
                if($coupon->type == 'amount') $amount -= $coupon->amount;
                else $amount -= ($amount*($coupon->amount/100));
                $coupon->update(['status'=>'used']);
                $coupon_id += $coupon->id;
            }
        }
        $initiation_order = [
            'amount'=>SuperM::transCharge($amount, true)*100,
            'email'=>$user->email,
            'currency'=>'NGN',
            'authorization_code'=>$card->more->authorization_code,
            'metadata'=>[
                'use_coupon'=>$request->use_coupon,
                'coupon_id'=>$coupon_id
            ]
        ];
        if($request->save_card) $initiation_order['metadata']['save_card'] = true;
        $response = SuperM::paystackPost("https://api.paystack.co/transaction/charge_authorization", $initiation_order);
        if($response['status']){
            $data = [
                'amount'=>$response['data']['amount']/100,
                'type'=>'debit',
                'description'=>'Payment for order '.$order->name,
                'payment_option'=>$response['data']['channel'],
                'reference'=>"LINC_".$response['data']['reference'],
                'status'=>'success',
                'verification'=>true,
                'others'=>[
                    'fee'=>$response['data']['fees']/100,
                ]
            ];
            $transaction = Transaction::create(['details'=>$data, 'user_id'=>$user->id]);
            $transaction->orders()->attach($order->id);
            $transaction->cards()->attach($card->id);
            if(isset($response['data']['metadata']['use_coupon']) && isset($response['data']['metadata']['coupon_id']) && $response['data']['metadata']['use_coupon'] == "true" && $response['data']['metadata']['coupon_id']*1 > 0){
                $coupon = $user->coupons()->find($response['data']['metadata']['coupon_id']*1);
                if($coupon)
                $coupon->transactions()->attach($transaction->id, array('amount_discounted'=>$order->more->price-$transaction->more->amount));
            }
            return $this->success("Order payment received and confirmed", $order);
        }
        return $this->error($response['message']);
    }
}
