<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use App\Traits\AppResponse;
use Inertia\Inertia;

class SettingController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new Setting();
    }

    public function index()
    {
        if(request()->wantsJson()){
            $data = $this->model->all();
            return $this->success("Configuration retrieved", $data);
        }else {
            $data['entries'] = $this->model->all();
            return Inertia::render('Settings/Index', $data);
        }
    }

    public function update(Request $request)
    {
        $entry = $this->model->findOrFail($request->entry_id);
        $entry->update(['value'=>$request->value]);
        return redirect()->back()->with('success', 'Settings updated');
    }
}
