<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;
use App\Http\Controllers\GlobalMethods as SuperM;

class SmsController extends Controller
{
    public function __invoke()
    {
        $data['users'] = User::where('is_admin', false)->get();
        return Inertia::render("Sms/Send", $data);
    }

    public function sendSms(Request $request)
    {
        $number = '';
        if($request->send_all){
            $users = User::where('is_admin', false)->get();
            foreach($users as $key => $user){
                $number .= $user->phone_number;
                if(isset($users[$key+1]) && $users[$key] != null){
                    $number .= ',';
                }
            }
        }
        for($i=0;$i<count($request->users);$i++){
            $number .= $request->users[$i];
            if(isset($request->users[$i+1]) && $request->users[$i] != null){
                $number .= ',';
            }
        }
        SuperM::sendSms($request->message, $number);
        return redirect()->back()->with('success', 'Messages sent successfully!');
    }

    public function sendSmsSingle(Request $request)
    {
        SuperM::sendSms($request->message, $request->user);
        return redirect()->back()->with('success', 'Messages sent successfully!');
    }
}
