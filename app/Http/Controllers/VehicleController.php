<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\Setting;
use App\Models\Order;
use App\Traits\AppResponse;
use App\Traits\FileUploadManager;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class VehicleController extends Controller
{
    use AppResponse;
    use FileUploadManager;

    protected $model;

    public function __construct()
    {
        $this->model = new Vehicle();
    }

    public function single($type)
    {
        $data['title'] = ucwords($type);
        if($type == 'available'){
            $data['entries'] = $this->model->where('is_available', true)->where('is_blacklisted', false)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'vehicle_types')->get();
        }else if($type == 'unavailable'){
            $data['entries'] = $this->model->where('is_available', false)->where('is_blacklisted', false)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'vehicle_types')->get();
        }else if($type == 'blacklisted'){
            $data['entries'] = $this->model->where('is_blacklisted', true)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'vehicle_types')->get();
        }else return redirect()->back()->with('error', 'Unknown server request');
        return Inertia::render('Vehicle/Index', $data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->except('color', 'vehicle_type_id', 'capacity_per_kilometre_price', 'front_shot', 'back_shot', 'side_shot', 'interior_shot', 'plate_number', 'documents');
        $data['user_id'] = $user->id;
        $data['is_available'] = false;
        $data['details']['color'] = $request->color;
        $data['details']['plate_number'] = $request->plate_number;
        $data['details']['capacity_per_kilometre_price'] = $request->capacity_per_kilometre_price*1;
        $data['details']['front_shot'] = $this->uploadSingle($request->front_shot, 'vehicle-photos');
        $data['details']['back_shot'] = $this->uploadSingle($request->back_shot, 'vehicle-photos');
        $data['details']['side_shot'] = $this->uploadSingle($request->side_shot, 'vehicle-photos');
        $data['details']['interior_shot'] = $this->uploadSingle($request->interior_shot, 'vehicle-photos');
        $data['details']['documents'] = $this->uploadSingle($request->documents, 'vehicle-photos');
        $entry = $this->model->create($data);
        $entry->vehicle_types()->attach($request->vehicle_type_id);
        return $this->success("You successfully added a new vehicle", $entry);
    }

    public function show($id)
    {
        $data['entry'] = $this->model->with('user', 'orders.user', 'orders.vehicle_types', 'rejected_orders.user', 'rejected_orders.vehicle_types', 'vehicle_types', 'complaints.vehicles', 'complaints.user')->findOrFail($id);
        return Inertia::render('Vehicle/Show', $data);
    }

    public function update(Request $request, $id)
    {
        try {
            $entry = $this->model->findOrFail($id);
            $data = $request->except('vehicle_type_id');
            $entry->update(['details'=>$data]);
            return $this->success('Vehicle updated successfully', $entry);
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {
            $entry = $this->model->findOrFail($id);
            $orders = $entry->orders()->count();
            if($orders > 0){
                return $this->error('This truck has orders');
            }
            $entry->delete();
            return $this->success('Truck deleted successfully');
        }catch(\Exception $e){
            return $this->error($e->getMessage());
        }
    }

    public function getClosestAvailableOrder()
    {
        $user = Auth::user();
        $vehicles = $user->vehicles()->where('status', 'approved')->where('is_blacklisted', false)->where('is_available', true)->get();
        $data = array();
        foreach($vehicles as $vehicle){
            $type = $vehicle->vehicle_types()->first();
            $orders = Order::where('status', 'pending')->whereHas('vehicle_types', function($query) use($type){
                $query->where('vehicle_types.id', $type->id);
            })->get();
            foreach($orders as $order){
                $data[] = [
                    'order_id'=>$order->id,
                    'vehicle_id'=>$vehicle->id,
                    'distance'=>SuperM::vincentyGreatCircleDistance($order->more->pickup_latitude, $order->more->pickup_longitude, $vehicle->current_latitude, $vehicle->current_longitude),
                    'distance_in'=>'km'
                ];
            }
        }
        if(count($data) > 0){
            $collection = collect($data)->sortBy('distance', SORT_REGULAR);
            $sorted = $collection->first();
            $dataRetrieved = [
                'order'=>Order::find($sorted['order_id']),
                'distance'=>$sorted['distance'],
                'distance_in'=>'km',
                'truck'=>Vehicle::find($sorted['vehicle_id'])
            ];
            return $this->success("Order found", $dataRetrieved);
        }else {
            return $this->error("No available order at the moment");
        }
    }

    public function setLocation(Request $request)
    {
        $user = Auth::user();
        $vehicle = $user->vehicles()->find($request->vehicle_id);
        if($vehicle != null && $vehicle->is_available) {
            $vehicle->update(['current_latitude'=>$request->current_latitude, 'current_longitude'=>$request->current_longitude]);
            return $this->success('Location updated');
        }
        return $this->error("Vehicle not available at the moment");
    }

    public function acceptOrder(Request $request)
    {
        $vehicle = $this->model->findOrFail($request->vehicle_id);
        $user = $vehicle->user;
        $order = Order::findOrFail($request->order_id);
        if($order->status == 'pending'){
            SuperM::getEstimatedPrice($order, $vehicle);
            $order->update(['status'=>'accepted']);
            $order->vehicles()->attach($vehicle->id, array('user_id'=>$user->id));
            return $this->success("Order accepted by rider.", ['order'=>$order, 'vehicle'=>$vehicle]);
        }
        return $this->error("Order already accepted");
    }

    public function rejectOrder(Request $request)
    {
        $vehicle = $this->model->find($request->vehicle_id);
        $order = Order::find($request->order_id);
        $order->update(['status'=>'pending']);
        $order->vehicles()->detach($vehicle->id);
        $order->rejected_orders()->attach($vehicle->id);
        return $this->success("Order rejected successfully");
    }

    public function setAvailability(Request $request)
    {
        $entry = $this->model->find($request->vehicle_id);
        if($entry->status == 'approved'){
            $entry->update(['is_available'=>$request->available]);
            return $this->success("Status updated successfully");
        }elseif($entry->status == 'rejected') return $this->error("This truck does not pass our checks.");
        return $this->error("This truck is currently under review.");
    }

    public function validateRide(Request $request)
    {
        $entry = $this->model->find($request->entry_id);
        $entry->status = $request->status;
        if($request->status == 'rejected'){
            $entry->notes = $request->notes;
        }
        $entry->save();
        return redirect()->back()->with('success', 'Truck updated successfully');
    }

    public function toggleBlacklist($action, $id)
    {
        $entry = $this->model->find($id);
        if($action == 'add'){
            $entry->update(['is_blacklisted'=>true]);
        }elseif($action == 'remove'){
            $entry->update(['is_blacklisted'=>false]);
        }else return redirect()->back()->with('errors', 'Unidentified action');
        return redirect()->back()->with('success', 'Vehicle updated successfully');
    }

    public function toggleFavorites($action, $id)
    {
        $user = Auth::user();
        $entry = $this->model->findOrFail($id);
        if($action == 'add'){
            $user->favorite_trucks()->attach($entry->id);
        }else $user->favorite_trucks()->detach($entry->id);
        return $this->success("Your favorite list has been updated successfully");
    }

    public function getClosestAvailableTrucks(Request $request)
    {
        $user = Auth::user();
        $lat = $request->latitude;
        $long = $request->longitude;
        $vehicles = $user->vehicles()->where('status', 'approved')->where('is_blacklisted', false)->where('is_available', true)->get();
        $data = array();
        foreach($vehicles as $vehicle){
            $data[] = [
                'vehicle'=>$vehicle,
                'vehicle_type'=>$vehicle->vehicle_types()->first(),
                'distance'=>SuperM::vincentyGreatCircleDistance($lat, $long, $vehicle->current_latitude, $vehicle->current_longitude),
                'distance_in'=>'km'
            ];
        }
        if(count($data) > 0){
            $collection = collect($data)->sortBy('distance', SORT_REGULAR);
            return $this->success("Order found", $collection);
        }else {
            return $this->error("No available vehicles at the moment");
        }
    }

}
