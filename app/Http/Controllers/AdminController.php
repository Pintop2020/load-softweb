<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
Use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Traits\FileUploadManager;

class AdminController extends Controller
{
    use FileUploadManager;

    public $model;

    public function __construct()
    {
        $this->model = new User();
    }
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $data['entries'] = $this->model->where('id', '>', 1)->where('is_admin', true)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('roles')->get();
        return Inertia::render('Admin/Index', $data);
    }

    public function create()
    {
        $data['roles'] = Role::latest()->get();
        return Inertia::render('Admin/Create', $data);
    }

    public function show($id)
    {
        $entry = $this->model->with('roles')->find($id);
        $data['entry'] = $entry;
        return Inertia::render('Admin/Show', $data);
    }

    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'phone_number' => ['required', 'string', 'max:11', 'min:11', 'unique:users'],
        ]);
        $password = \Hash::make($request->first_name.date('Y'));
        $data = [
            'first_name'=>$request->first_name,
            'last_name'=>$request->last_name,
            'email'=>$request->email,
            'email_verified_at'=>Carbon::now(),
            'password'=>$password,
            'phone_number'=>$request->phone_number,
            'is_admin'=>true,
            'is_active'=>true,
        ];
        $roles = Role::find($request->role);
        $user = $this->model->create($data);
        $user->syncRoles($roles);
        return redirect('/admins')->with('success', 'New admin has been saved successfully!');
    }

    public function edit($id)
    {
        $data['roles'] = Role::latest()->get();
        $data['entry'] = $this->model->with('roles')->find($id);
        return Inertia::render('Admin/Edit', $data);
    }

    public function update(Request $request, $id)
    {
        $user = $this->model->find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();
        $role = Role::find($request->role);
        $user->syncRoles($role);
        return redirect('/admins')->with('success', 'Admin has been updated successfully!');
    }

    public function profile()
    {
        return Inertia::render('Admin/Profile');
    }

    public function profileUpdate(Request $request)
    {
        $user = Auth::user();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        if($request->passport && $request->hasFile('passport') && $request->passport != null) $user->profile_photo_path = $this->uploadSingle($request->passport, 'profile-photos');
        $user->save();
        return redirect()->back()->with('success', 'Account updated successfully.');
    }

}
