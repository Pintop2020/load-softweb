<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Notifications\GeneralNotification;
use App\Http\Controllers\GlobalMethods as SuperM;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Illuminate\Support\Facades\Notification;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users'] = User::where('is_admin', false)->where('details', '!=', null)->latest()->get();
        $data['entries'] = $this->getNotifications(request()->sortBy ?? 'asc', request()->show ?? 50);
        $data['read'] = DB::table('notifications')->where('read_at', '!=', null)->where('data->is_admin', true)->latest()->count();
        $data['unread'] = DB::table('notifications')->where('read_at', null)->where('data->is_admin', true)->latest()->count();
        $data['all'] = DB::table('notifications')->where('data->is_admin', true)->latest()->count();
        return Inertia::render('Notification/Index', $data);
    }

    private function getNotifications($sortBy, $show)
    {
        $entries = DB::table('notifications')->where('data->is_admin', true)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->get();
        $data = array();
        foreach ($entries as $entry){
            $entry->user = User::find($entry->notifiable_id)->toArray();
            $data[] = $entry;
        }
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->send_all){
            $users = User::where('is_admin', false)->latest()->get();
            foreach($users as $user){
                Notification::send($user, new GeneralNotification($request->message, $request->subject, $user, 'direct', true));
                SuperM::sendPush($user, $request->subject, $request->message);
            }
        }else {
            $users = User::find($request->users);
            foreach($users as $user){
                Notification::send($user, new GeneralNotification($request->message, $request->subject, $user, 'direct', true));
                SuperM::sendPush($user, $request->subject, $request->message);
            }
        }
        return redirect()->back()->with('success', 'Notification sent');
    }

    public function storeSingle(Request $request)
    {
        $user  = User::find($request->user);
        Notification::send($user, new GeneralNotification($request->message, $request->subject, $user, 'direct', true));
        SuperM::sendPush($user, $request->subject, $request->message);
        return redirect()->back()->with('success', 'Notification sent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $notifications = $user->notifications()->where('id', $id)->first();
        if($notifications->read_at == null) $notifications->update(['read_at'=>now()]);
        $data['entry'] = $notifications;
        return Inertia::render('Notification/Show', $data);
    }
}
