<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payout;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\GlobalMethods as SuperM;

class PayoutController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new Payout();
    }

    public function single($type)
    {
        $data['title'] = ucwords($type);
        $data['entries'] = $this->model->where('status', $type)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user')->get();
        return Inertia::render('Payout/Index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->except('_token');
        $wallet = $user->wallet;
        $amount = $request->amount;
        $payouts = $user->payouts()->count();
        if($payouts < 1){
            if($amount <= $wallet->amount){
                $wallet->update(['amount'=>DB::raw('amount - '.$amount)]);
                $user->payouts()->save(new Payout(['details'=>$data]));
                return $this->success("Your withdrawal has been queued and will be attended to shortly");
            }
            return $this->error("You don't have sufficient funds to perform this transaction.");
        }
        return $this->error("You have a pending payout. Please wait until it has been processed before retrying.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['entry'] = $this->model->with('user', 'transactions')->find($id);
        return Inertia::render('Payout/Show', $data);
    }

    public function processPayout(Request $request)
    {
        $entry = $this->model->find($request->payout);
        $user = $entry->user;
        $action = $request->type;
        $wallet = $user->wallet;
        if($action == 'approve'){
            $data = [
                'amount'=>$entry->more->amount,
                'type'=>'debit',
                'description'=>$request->note,
                'payment_option'=>'transfer',
                'reference'=>"LPRT_".SuperM::randomToken('transactions', 'details->reference'),
                'status'=>'success',
                'verification'=>true,
                'others'=>[
                    'fee'=>0,
                ]
            ];
            $transaction = Transaction::create(['details'=>$data, 'user_id'=>$user->id]);
            $entry->transactions()->attach($transaction->id);
            $entry->update(['status'=>'approved']);
        }elseif($action == 'decline') {
            $wallet->update(['amount'=>DB::raw('amount + '.$entry->more->amount)]);
            $data = [
                'amount'=>$entry->more->amount,
                'type'=>'debit',
                'description'=>$request->note,
                'payment_option'=>'transfer',
                'reference'=>"LPRT_".SuperM::randomToken('transactions', 'details->reference'),
                'status'=>'failed',
                'verification'=>true,
                'others'=>[
                    'fee'=>0,
                ]
            ];
            $transaction = Transaction::create(['details'=>$data, 'user_id'=>$user->id]);
            $entry->transactions()->attach($transaction->id);
            $entry->update(['status'=>'declined']);
        }
        return redirect()->back()->with('success', 'Payout has been updated successfully');
    }
}
