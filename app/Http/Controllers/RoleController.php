<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Inertia\Inertia;

class RoleController extends Controller
{
    public $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function index()
    {
        $data['entries'] = $this->model->where('name', '!=', 'super admin')->where('name', '!=', 'user')->where('name', '!=', 'rider')->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('permissions')->get();
        return Inertia::render('Role/Index', $data);
    }

    public function create()
    {
        $data['permissions'] = Permission::all();
        return Inertia::render('Role/Create', $data);
    }

    public function store(Request $request)
    {
        try{
            $data = $request->only('name');
            $permissions = $request->permissions;
            $role = $this->model->create($data);
            $role->syncPermissions($permissions);
            return redirect()->route('roles.index')->with('success', 'You have successfully added a role.');
        }catch(\Exception $e){
            return redirect()->back()->flash('error', $e->getMessage());
        }
    }

    public function edit($id)
    {
        $data['permissions'] = Permission::all();
        $data['entry'] = $this->model->with('permissions')->find($id);
        return Inertia::render('Role/Edit', $data);
    }

    public function update(Request $request, $id)
    {
        try{
            $role = $this->model->find($id);
            $data = $request->only('name');
            $permissions = $request->permissions;
            $role->update($data);
            $role->syncPermissions($permissions);
            return redirect()->route('roles.index')->with('success', 'You have successfully updated a role.');
        }catch(\Exception $e){
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $role = $this->model->find($id);
        if($role->users->count() > 0){
            return redirect()->back()->with('error', 'Role has active users.');
        }else {
            $role->delete();
            return redirect()->route('roles.index')->with('success', 'Role deleted successfully.');
        }
    }
}
