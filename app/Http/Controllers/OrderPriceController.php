<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\OrderPrice;
use App\Models\Vehicle;
use App\Models\Order;
use App\Traits\AppResponse;

class OrderPriceController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new OrderPrice();
    }

    public function negotiate(Request $request)
    {
        $user = Auth::user();
        $vehicle = Vehicle::find($request->vehicle_id);
        $order = Order::find($request->order_id);
        $entry = OrderPrice::create(['amount'=>$request->amount_can_afford, 'order_id'=>$order->id]);
        $entry->vehicles()->attach($vehicle->id);
        $entry->users()->attach($user->id);
        if($user->user_type == 'user') return $this->success('New price sent to dispatch.', $entry);
        return $this->success('New price sent to customer.', $entry);
    }

    public function acceptPrice($id)
    {
        $entry = $this->model->find($id);
        $order = $entry->order;
        $order->update(['details->price'=>$entry->amount]);
        $entry->update(['agreed'=>true]);
        return $this->success("Price agreement reached between dispatch and customer");
    }
}
