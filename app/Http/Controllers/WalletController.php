<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Traits\AppResponse;

class WalletController extends Controller
{
    use AppResponse;

    public function updateWallet(Request $request)
    {
        $user = User::findOrFail($request->user);
        $wallet = $user->wallet;
        $amount = $request->amount*1;
        if($request->type == 'credit'){
            $wallet->update(['amount'=>DB::raw('amount + '.$amount)]);
        }else {
            $wallet->update(['amount'=>DB::raw('amount - '.$amount)]);
        }
        $data = [
            'amount'=>$amount,
            'type'=>$request->type,
            'description'=>$request->note,
            'payment_option'=>'cash',
            'reference'=>$request->reference,
            'status'=>'success',
            'verification'=>true,
            'others'=>[
                'fee'=>0,
            ]
        ];
        $user->transactions()->save(new Transaction(['details'=>$data]));
        return redirect()->back()->with('success', 'Wallet updated successfully');
    }

    public function payNow(Request $request)
    {
        $user = Auth::user();
        $order = $user->orders()->findOrFail($request->order_id);
        $amount = $order->more->price;
        $coupon_id = 0;
        $wallet = $user->wallet;
        if($request->use_coupon){
            $coupon = $user->coupons()->where('status', 'pending')->first();
            if($coupon){
                if($coupon->type == 'amount') $amount -= $coupon->amount;
                else $amount -= ($amount*($coupon->amount/100));
                $coupon->update(['status'=>'used']);
                $coupon_id += $coupon->id;
            }
        }
        if($amount > $wallet->amount) return $this->error("You don't have sufficient funds for this transaction.");
        $wallet->update(['amount'=>DB::raw('amount - '.$amount)]);
        $data = [
            'amount'=>$amount,
            'type'=>'debit',
            'description'=>'Payment for order '.$order->name,
            'payment_option'=>'wallet',
            'reference'=>"LIDW_".SuperM::randomToken('transactions', 'details->reference'),
            'status'=>'success',
            'verification'=>true,
            'others'=>[
                'fee'=>0,
            ]
        ];
        $transaction = Transaction::create(['details'=>$data, 'user_id'=>$user->id]);
        $transaction->orders()->attach($order->id);
        if($request->use_coupon){
            $coupon = $user->coupons()->find($coupon_id);
            if($coupon)
            $coupon->transactions()->attach($transaction->id, array('amount_discounted'=>$order->more->price-$transaction->more->amount));
        }
        return $this->success("Order payment received and confirmed", $order);
    }
}
