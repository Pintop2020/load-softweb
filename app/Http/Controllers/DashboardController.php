<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Inertia\Inertia;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Order;
use App\Models\Payout;
use App\Models\Transaction;
use App\Models\Vehicle;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $data['wallet'] = Wallet::sum('amount');
        $data['transactions'] = Transaction::where('details->status', 'success')->sum('details->amount');
        $data['payouts'] = Payout::where('status', 'approved')->sum('details->amount');
        $all_orders = Order::count();
        $pending_orders = Order::where('status', 'pending')->count();
        $delivered_orders = Order::where('status', 'delivered')->count();
        $in_progress = Order::where('status', 'accepted')->orWhere('status', 'arrived')->orWhere('status', 'picked')->count();
        $cancelled_orders = Order::where('status', 'cancelled')->count();
        $data['orders'] = [
            'all'=>$all_orders,
            'pending'=>$pending_orders > 0 ? ($pending_orders/$all_orders)*100 : 0,
            'in_progress'=>$in_progress > 0 ? ($in_progress/$all_orders)*100 : 0,
            'delivered'=>$delivered_orders > 0 ? ($delivered_orders/$all_orders)*100 : 0,
            'cancelled'=>$cancelled_orders > 0 ? ($cancelled_orders/$all_orders)*100 : 0,
        ];
        $all_trucks = Vehicle::count();
        $blacklisted = Vehicle::where('is_blacklisted', true)->count();
        $available = Vehicle::where('is_blacklisted', false)->where('is_available', true)->count();
        $unavailable = Vehicle::where('is_blacklisted', false)->where('is_available', false)->count();
        $data['trucks'] = [
            'all'=>$all_trucks,
            'blacklisted'=>$blacklisted > 0 ? ($blacklisted/$all_trucks)*100 : 0,
            'unavailable'=>$unavailable > 0 ? ($unavailable/$all_trucks)*100 : 0,
            'available'=>$available > 0 ? ($available/$all_trucks)*100 : 0,
        ];
        $data['users'] = [
            'all'=>User::where('is_admin', false)->where('user_type', 'user')->count(),
            'recents'=>User::where('is_admin', false)->where('user_type', 'user')->latest()->take(7)->get()
        ];
        $data['riders'] = [
            'all'=>User::where('is_admin', false)->where('user_type', 'dispatch')->count(),
            'recents'=>User::where('is_admin', false)->where('user_type', 'dispatch')->latest()->take(7)->get()
        ];
        $data['histories'] = Order::latest()->take(5)->with('vehicle_types', 'user')->get();
        for($i=1;$i<13;$i++){
            $data['month_label'][] = Carbon::createFromFormat('!m', $i)->monthName;
            $data['payouts_repo'][] = Payout::where('details->status', 'approved')->whereMonth('created_at', '=', Carbon::createFromFormat('!m', $i)->month)->whereYear('created_at', '=', date('Y'))->sum('details->amount');
            $data['transaction_repo'][] = Transaction::where('details->status', 'success')->whereMonth('created_at', '=', Carbon::createFromFormat('!m', $i)->month)->whereYear('created_at', '=', date('Y'))->sum('details->amount');
        }
        return Inertia::render('Dashboard/Dashboard', $data);
    }
}
