<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Vehicle;
use App\Models\Transaction;
use App\Models\Card;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\OrderPhoto;
use App\Traits\AppResponse;
use App\Traits\FileUploadManager;
use App\Http\Controllers\GlobalMethods as SuperM;

class OrderController extends Controller
{
    use AppResponse;
    use FileUploadManager;

    protected $model;

    public function __construct()
    {
        $this->model = new Order();
    }

    public function single($type)
    {
        $data['title'] = ucwords($type);
        $data['entries'] = $this->model->where('status', $type)->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'transactions', 'vehicle_types')->get();
        return Inertia::render('Order/Index', $data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $distance = SuperM::vincentyGreatCircleDistance($request->pickup_latitude, $request->pickup_longitude, $request->destination_latitude, $request->destination_longitude);
        $data = $request->except('images', 'vehicle_type_id');
        $data['distance'] = $distance;
        $order = $this->model->create(['details'=>$data]);
        $user->orders()->save($order);
        foreach($request->images as $image){
           $order->photos()->save(new OrderPhoto(['image_path'=>$this->uploadSingle($image, 'order-photos')]));
        }
        $order->vehicle_types()->attach($request->vehicle_type_id);
        SuperM::getEstimatedPriceEstimate($order);
        return $this->success("Order booked successfully", $order);
    }

    public function show($id)
    {
        $entry = $this->model->with('ratings', 'user', 'transactions', 'photos', 'vehicles', 'vehicle_types', 'rejected_orders.user', 'riders', 'prices.users', 'prices.vehicles', 'rejected_orders.vehicle_types')->findOrFail($id)->toArray();
        $data['entry'] = $entry;
        return Inertia::render('Order/Show', $data);
    }

    public function update(Request $request, $id)
    {
        $entry = $this->model->with('vehicles.user')->findOrFail($id);
        if(request()->wantsJson()){
            if($request->status){
                $vehicle = $entry->vehicles()->first();
                $status = strtolower($request->status);
                if($entry->status != 'delivered'){
                    if($status == 'picked'){
                        if($vehicle->pivot->time_arrived != null){
                            $entry->update(['status'=>$status]);
                            $entry->vehicles()->updateExistingPivot($vehicle->id, array('time_picked'=>now()));
                            return $this->success('Order Updated successfully', $entry);
                        }
                        return $this->error("You never arrived at this location.");
                    }elseif($status == 'cancelled'){
                        $entry->update(['status'=>$status]);
                        $entry->vehicles()->updateExistingPivot($vehicle->id, array('time_cancelled'=>now()));
                        return $this->success("Order cancelled successfully", $entry);
                    }elseif($status == 'delivered'){
                        if($entry->transactions()->count() > 0){
                            if($vehicle->pivot->time_picked != null){
                                $entry->update(['status'=>$status]);
                                $entry->vehicles()->updateExistingPivot($vehicle->id, array('time_delivered'=>now()));
                                return $this->success("Order delivered successfully", $entry);
                            }
                            return $this->error("You never picked this item up.");
                        }
                        return $this->error("You can't deliver an order without payment confirmation.");
                    }elseif($status == 'arrived'){
                        $entry->update(['status'=>$status]);
                        $entry->vehicles()->updateExistingPivot($vehicle->id, array('time_arrived'=>now()));
                        return $this->success('Order Updated successfully', $entry);
                    }elseif($status == 'failed'){
                        $entry->update(['status'=>$status]);
                        $entry->vehicles()->updateExistingPivot($vehicle->id, array('reason'=>$request->reason));
                        return $this->success('Order Updated successfully', $entry);
                    }
                    return $this->error("Unknown request");
                }
                return $this->error("This order has been completely processed");
            }elseif($request->capacity){
                if($entry->status == 'accepted'){
                    $entry->update(['details->capacity'=>$request->capacity]);
                    SuperM::getEstimatedPrice($entry, $entry->vehicles()->first());
                    return $this->success("Order updated successfully", $entry);
                }
                return $this->error("You can not send this request unless you have accepted the order.");
            }
            return $this->error("Unknown request");
        }
    }

    public function payNow(Request $request)
    {
        $user = Auth::user();
        $order = $user->orders()->findOrFail($request->order_id);
        $vehicle = $order->vehicles()->first();
        $amount = $order->more->price;
        $coupon_id = 0;
        if($request->use_coupon){
            $coupon = $user->coupons()->where('status', 'pending')->first();
            if($coupon){
                if($coupon->type == 'amount') $amount -= $coupon->amount;
                else $amount -= ($amount*($coupon->amount/100));
                $coupon->update(['status'=>'used']);
                $coupon_id += $coupon->id;
            }
        }
        $initiation_order = [
            'amount'=>SuperM::transCharge($amount, true)*100,
            'email'=>$user->email,
            'currency'=>'NGN',
            'metadata'=>[
                'order_id'=>$order->id,
                'vehicle_id'=>$vehicle->id,
                'rider_id'=>$vehicle->user->id,
                'use_coupon'=>$request->use_coupon,
                'coupon_id'=>$coupon_id
            ]
        ];
        if($request->save_card) $initiation_order['metadata']['save_card'] = true;
        $response = SuperM::paystackPost("https://api.paystack.co/transaction/initialize", $initiation_order);
        if($response['status']){
            return $this->success($response['message'], $response['data']);
        }
        return $this->error($response['message']);
    }

    public function validateOrder(Request $request)
    {
        $user = Auth::user();
        $order = $user->orders()->findOrFail($request->order_id);
        $response = SuperM::paystackGet("https://api.paystack.co/transaction/verify/$request->reference");
        if($response['status']){
            $data = [
                'amount'=>$response['data']['amount']/100,
                'type'=>'debit',
                'description'=>'Payment for order '.$order->name,
                'payment_option'=>$response['data']['channel'],
                'reference'=>"LIRC_".$request->reference,
                'status'=>'success',
                'verification'=>true,
                'others'=>[
                    'fee'=>$response['data']['fees']/100,
                ]
            ];
            $transaction = Transaction::create(['details'=>$data, 'user_id'=>$user->id]);
            $transaction->orders()->attach($order->id);
            if(isset($response['data']['metadata']['save_card']) && $response['data']['metadata']['save_card'] == "true"){
                $authorization = $response['data']['authorization'];
                $count = Card::where(['details->bin'=>$authorization['bin'], 'details->last4'=>$authorization['last4'], 'details->exp_month'=>$authorization['exp_month'], 'details->exp_year'=>$authorization['exp_year']])->count();
                if($count < 1){
                    $card = Card::create(['user_id'=>$user->id, 'details'=>$response['data']['authorization']]);
                    $transaction->cards()->attach($card->id);
                }
            }
            if(isset($response['data']['metadata']['use_coupon']) && isset($response['data']['metadata']['coupon_id']) && $response['data']['metadata']['use_coupon'] == "true" && $response['data']['metadata']['coupon_id']*1 > 0){
                $coupon = $user->coupons()->find($response['data']['metadata']['coupon_id']*1);
                if($coupon)
                $coupon->transactions()->attach($transaction->id, array('amount_discounted'=>$order->more->price-$transaction->more->amount));
            }
            return $this->success("Order payment received and confirmed", $order);
        }
        return $this->error($response['message']);
    }
}
