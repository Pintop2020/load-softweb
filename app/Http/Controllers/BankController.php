<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\GlobalMethods as SuperM;
use App\Traits\AppResponse;

class BankController extends Controller
{
    use AppResponse;
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $response = SuperM::paystackGet("https://api.paystack.co/bank");
        if($response['status']) {
            return $this->success($response['message'], $response['data']);
        }
        return $this->error($response['message']);
    }

    public function bankValidate(Request $request)
    {
        $response = SuperM::paystackGet("https://api.paystack.co/bank/resolve?account_number=$request->account_number&bank_code=$request->bank_code");
        if($response['status']){
            return $this->success($response['message'], $response['data']);
        }
        return $this->error($response['message']);
    }
}
