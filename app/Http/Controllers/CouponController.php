<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\User;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CouponController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Coupon();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['entries'] = $this->model->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'transactions')->get();
        return Inertia::render('Coupon/Index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['users'] = User::where('is_admin', false)->where('user_type', 'user')->whereDoesntHave('coupons', function($query){
            $query->where('status', 'pending');
        })->get();
        return Inertia::render('Coupon/Create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->send_all){
            $users = User::where('is_admin', false)->where('user_type', 'user')->whereDoesntHave('coupons', function($query){
                $query->where('status', 'pending');
            })->get();
            $code = $request->code != null ? strtoupper($request->code) : strtoupper(Str::random(8));
            foreach($users as $user){
                $user->coupons()->save(new Coupon(['code'=>$code, 'type'=>$request->type, 'amount'=>$request->amount, 'expired_at'=>Carbon::parse($request->expires)]));
            }
        }else {
            $code = $request->code != null ? strtoupper($request->code) : strtoupper(Str::random(8));
            foreach($request->users as $single){
                $user = User::find($single);
                if(!$user->coupons()->where('status', 'pending')->first()){
                    $user->coupons()->save(new Coupon(['code'=>$code, 'type'=>$request->type, 'amount'=>$request->amount, 'expired_at'=>Carbon::parse($request->expires)]));
                }
            }
        }
        return redirect('/coupons')->with('success', 'Coupons created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['entry'] = $this->model->with('user', 'transactions')->findOrFail($id);
        return Inertia::render('Coupon/Show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['entry'] = $this->model->findOrFail($id);
        return Inertia::render('Coupon/Edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $entry = $this->model->findOrFail($id);
        $data = $request->except('expires');
        $data['expired_at'] = Carbon::parse($request->expires);
        $entry->update($data);
        return redirect()->route('coupons.show', $entry->id)->with('success', 'Coupon updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $entry = $this->model->findOrFail($id);
        if($entry->status != 'pending'){
            return redirect()->back()->with('error', 'You can no longer delete this coupon');
        }
        $entry->delete();
        return redirect('/coupons')->with('success', 'Coupon deleted successfully');
    }
}
