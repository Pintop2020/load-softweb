<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use App\Models\Complaint;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Validator;
use App\Traits\AppResponse;
use Illuminate\Support\Facades\Hash;

class ComplaintController extends Controller
{
    use AppResponse;

    protected $model;

    public function __construct()
    {
        $this->model = new Complaint();
    }

    public function index()
    {
        $data['entries'] = $this->model->orderBy('id', request()->sortBy ?? 'desc')->take(request()->show ? request()->show*1 : 50)->with('user', 'vehicles')->get();
        return Inertia::render('Complaint/Index', $data);
    }

    public function store(Request $request)
    {
        $user = Auth::user();
        $vehicle = Vehicle::findOrFail($request->vehicle_id);
        $entry = $this->model->create(['user_id'=>$user->id, 'message'=>$request->message]);
        $entry->vehicles()->attach($vehicle->id);
        return $this->success("Query sent successfully");
    }
}
