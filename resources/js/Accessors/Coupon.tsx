import { Inertia } from "@inertiajs/inertia"
import { Link } from "@inertiajs/inertia-react"
import clsx from "clsx"
import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { NumberFormat } from "../Reuseable/plugins"
import { Swal } from "../Reuseable/plugins"
import { DropDown } from "../Components/Components"

export const couponColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
      Header: 'Discount',
      accessor: 'discount'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date',
        accessor: 'Date'
    }, {
        Header: 'Expiry',
        accessor: 'expires'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const couponColumnsSimple = [
    {
        Header: 'S/N',
        accessor: 'sn'
    }, {
        Header: 'Discount',
        accessor: 'discount'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date',
        accessor: 'Date'
    }, {
        Header: 'Expiry',
        accessor: 'expires'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type AmountProps = {
    type: string,
    amount: number,
    status: string
}

export const ParseDiscount: React.FC<AmountProps> = ({
    amount, type, status
}) => {
    return (
        <span className={clsx("", status == 'pending' && "text-warning", status == 'used' && "text-success", status == 'expired' && "text-danger")}>
            {type == 'amount' ? <NumberFormat value={amount} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /> : amount.toFixed(2)+'%'}
        </span>
    )
}

type StatusProps = {
    status: any,
}

export const CouponStatus: React.FC<StatusProps> = ({
    status
}) => {
    return (
        <>
        {status == 'pending' && <div className="badge badge-light-warning">Unused</div>}
        {status == 'used' && <div className="badge badge-light-success">Used</div>}
        {status == 'expired' && <div className="badge badge-light-danger">Expired</div>}
        </>
    );
}

function getMenus(item: any){
    let resps: any = []
    resps.push({'permission': 'view coupons', 'click': null, 'name': 'View coupon', 'href': route('coupons.show', item.id)})
    if(item.status == 'pending'){
        resps.push({'permission': 'edit coupons', 'click': null, 'name': 'Edit coupon', 'href': route('coupons.edit', item.id)})
        resps.push({'permission': 'delete coupons', 'click': (event: any) : void => {
            event.preventDefault()
            Swal.fire({
                title: 'Are you sure?',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Proceed'
            }).then((result) => {
                if (result.isConfirmed) {
                    Inertia.delete(route('coupons.destroy', item.id))
                }
            })
        }, 'name': 'Delete Coupon', 'href': ''})
    }
    if(item.user){
        resps.push({'permission': 'view customer', 'click': null, 'name': 'View users', 'href': route('users.show', item.user.id)})
    }
    if(item.transactions.length > 0){
        resps.push({'permission': 'view transactions', 'click': null, 'name': 'View invoice', 'href': route('transactions.show', item.transactions[0].id)})
    }
    return resps
}

export function couponCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user && <Link href={route('users.show', item.user.id)} className="text-gray-800 text-hover-primary mb-1">{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}</Link>
        collect['discount'] = <ParseDiscount amount={item.amount} type={item.type} status={item.status} />
        collect['status'] = <CouponStatus status={item.status}/>
        collect['date'] = datFormat2(item.created_at)
        collect['expires'] = datFormat2(item.expired_at)
        collect['action'] = <DropDown myKey={i+1} myId={'coupons'} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
