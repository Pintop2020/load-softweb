import { Link } from "@inertiajs/inertia-react"
import React from "react"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"

export const notificationColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
        Header: '#',
        accessor: 'passport'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
      Header: 'Subject',
      accessor: 'subject'
    }, {
        Header: 'Message',
        accessor: 'message'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date Sent',
        accessor: 'date_sent'
    }, {
        Header: 'Date Read',
        accessor: 'date_read'
    }
]

type StatusProps = {
    notification: any,
}

export const NotificationStatus: React.FC<StatusProps> = ({
    notification
}) => {
    return (
        <>
            {notification.read_at ? <div className="badge badge-light-success">Read</div> : <div className="badge badge-light-danger">Unread</div>}
        </>
    )
}

export function notificationCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        let decoded: any = JSON.parse(item.data)
        var collect: any = {}
        collect['sn'] = i+1
        collect['passport'] = <><div className="avatar-w"><img width="50px" height="50px" className="rounded-circle avatar-sm" alt="" src={item.user.profile_photo_url}/></div></>
        collect['user'] = <div className="text-gray-600 text-hover-primary mb-1"><div className="text-muted">{capitalizeFirstLetter(item.user.first_name)} {capitalizeFirstLetter(item.user.last_name)}<br/>{item.user.email}</div><div className="text-primary">{item.user.phone_number}</div></div>
        collect['subject'] = decoded.title
        collect['message'] = decoded.body
        collect['status'] = <NotificationStatus notification={item} />
        collect['date_sent'] = datFormat2(item.created_at)
        collect['date_read'] = item.read_at ? datFormat2(item.read_at) : ''
        collection.push(collect)
    })
    return collection
}
