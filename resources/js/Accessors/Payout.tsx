import { Link } from "@inertiajs/inertia-react"
import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { NumberFormat } from "../Reuseable/plugins"

export const payoutColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
      Header: 'Bank',
      accessor: 'bank'
    }, {
      Header: 'Account Name',
      accessor: 'account_name'
    }, {
        Header: 'Account Number',
        accessor: 'account_number'
    }, {
        Header: 'Amount',
        accessor: 'amount'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const payoutColumnsSimple = [
    {
        Header: 'S/N',
        accessor: 'sn'
    }, {
        Header: 'Bank',
        accessor: 'bank'
    }, {
        Header: 'Account Name',
        accessor: 'account_name'
    }, {
        Header: 'Account Number',
        accessor: 'account_number'
    }, {
        Header: 'Amount',
        accessor: 'amount'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type AmountProps = {
    amount: number,
    status: string
}

export const ParsePayoutAmount: React.FC<AmountProps> = ({
    amount, status
}) => {
    return (
        <>
            {status == 'pending' && <span className="text-warning"><NumberFormat value={amount} displayType={"text"} thousandSeparator={true} prefix={"₦"} decimalSeparator="." decimalScale={2} /></span>}
            {status == 'declined' && <span className="text-danger"><NumberFormat value={amount} displayType={"text"} thousandSeparator={true} prefix={"₦"} decimalSeparator="." decimalScale={2} /></span>}
            {status == 'approved' && <span className="text-success"><NumberFormat value={amount} displayType={"text"} thousandSeparator={true} prefix={"₦"} decimalSeparator="." decimalScale={2} /></span>}
        </>
    )
}

type StatusProps = {
    status: string,
}

export const PayoutStatus: React.FC<StatusProps> = ({
    status
}) => {
    return (
        <>
        {status == 'approved' && <div className="badge badge-light-success">Approved</div>}
        {status == 'declined' && <div className="badge badge-light-danger">Declined</div>}
        {status == 'pending' && <div className="badge badge-light-warning">Pending</div>}
        </>
    );
}

export function payoutCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user && <Link href={route('users.show', item.user.id)} className="text-gray-800 text-hover-primary mb-1">{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}</Link>
        collect['bank'] = item.more.bank
        collect['account_name'] = item.more.account_name
        collect['account_number'] = item.more.account_number
        collect['amount'] = <ParsePayoutAmount amount={item.more.amount} status={item.status} />
        collect['status'] = <PayoutStatus status={item.status} />
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <Link href={route('payouts.show', item.id)} className='btn btn-sm btn-light-primary'>View Payout</Link>
        collection.push(collect)
    })
    return collection
}
