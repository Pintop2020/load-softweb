import React from "react"
import route from "ziggy-js"
import { datFormat2 } from "../Reuseable/number_utils"
import clsx from "clsx"
import { DropDown } from "../Components/Components"
import { Swal } from "../Reuseable/plugins"
import { Inertia } from "@inertiajs/inertia"

export const vehicleTypeColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Name',
      accessor: 'name'
    }, {
        Header: 'Icon',
        accessor: 'icon'
    }, {
      Header: 'Trucks',
      accessor: 'trucks'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

function getMenus(item: any){
    let resps: any = []
    resps.push({'permission': 'view vehicle types', 'click': null, 'name': 'View type', 'href': route('vehicle_types.show', item.id)})
    resps.push({'permission': 'edit vehicle types', 'click': null, 'name': 'Edit type', 'href': route('vehicle_types.edit', item.id)})
    resps.push({'permission': 'delete vehicle types', 'click': (event: any) : void => {
        event.preventDefault()
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Proceed'
        }).then((result) => {
            if (result.isConfirmed) {
                Inertia.delete(route('vehicle_types.destroy', item.id), {
                    preserveState: false
                })
            }
        })
    }, 'name': 'Delete Type', 'href': ''})
    return resps
}

export function vehicleTypeCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['name'] = <div className='text-gray-800 text-hover-primary mb-1'>{item.name}</div>
        collect['icon'] = <i className={clsx(item.icon, "text-primary")}></i>
        collect['trucks'] = item.vehicles_count > 0 ? item.vehicles_count+" trucks" : "0 truck"
        collect['date'] = <span className="text-muted">{datFormat2(item.created_at)}</span>
        collect['action'] = <DropDown myId={'types'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
