import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import clsx from "clsx"
import { DropDown } from "../Components/Components"

export const vehicleColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Rider',
      accessor: 'user'
    }, {
        Header: 'Name',
        accessor: 'name'
    }, {
      Header: 'Current Location',
      accessor: 'location'
    }, {
        Header: 'Type',
        accessor: 'type'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const vehicleColumnsSimple = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
        Header: 'Name',
        accessor: 'name'
    }, {
      Header: 'Current Location',
      accessor: 'location'
    }, {
        Header: 'Type',
        accessor: 'type'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type StatusProps = {
    isAvailable: boolean,
}

export const VehicleStatus: React.FC<StatusProps> = ({
    isAvailable
}) => {
    return (
        <>
            {isAvailable ? <span className="badge badge-light-success">Available</span> : <span className="badge badge-light-warning">Unavailable</span>}
        </>
    );
}

type StatusProps3 = {
    isBlaclisted: boolean,
}

export const VehicleBlacklisted: React.FC<StatusProps3> = ({
    isBlaclisted
}) => {
    return (
        <>
            {isBlaclisted ? <span className="badge badge-light-danger">Blacklisted</span> : <span className="badge badge-light-success">Not blacklisted</span>}
        </>
    );
}

type StatusProps2 = {
    status: string,
}

export const VehicleStatus2: React.FC<StatusProps2> = ({
    status
}) => {
    return (
        <>
            {status == 'pending' && <span className="badge badge-light-warning">Pending approval</span>}
            {status == 'approved' && <span className="badge badge-light-success">Approved</span>}
            {status == 'rejected' && <span className="badge badge-light-danger">Rejected</span>}
        </>
    );
}

function getMenus(item: any){
    let resps: any = []
    resps.push({'permission': 'view vehicles', 'click': null, 'name': 'View '+item.vehicle_types[0].name, 'href': route('vehicles.show', item.id)})
    if(item.user){
        resps.push({'permission': 'view riders', 'click': null, 'name': 'View rider', 'href': route('users.show', item.user.id)})
    }
    return resps
}

export function vehicleCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user ? <div className='text-gray-800 text-hover-primary mb-1'>{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}<div className="text-muted">{item.user.email.toLowerCase()}</div></div> : <div className="badge badge-light-warning">Unasigned</div>
        collect['name'] = <span className="text-dark">{item.name}</span>
        collect['location'] = <span className="text-muted">{item.current_address ? item.current_address : 'Unknown'}</span>
        collect['type'] = <i className={clsx(item.vehicle_types[0].icon, "fs-2x text-info")}/>
        collect['status'] = <VehicleStatus2 status={item.status}/>
        collect['date'] = <span className="text-muted">{datFormat2(item.created_at)}</span>
        collect['action'] = <DropDown myId={'vehicles'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
