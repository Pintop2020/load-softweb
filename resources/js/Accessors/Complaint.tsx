import { Link } from "@inertiajs/inertia-react"
import clsx from "clsx"
import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { Swal } from "../Reuseable/plugins"
import { DropDown } from "../Components/Components"
import { Inertia } from "@inertiajs/inertia"

export const complaintColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Customer',
      accessor: 'user'
    }, {
        Header: 'Message',
        accessor: 'message'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const complaintColumnsSimple = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
        Header: 'Message',
        accessor: 'message'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

function getMenus(item: any){
    let resps: any = []
    if(item.user){
        resps.push({'permission': 'view customer', 'click': null, 'name': 'View customer', 'href': route('users.show', item.user.id)})
    }
    if(item.vehicles && item.vehicles.length > 0){
        resps.push({'permission': 'view vehicles', 'click': null, 'name': 'View truck', 'href': route('users.show', item.vehicles[0].id)})
    }
    return resps
}

export function complaintCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user && <div className="text-gray-800 text-hover-primary mb-1">{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}</div>
        collect['message'] = <span className="text-muted">{item.message}</span>
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <DropDown myKey={i+1} myId={'ratings'} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
