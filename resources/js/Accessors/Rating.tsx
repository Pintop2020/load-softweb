import { Link } from "@inertiajs/inertia-react"
import clsx from "clsx"
import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { Swal } from "../Reuseable/plugins"
import { DropDown } from "../Components/Components"
import { Inertia } from "@inertiajs/inertia"

export const ratingColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
      Header: 'Rating',
      accessor: 'ratings'
    }, {
        Header: 'Comments',
        accessor: 'comments'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const ratingColumnsSimple = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
      Header: 'Rating',
      accessor: 'ratings'
    }, {
        Header: 'Comments',
        accessor: 'comments'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type RatingProps = {
    ratings: number
}

function getStars(rating: number){
    let mani = Math.round(rating)
    let balance = 5-mani
    let resps: any = []
    for(var i =1;i<=mani;i++){
        resps.push("fas fa-star")
    }
    if(balance > 0){
        for(var i =1;i<=balance;i++){
            resps.push("far fa-star")
        }
    }
    return resps
}

export const ParseRatings: React.FC<RatingProps> = ({
    ratings
}) => {
    return (
        <>
            {getStars(ratings).map((item: any, i: number)=>{
                return <i key={i} className={clsx(item, "text-warning")}></i>
            })}
        </>
    )
}

function getMenus(item: any){
    let resps: any = []
    if(item.user){
        resps.push({'permission': 'view customer', 'click': null, 'name': 'View customer', 'href': route('users.show', item.user.id)})
    }
    if(item.driver_rated && item.driver_rated.length > 0){
        resps.push({'permission': 'view rider', 'click': null, 'name': 'View rider', 'href': route('users.show', item.driver_rated[0].id)})
    }
    if(item.orders && item.orders.length > 0){
        resps.push({'permission': 'view orders', 'click': null, 'name': 'View order', 'href': route('orders.show', item.orders[0].id)})
    }
    resps.push({'permission': 'delete ratings', 'click': (event: any) : void => {
        event.preventDefault()
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Proceed'
        }).then((result) => {
            if (result.isConfirmed) {
                Inertia.delete(route('ratings.destroy', item.id));
            }
        })
    }, 'name': 'Delete rating', 'href': ''})
    return resps
}

export function ratingCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user && <Link href={route('users.show', item.user.id)} className="text-gray-800 text-hover-primary mb-1">{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}</Link>
        collect['ratings'] = <ParseRatings ratings={item.ratings} />
        collect['comments'] = <span className="text-muted">{item.comments}</span>
        collect['date'] = datFormat2(item.created_at)
        collect['expires'] = datFormat2(item.expired_at)
        collect['action'] = <DropDown myKey={i+1} myId={'ratings'} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
