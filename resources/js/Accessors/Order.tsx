import { Link } from "@inertiajs/inertia-react"
import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import clsx from "clsx"
import { DropDown } from "../Components/Components"

export const orderColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
        Header: 'Name',
        accessor: 'load'
    }, {
      Header: 'Pickup Address',
      accessor: 'pickup'
    }, {
        Header: 'Destination',
        accessor: 'destination'
    }, {
        Header: 'Vehicle',
        accessor: 'vehicle'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const orderColumnsSimple = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
        Header: 'Name',
        accessor: 'load'
    }, {
      Header: 'Pickup Address',
      accessor: 'pickup'
    }, {
        Header: 'Destination',
        accessor: 'destination'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Vehicle',
        accessor: 'vehicle'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type StatusProps = {
    status: string,
}

export const OrderStatus: React.FC<StatusProps> = ({
    status
}) => {
    return (
        <>
            {status == 'accepted' && <div className="badge badge-light-primary">Assigned</div>}
            {status == 'pending' && <div className="badge badge-light-warning">Unassigned</div>}
            {status == 'cancelled' && <div className="badge badge-light-danger">Cancelled</div>}
            {status == 'delivered' && <div className="badge badge-light-success">Delivered</div>}
            {status == 'picked' && <div className="badge badge-light-info">Picked Up</div>}
            {status == 'failed' && <div className="badge badge-light-danger">Failed Delivery</div>}
        </>
    );
}

function getMenus(item: any){
    let resps: any = []
    resps.push({'permission': 'view orders', 'click': null, 'name': 'View order', 'href': route('orders.show', item.id)})
    if(item.rider){
        resps.push({'permission': 'view riders', 'click': null, 'name': 'View rider', 'href': route('users.show', item.rider.id)})
    }
    if(item.user){
        resps.push({'permission': 'view customer', 'click': null, 'name': 'View customer', 'href': route('users.show', item.user.id)})
    }
    if(item.transactions && item.transactions.length > 0){
        resps.push({'permission': 'view transactions', 'click': null, 'name': 'View invoice', 'href': route('transactions.show', item.transactions[0].id)})
    }
    return resps
}

export function orderCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user && <div className='text-gray-800 text-hover-primary mb-1'>{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}</div>
        collect['name'] = <span className="text-muted">{item.more.load_name}</span>
        collect['pickup'] = <span className="text-muted">{item.more.pickup_address}</span>
        collect['destination'] = <span className="text-muted">{item.more.destination_address}</span>
        collect['status'] = <OrderStatus status={item.status} />
        collect['vehicle'] = <i className={clsx(item.vehicle_types[0].icon, "fs-2x text-info")}></i>
        collect['date'] = <span className="text-muted">{datFormat2(item.created_at)}</span>
        collect['action'] = <DropDown myKey={i+1} myId={'orders'} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
