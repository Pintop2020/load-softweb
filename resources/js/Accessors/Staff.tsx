import { Inertia } from "@inertiajs/inertia"
import React from "react"
import route from "ziggy-js"
import { DropDown } from "../Components/Components"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { Swal } from "../Reuseable/plugins"
import { UserStatus } from "./Accessors"

export const staffColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Name',
      accessor: 'name'
    }, {
      Header: 'Email',
      accessor: 'email'
    }, {
        Header: 'Mobile Number',
        accessor: 'mobile'
    }, {
        Header: 'Role',
        accessor: 'role'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date Added',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

function getMenus(item: any){
    let resps: any = []
    // resps.push({'permission': 'view administrators', 'click': null, 'name': 'View staff', 'href': route('admins.show', item.id)})
    resps.push({'permission': 'edit administrators', 'click': null, 'name': 'Edit staff', 'href': route('admins.edit', item.id)})
    resps.push({'permission': 'restrict staff', 'click': (event: any) : void => {
        event.preventDefault()
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Proceed'
        }).then((result) => {
            if (result.isConfirmed) {
                Inertia.get("/users/perm/"+item.id);
            }
        })
    }, 'name': item.is_active ? 'Restrict account' : 'Unrestrict account', 'href': ''})
    return resps
}

export function staffCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['name'] = <div className='text-gray-800 text-hover-primary mb-1'>{capitalizeFirstLetter(item.first_name)+' '+capitalizeFirstLetter(item.last_name)}</div>
        collect['email'] = <div className="text-gray-600 text-hover-primary mb-1">{item.email.toLowerCase()}</div>
        collect['mobile'] = <div className="text-gray-600 text-hover-primary mb-1">{item.phone_number}</div>
        collect['role'] = <div className="text-success text-bold">{item.roles[0].name}</div>
        collect['status'] = <UserStatus isActive={item.is_active} />
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <DropDown myId={'admins'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
