import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { DropDown } from "../Components/Components"
import { NumberFormat, Swal } from "../Reuseable/plugins"

export const priceColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
        Header: 'Role',
        accessor: 'role'
    }, {
      Header: 'Amount',
      accessor: 'amount'
    }, {
        Header: 'Status',
        accessor: 'status'
      }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

function getMenus(item: any){
    let resps: any = []
    if(item.users[0].user_type == 'dispatch') resps.push({'permission': 'view users', 'click': null, 'name': 'View Rider', 'href': route('users.show', item.users[0].id)})
    resps.push({'permission': 'view vehicles', 'click': null, 'name': 'View truck', 'href': route('vehicles.show', item.vehicles[0].id)})
    return resps
}

export function priceCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = <div className='text-gray-800 text-hover-primary mb-1'>{capitalizeFirstLetter(item.users[0].first_name)+' '+capitalizeFirstLetter(item.users[0].last_name)}</div>
        collect['role'] = item.users[0].user_type == 'user' ? 'Customer' : 'Dispatch'
        collect['amount'] = <NumberFormat value={item.amount} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />
        collect['status'] = item.agreed ? <span className="text-success">Accepted</span> : <span className="text-danger">Not accepted</span>
        collect['date'] = <span className="text-muted">{datFormat2(item.created_at)}</span>
        collect['action'] = <DropDown myId={'prices'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
