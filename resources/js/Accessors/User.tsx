import { Inertia } from "@inertiajs/inertia"
import clsx from "clsx"
import React, { FC } from "react"
import route from "ziggy-js"
import { DropDown } from "../Components/Components"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { NumberFormat, Swal } from "../Reuseable/plugins"

export const userColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Name',
      accessor: 'name'
    }, {
      Header: 'Email',
      accessor: 'email'
    }, {
        Header: 'Mobile Number',
        accessor: 'mobile'
    }, {
        Header: 'Wallet',
        accessor: 'wallet'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date Joined',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const riderColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Name',
      accessor: 'name'
    }, {
      Header: 'Email',
      accessor: 'email'
    }, {
        Header: 'Mobile Number',
        accessor: 'mobile'
    }, {
        Header: 'Wallet',
        accessor: 'wallet'
    }, {
        Header: 'Trucks',
        accessor: 'trucks'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date Joined',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type AmountProps = {
    amount: number,
}

export const ParseWallet: FC<AmountProps> = ({
    amount
}) => {
    return (
        <>
            <span className="text-danger"><NumberFormat value={amount} displayType={"text"} thousandSeparator={true} prefix={"₦"} decimalSeparator="." decimalScale={2} /></span>
        </>
    )
}

type StatusProps = {
    isActive: boolean,
}

type GuarantorProps = {
    user: any,
}

export const UserStatus: FC<StatusProps> = ({
    isActive
}) => {
    return (
        <>
            {isActive ? <div className="badge badge-light-success">Active</div> : <div className="badge badge-light-danger">Restricted</div>}
        </>
    );
}

export const GuarantorStatus: FC<GuarantorProps> = ({
    user
}) => {
    return (
        <>
            {user.more && user.more.is_guarantor_confirmed ? <div className="badge badge-light-success">Confirmed</div> : <div className="badge badge-light-danger">Unguaranteed</div>}
        </>
    );
}

function getMenus(item: any){
    let resps: any = []
    if(item.user_type == 'dispatch'){
        resps.push({'permission': 'view riders', 'click': null, 'name': 'View account', 'href': route('users.show', item.id)})
    }else {
        resps.push({'permission': 'view users', 'click': null, 'name': 'View account', 'href': route('users.show', item.id)})
    }
    if(item.vehicle){
        resps.push({'permission': 'view vehicles', 'click': null, 'name': 'View vehicle', 'href': route('vehicles.show', item.vehicle.id)})
    }
    resps.push({'permission': 'restrict user', 'click': (event: any) : void => {
        event.preventDefault()
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Proceed'
        }).then((result) => {
            if (result.isConfirmed) {
                Inertia.get("/users/perm/"+item.id);
            }
        })
    }, 'name': item.is_active ? 'Restrict account' : 'Unrestrict account', 'href': ''})
    return resps
}

export function userCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['name'] = <div className='text-gray-800 text-hover-primary mb-1'>{capitalizeFirstLetter(item.first_name)+' '+capitalizeFirstLetter(item.last_name)}</div>
        collect['email'] = <div className="text-gray-600 text-hover-primary mb-1">{item.email.toLowerCase()}</div>
        collect['mobile'] = <div className="text-gray-600 text-hover-primary mb-1">{item.phone_number}</div>
        collect['wallet'] = <ParseWallet amount={item.wallet.amount} />
        collect['status'] = <UserStatus isActive={item.is_active} />
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <DropDown myId={'users'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}

export function riderCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['name'] = <div className='text-gray-800 text-hover-primary mb-1'>{capitalizeFirstLetter(item.first_name)+' '+capitalizeFirstLetter(item.last_name)}</div>
        collect['email'] = <div className="text-gray-600 text-hover-primary mb-1">{item.email.toLowerCase()}</div>
        collect['mobile'] = <div className="text-gray-600 text-hover-primary mb-1">{item.phone_number}</div>
        collect['wallet'] = <ParseWallet amount={item.wallet.amount} />
        collect['trucks'] = item.vehicles.length > 0 ? item.vehicles.length+" trucks" : "0 truck"
        collect['status'] = <UserStatus isActive={item.is_active} />
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <DropDown myId={'riders'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
