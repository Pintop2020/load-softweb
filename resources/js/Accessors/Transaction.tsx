import React from "react"
import route from "ziggy-js"
import { capitalizeFirstLetter, datFormat2 } from "../Reuseable/number_utils"
import { NumberFormat } from "../Reuseable/plugins"
import { DropDown } from "../Components/Components"

export const transactionColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'User',
      accessor: 'user'
    }, {
      Header: 'Amount',
      accessor: 'amount'
    }, {
      Header: 'Mode',
      accessor: 'mode'
    }, {
        Header: 'Reference',
        accessor: 'reference'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

export const transactionColumnsSimple = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Amount',
      accessor: 'amount'
    }, {
      Header: 'Mode',
      accessor: 'mode'
    }, {
        Header: 'Reference',
        accessor: 'reference'
    }, {
        Header: 'Status',
        accessor: 'status'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

type AmountProps = {
    amount: number,
    type: string
}

export const ParseAmount: React.FC<AmountProps> = ({
    amount, type
}) => {
    return (
        <>
            {type == 'debit' && <span className="text-danger"><NumberFormat value={amount} displayType={"text"} thousandSeparator={true} prefix={"- "} suffix=" NGN" decimalSeparator="." decimalScale={2} /></span>}
            {type == 'credit' && <span className="text-success"><NumberFormat value={amount} displayType={"text"} thousandSeparator={true} prefix={"+ "} suffix=" NGN" decimalSeparator="." decimalScale={2} /></span>}
        </>
    )
}

type StatusProps = {
    status: any,
}

export const TranStatus: React.FC<StatusProps> = ({
    status
}) => {
    return (
        <>
        {status == 'success' && <div className="badge badge-light-success">Successful</div>}
        {status == 'failed' && <div className="badge badge-light-danger">Failed</div>}
        {status == 'pending' && <div className="badge badge-light-warnig">Pending</div>}
        </>
    );
}


function getMenus(item: any){
    let resps: any = []
    resps.push({'permission': 'view transactions', 'click': null, 'name': 'View invoice', 'href': route('transactions.show', item.id)})
    if(item.user){
        resps.push({'permission': 'view users', 'click': null, 'name': 'View user', 'href': route('users.show', item.user.id)})
    }
    if(item.orders.length > 0){
        resps.push({'permission': 'view orders', 'click': null, 'name': 'View order', 'href': route('orders.show', item.orders[0].id)})
    }
    if(item.coupons.length > 0){
        resps.push({'permission': 'view coupons', 'click': null, 'name': 'View coupon', 'href': route('coupons.show', item.coupons[0].id)})
    }
    if(item.payouts.length > 0){
        resps.push({'permission': 'view payouts', 'click': null, 'name': 'View payout', 'href': route('payouts.show', item.payouts[0].id)})
    }
    return resps
}

export function transactionCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['user'] = item.user && <div className="text-gray-800 text-hover-primary mb-1">{capitalizeFirstLetter(item.user.first_name)+' '+capitalizeFirstLetter(item.user.last_name)}</div>
        collect['amount'] = <ParseAmount amount={item.more.amount} type={item.more.type} />
        collect['mode'] = item.more.payment_option.toUpperCase();
        collect['reference'] = <span className="text-dark">{item.more.reference}</span>
        collect['status'] = <TranStatus status={item.more.status} />
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <DropDown myId={'transactions'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
