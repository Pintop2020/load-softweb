import { Inertia } from "@inertiajs/inertia"
import React from "react"
import route from "ziggy-js"
import { DropDown } from "../Components/Components"
import { datFormat2 } from "../Reuseable/number_utils"
import { Swal } from "../Reuseable/plugins"

export const roleColumns = [
    {
      Header: 'S/N',
      accessor: 'sn'
    }, {
      Header: 'Name',
      accessor: 'name'
    }, {
      Header: 'Permissions',
      accessor: 'permissions'
    }, {
        Header: 'Date',
        accessor: 'date'
    }, {
        Header: 'Action',
        accessor: 'action'
    }
]

function getMenus(item: any){
    let resps: any = []
    resps.push({'permission': 'edit roles', 'click': null, 'name': 'Edit role', 'href': route('roles.edit', item.id)})
    resps.push({'permission': 'delete roles', 'click': (event: any) : void => {
        event.preventDefault()
        Swal.fire({
            title: 'Are you sure?',
            text: "",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Proceed'
        }).then((result) => {
            if (result.isConfirmed) {
                Inertia.delete(route('roles.destroy', item.id));
            }
        })
    }, 'name': 'Delete role', 'href': ''})
    return resps
}

export function roleCollection(data: any) {
    let collection: any = []
    data.forEach((item: any, i: number)=>{
        var collect: any = {}
        collect['sn'] = i+1
        collect['name'] = <div className='text-gray-800 text-hover-primary mb-1'>{item.name.toUpperCase()}</div>
        collect['permissions'] = item.permissions.length
        collect['date'] = datFormat2(item.created_at)
        collect['action'] = <DropDown myId={'roles'} myKey={i+1} lists={getMenus(item)}/>
        collection.push(collect)
    })
    return collection
}
