export function checkIsActive(item: any) {
    if(item.subs.length > 0){
        const current = item.link.replace('/', '')
        const currentUrl = getCurrentUrl(window.location.pathname)
        if (current === currentUrl) {
          return true
        }
        return false
    }else {
        const current = item.link
        const currentUrl = window.location.pathname
        if (current === currentUrl) {
          return true
        }
        return false
    }
}

export function getCurrentUrl(pathname: string) {
    return pathname.split('/')[1]
}
