import React from 'react'
import { ReactHtmlParser } from '../Reuseable/plugins';

type Props = {
    className?: string,
    icon: string
}

const SVGP: React.FC<Props> = ({className = '', icon}) => {
  return (

    <span className={`svg-icon ${className}`}>
        { ReactHtmlParser(icon) }
    </span>
  )
}

export default SVGP
