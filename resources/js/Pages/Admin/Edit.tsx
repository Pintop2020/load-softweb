import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Col, Form, Row, Card } from 'react-bootstrap'
import { Select } from '../../Reuseable/plugins'
import { CircularProgress } from '@mui/material'

type Props = {
    roles: any,
    entry: any
}

interface IErrors {
    first_name: any,
    last_name: any,
    role: any
}

function getRoles(roles: any) {
    var resps: any = []
    roles.map((item: any)=>{
        resps.push({value: item.id, label: item.name})
    })
    return resps
}

const AdminEdit: React.FC<Props> = ({
    roles,
    entry,
}) => {
    const {data, setData, put, processing, errors} = useForm({
        first_name: entry.first_name,
        last_name: entry.last_name,
        role: entry.roles[0].id,
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        first_name: null,
        last_name: null,
        role: null
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            put('/admins/'+entry.id, {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title={'Edit staff'} pages={['Dashboard', 'Staff', 'Edit staff']}>
            <div className="row gy-5 g-xl-8">
                <div className="col-xxl-12">
                    <Card>
                        <Card.Header>
                            <Card.Title>Edit staff</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={handleSubmit} className="">
                                <Row>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">First Name</Form.Label>
                                            <Form.Control name="first_name" type="text" value={data.first_name} onChange={handleChange} disabled={processing} />
                                            {errors.first_name && <Form.Text className="text-danger">{errors.first_name}</Form.Text>}
                                            {formErrors.first_name && <Form.Text className="text-danger">{formErrors.first_name}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Last Name</Form.Label>
                                            <Form.Control name="last_name" type="text" value={data.last_name} onChange={handleChange} disabled={processing} />
                                            {errors.last_name && <Form.Text className="text-danger">{errors.last_name}</Form.Text>}
                                            {formErrors.last_name && <Form.Text className="text-danger">{formErrors.last_name}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={12} lg={12}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Role</Form.Label>
                                            <Select defaultValue={{ value: entry.roles[0].id, label: entry.roles[0].name }} options={getRoles(roles)} name="role" onChange={(selected: any)=>{
                                                setData({...data, role: selected.value})
                                                setFormErrors({...formErrors, role: null})
                                            }} isDisabled={processing} />
                                            {formErrors.role && <Form.Text className="text-danger">{formErrors.role}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-4">
                                    <button disabled={processing} type="submit" className={["btn btn-sm btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }
                                    </button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </AdminLayout>
    )
}
export default AdminEdit
