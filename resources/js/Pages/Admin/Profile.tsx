import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm, usePage } from '@inertiajs/inertia-react'
import bgImage from '../../../assets/media/avatars/blank.png'
import { Form } from 'react-bootstrap'
import { CircularProgress } from '@mui/material'



const AdminProfile = () => {
    const { auth } = usePage().props
    let sessionData: any = auth
    const [image, setImage] = useState('')
    const {data, setData, post, processing} = useForm({
        passport: null,
        first_name: sessionData.user.first_name,
        last_name: sessionData.user.last_name,
        email: sessionData.user.email,
        phone_number: sessionData.user.phone_number,
    })
    const [formErrors, setFormErrors] = useState({
        first_name: null,
        last_name: null,
        email: null,
        phone_number: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'passport') {
                errorsInit[key] = "This field is required"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value: any = target.type === 'file' ? target.files[0]
        : target.value
        setData(target.name, value)
        let errorSign: any = {...formErrors}
        errorSign[target.name] = null
        setFormErrors(errorSign)
        if(target.name == 'passport') {
            setImage(URL.createObjectURL(target.files[0]))
        }
    }
    const handleSubmit = (e: any) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/profile/update', {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title='My Profile' pages={['My Profile']}>
            <div className="row g-5 g-xl-8">
                <div className="card mb-5 mb-xl-10">
                    <div className="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
                        <div className="card-title m-0">
                            <h3 className="fw-bolder m-0">Profile Details</h3>
                        </div>
                    </div>
                    <div className="collapse show">
                        <Form onSubmit={handleSubmit} className="form">
                            <div className="card-body border-top p-9">
                                <div className="row mb-6">
                                    <label className="col-lg-4 col-form-label fw-bold fs-6">Avatar</label>
                                    <div className="col-lg-8">
                                        <div className="image-input image-input-outline" data-kt-image-input="true" style={{
                                            backgroundImage: `url(${bgImage})`
                                         }}>
                                            <div className="image-input-wrapper w-125px h-125px" style={{
                                                backgroundImage: `url(${image ? image : sessionData.user.profile_photo_url})`
                                             }}></div>
                                            <label className="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                <i className="bi bi-pencil-fill fs-7"></i>
                                                <input type="file" name="passport" accept=".png, .jpg, .jpeg" onChange={handleChange} />
                                                <input type="hidden" name="avatar_remove" />
                                            </label>
                                        </div>
                                        <div className="form-text">Allowed file types: png, jpg, jpeg.</div>
                                    </div>
                                </div>
                                <div className="row mb-6">
                                    <label className="col-lg-4 col-form-label required fw-bold fs-6">Full Name</label>
                                    <div className="col-lg-8">
                                        <div className="row">
                                            <div className="col-lg-6 fv-row">
                                                <Form.Control type="text" name="first_name" className="form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="First name" disabled={processing} value={data.first_name} onChange={handleChange} />
                                                {formErrors.first_name && <Form.Text className="text-danger">{formErrors.first_name}</Form.Text>}
                                            </div>
                                            <div className="col-lg-6 fv-row">
                                                <Form.Control type="text" name="last_name" className="form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="Last name" disabled={processing} value={data.last_name} onChange={handleChange} />
                                                {formErrors.last_name && <Form.Text className="text-danger">{formErrors.last_name}</Form.Text>}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row mb-6">
                                    <label className="col-lg-4 col-form-label required fw-bold fs-6">Email</label>
                                    <div className="col-lg-8">
                                        <Form.Control type="email" name="email" className="form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="Email" disabled={true} value={data.email} onChange={handleChange} />
                                    </div>
                                </div>
                                <div className="row mb-6">
                                    <label className="col-lg-4 col-form-label required fw-bold fs-6">Phone number</label>
                                    <div className="col-lg-8">
                                        <Form.Control type="text" name="phone_number" className="form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="Phone number" disabled={true} value={data.phone_number} onChange={handleChange} />
                                    </div>
                                </div>
                            </div>
                            <div className="card-footer d-flex justify-content-end py-6 px-9">
                                <button type="submit" disabled={processing} className="btn btn-primary">{processing ? <><CircularProgress color="inherit" /></> : "Save Changes" }</button>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default AdminProfile
