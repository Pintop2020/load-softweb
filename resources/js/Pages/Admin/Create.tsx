import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Col, Form, Row, Card } from 'react-bootstrap'
import { Select } from '../../Reuseable/plugins'
import { CircularProgress } from '@mui/material'

type Props = {
    roles: any,
}

interface IErrors {
    first_name: any,
    last_name: any,
    email: any,
    phone_number: any,
    role: any
}

function getRoles(roles: any) {
    var resps: any = [];
    roles.map((item: any)=>{
        resps.push({value: item.id, label: item.name});
    });
    return resps;
}

const AdminCreate: React.FC<Props> = ({
    roles,
}) => {
    const {data, setData, post, processing, errors} = useForm({
        first_name: '',
        last_name: '',
        email: '',
        phone_number: '',
        role: ''
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        first_name: null,
        last_name: null,
        email: null,
        phone_number: null,
        role: null
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        const validMail: any = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        let validPhoneNumber: any = /^[0]\d{10}$/;

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required";
            }
            if (fields.email && !fields.email.match(validMail)) {
                errorsInit.email = "Please enter a valid email address";
            }
            if (fields.phone_number && !fields.phone_number.match(validPhoneNumber)) {
                errorsInit.phone_number = "Please enter a valid phone number";
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/admins', {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title={'Add staff'} pages={['Dashboard', 'Staff', 'Add staff']}>
            <div className="row gy-5 g-xl-8">
                <div className="col-xxl-12">
                    <Card>
                        <Card.Header>
                            <Card.Title>Add new staff</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={handleSubmit} className="">
                                <Row>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">First Name</Form.Label>
                                            <Form.Control name="first_name" type="text" value={data.first_name} onChange={handleChange} disabled={processing} />
                                            {errors.first_name && <Form.Text className="text-danger">{errors.first_name}</Form.Text>}
                                            {formErrors.first_name && <Form.Text className="text-danger">{formErrors.first_name}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Last Name</Form.Label>
                                            <Form.Control name="last_name" type="text" value={data.last_name} onChange={handleChange} disabled={processing} />
                                            {errors.last_name && <Form.Text className="text-danger">{errors.last_name}</Form.Text>}
                                            {formErrors.last_name && <Form.Text className="text-danger">{formErrors.last_name}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">E-mail</Form.Label>
                                            <Form.Control name="email" type="email" value={data.email} onChange={handleChange} disabled={processing} />
                                            {errors.email && <Form.Text className="text-danger">{errors.email}</Form.Text>}
                                            {formErrors.email && <Form.Text className="text-danger">{formErrors.email}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Phone number</Form.Label>
                                            <Form.Control name="phone_number" type="text" value={data.phone_number} onChange={handleChange} disabled={processing} />
                                            {formErrors.phone_number && <Form.Text className="text-danger">{formErrors.phone_number}</Form.Text>}
                                            {errors.phone_number && <Form.Text className="text-danger">{errors.phone_number}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={12} lg={12}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Role</Form.Label>
                                            <Select options={getRoles(roles)} name="role" onChange={(selected: any)=>{
                                                setData({...data, role: selected.value});
                                                setFormErrors({...formErrors, role: null});
                                            }} isDisabled={processing} />
                                            {formErrors.role && <Form.Text className="text-danger">{formErrors.role}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-4">
                                    <button disabled={processing} type="submit" className={["btn btn-sm btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }
                                    </button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </AdminLayout>
    )
}
export default AdminCreate
