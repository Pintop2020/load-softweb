import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import ShowUser from './ShowUser'
import { getUserType } from './Shared'
import ShowRider from './ShowRider'

type Props = {
    entry: any,
    transactions: any,
    payouts?: any,
    vehicles?: any
}

const UserShow: FC<Props> = ({
    entry,
    transactions,
    payouts,
    vehicles
}) => {
    return (
        <AdminLayout title={'View '+getUserType(entry)} pages={['Dashboard', 'Users', getUserType(entry)+'s', 'View '+getUserType(entry)]}>
            <div className="d-flex flex-column flex-xl-row">
                {entry.user_type == 'user' && <ShowUser entry={entry} transactions={transactions} payouts={payouts}/>}
                {entry.user_type == 'dispatch' && <ShowRider entry={entry} transactions={transactions} payouts={payouts} vehicles={vehicles}/>}
            </div>
        </AdminLayout>
    )
}
export default UserShow
