import React, { FC } from 'react'
import { UserStatus, transactionColumnsSimple, transactionCollection, orderColumnsSimple, orderCollection, couponColumnsSimple, couponCollection, payoutColumnsSimple, payoutCollection, ratingColumnsSimple, ratingCollection, complaintColumnsSimple, complaintCollection } from '../../Accessors/Accessors'
import { checkPermission } from '../../Reuseable/app_config'
import { capitalizeFirstLetter, datFormat3 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import { Card } from 'react-bootstrap'
import {NumberFormat, Swal} from '../../Reuseable/plugins'
import { Link, usePage } from '@inertiajs/inertia-react'
import { Inertia } from '@inertiajs/inertia'
import {DataTable} from '../../Components/Components'
import { getUserType, OverviewBar, SingleDetail, WalletUpdate } from './Shared'

type Props = {
    entry: any,
    transactions: any,
    payouts: any
}

const ShowUser: FC<Props> = ({
    entry,
    transactions,
    payouts
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <>
            <div className="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10">
                <div className="card mb-5 mb-xl-8">
                    <div className="card-body pt-15">
                        <div className="d-flex flex-center flex-column mb-5">
                            <div className="symbol symbol-100px symbol-circle mb-7">
                                <img src={entry.profile_photo_url} />
                            </div>
                            <a href="#" className="fs-3 text-gray-800 text-hover-primary fw-bolder mb-1">{capitalizeFirstLetter(entry.first_name)} {capitalizeFirstLetter(entry.last_name)}</a>
                            <div className="fs-5 fw-bold text-muted mb-6">{getUserType(entry)}</div>
                            <OverviewBar title1='Orders' data1={entry.orders.length} title2='Status' data2={<UserStatus isActive={entry.is_active}/>} title3='Transactions' data3={<NumberFormat value={transactions} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />}/>
                            <OverviewBar title1='Payouts' data1={<NumberFormat value={payouts} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />} title2='Ratings' data2={entry.ratings_count} title3='Ratings' data3={entry.full_ratings}/>
                        </div>
                        <div className="d-flex flex-stack fs-4 py-3">
                            <div className="fw-bolder">Details</div>
                            {checkPermission(sessionData.user, 'restrict user') && <Link href="#" onClick={(event)=>{
                                event.preventDefault()
                                Swal.fire({
                                    title: 'Are you sure?',
                                    text: "",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Proceed'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        Inertia.get("/users/perm/"+entry.id);
                                    }
                                })
                            }} className={clsx("btn btn-sm", entry.is_active ? "btn-light-danger":"btn-light-success")}>{entry.is_active ? "Restrict Account": "Unrestrict Account"}</Link>}
                        </div>
                        <div className="separator separator-dashed my-3"></div>
                        <div className="collapse show">
                            <div className="py-5 fs-6">
                                <UserStatus isActive={entry.is_active}/>
                                <SingleDetail title="Email" children={entry.email.toLowerCase()}/>
                                <SingleDetail title="Phone Number" children={entry.phone_number}/>
                                {entry.more && <SingleDetail title="Address" children={entry.more.address}/>}
                                {entry.more && <SingleDetail title="State" children={entry.more.state}/>}
                                <SingleDetail title="Date Joined" children={datFormat3(entry.created_at)}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex-lg-row-fluid ms-lg-15">
                {checkPermission(sessionData.user, "update wallet") && <WalletUpdate entry={entry}/>}
                {entry.transactions.length > 0 && <Card className="mb-11">
                    <Card.Header>
                        <Card.Title>Transactions</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={transactionColumnsSimple} data={transactionCollection(entry.transactions)} />
                    </Card.Body>
                </Card>}
                {entry.orders.length > 0 && <Card className="mb-8">
                    <Card.Header>
                        <Card.Title>Orders</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={orderColumnsSimple} data={orderCollection(entry.orders)} />
                    </Card.Body>
                </Card>}
                {entry.coupons.length > 0 && <Card className="mb-8">
                    <Card.Header>
                        <Card.Title>Coupons</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={couponColumnsSimple} data={couponCollection(entry.coupons)} />
                    </Card.Body>
                </Card>}
                {entry.payouts.length > 0 && <Card className="mb-8">
                    <Card.Header>
                        <Card.Title>Payouts</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={payoutColumnsSimple} data={payoutCollection(entry.payouts)} />
                    </Card.Body>
                </Card>}
                {entry.ratings.length > 0 && <Card className="mb-8">
                    <Card.Header>
                        <Card.Title>Ratings</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={ratingColumnsSimple} data={ratingCollection(entry.ratings)} />
                    </Card.Body>
                </Card>}
                {entry.complaints.length > 0 && <Card className="mb-8">
                    <Card.Header>
                        <Card.Title>Complaints</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={complaintColumnsSimple} data={complaintCollection(entry.complaints)} />
                    </Card.Body>
                </Card>}
            </div>
        </>
    )
}
export default ShowUser
