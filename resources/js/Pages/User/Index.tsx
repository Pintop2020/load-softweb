import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { userColumns, riderColumns, userCollection, riderCollection } from '../../Accessors/Accessors'

type Props = {
    entries: any,
    title: string
}

const UserIndex: React.FC<Props> = ({
    entries,
    title
}) => {
    return (
        <AdminLayout title={title} pages={['Dashboard', 'Users', title]}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            {title.toLowerCase() == 'customers' && <DataTable columns={ userColumns } data={userCollection(entries)} />}
                            {title.toLowerCase() == 'dispatch' && <DataTable columns={ riderColumns } data={riderCollection(entries)} />}
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default UserIndex
