import React, { FC } from 'react'
import { UserStatus, transactionColumnsSimple, transactionCollection, orderColumns, orderCollection, ratingColumns, ratingCollection, GuarantorStatus, payoutColumnsSimple, payoutCollection, vehicleColumnsSimple, vehicleCollection } from '../../Accessors/Accessors'
import { checkPermission } from '../../Reuseable/app_config'
import { capitalizeFirstLetter, datFormat3 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import { Card } from 'react-bootstrap'
import {NumberFormat, Swal} from '../../Reuseable/plugins'
import { Link, usePage } from '@inertiajs/inertia-react'
import { Inertia } from '@inertiajs/inertia'
import {DataTable} from '../../Components/Components'
import { getUserType, OverviewBar, SingleDetail, WalletUpdate } from './Shared'

type Props = {
    entry: any,
    transactions: any,
    payouts: any,
    vehicles: any
}

const ShowRider: FC<Props> = ({
    entry,
    transactions,
    payouts,
    vehicles
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <>
            <div className="flex-column flex-lg-row-auto w-100 w-xl-350px mb-10">
                <div className="card mb-5 mb-xl-8">
                    <div className="card-body pt-15">
                        <div className="d-flex flex-center flex-column mb-5">
                            <div className="symbol symbol-100px symbol-circle mb-7">
                                <img src={entry.profile_photo_url} />
                            </div>
                            <a href="#" className="fs-3 text-gray-800 text-hover-primary fw-bolder mb-1">{capitalizeFirstLetter(entry.first_name)} {capitalizeFirstLetter(entry.last_name)}</a>
                            <div className="fs-5 fw-bold text-muted mb-6">{getUserType(entry)}</div>
                            <OverviewBar title1='Rides' data1={entry.rides.length} title2='Status' data2={<UserStatus isActive={entry.is_active}/>} title3='Transactions' data3={<NumberFormat value={transactions} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />}/>
                            <OverviewBar title1='Payouts' data1={<NumberFormat value={payouts} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />} title2='Trucks' data2={entry.vehicles.length} title3='Guarantor' data3={<GuarantorStatus user={entry} />}/>
                            <OverviewBar title1='Ratings' data1={entry.full_ratings} title2='All rated' data2={entry.driver_ratings_count} title3='' data3=''/>
                        </div>
                        <div className="d-flex flex-stack fs-4 py-3">
                            <div className="fw-bolder">Details</div>
                            {checkPermission(sessionData.user, 'restrict user') && <Link href="#" onClick={(event)=>{
                                event.preventDefault()
                                Swal.fire({
                                    title: 'Are you sure?',
                                    text: "",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Proceed'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        Inertia.get("/users/perm/"+entry.id);
                                    }
                                })
                            }} className={clsx("btn btn-sm", entry.is_active ? "btn-light-danger":"btn-light-success")}>{entry.is_active ? "Restrict Account": "Unrestrict Account"}</Link>}
                        </div>
                        <div className="separator separator-dashed my-3"></div>
                        <div className="collapse show">
                            <div className="py-5 fs-6">
                                {checkPermission(sessionData.user, "update user") && entry.more && !entry.more.is_guarantor_confirmed && <Link href="#" onClick={(event)=>{
                                    event.preventDefault()
                                    Swal.fire({
                                        title: 'Are you sure?',
                                        text: "",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Proceed'
                                    }).then((result) => {
                                        if (result.isConfirmed) {
                                            Inertia.get("/guarantors/confirm/"+entry.id);
                                        }
                                    })
                                }} className="btn btn-sm btn-light-success mb-9">Confirm Guarantor</Link>}
                                <SingleDetail title="Email" children={entry.email.toLowerCase()}/>
                                <SingleDetail title="Phone Number" children={entry.phone_number}/>
                                {entry.more && <>
                                    <SingleDetail title="Date of birth" children={entry.more.date_of_birth}/>
                                    <SingleDetail title="Driver license">
                                        <a href={'/storage/'+entry.more.driver_license} target="_blank" className="btn btn-sm btn-light-primary">Download</a>
                                    </SingleDetail>
                                    <SingleDetail title="Guarantor's name" children={entry.more.guarantor_name}/>
                                    <SingleDetail title="Guarantor's phone number" children={entry.more.guarantor_phone_number}/>
                                    <SingleDetail title="Guarantor's address" children={entry.more.gurantor_address}/>
                                    <SingleDetail title="Guarantor's passport">
                                        <img src={'/storage/'+entry.more.guarantor_passport} style={{
                                            width: '120px', height: '120px'
                                        }}/>
                                    </SingleDetail>
                                    <SingleDetail title="Next of kin's name" children={entry.more.next_kin_name}/>
                                    <SingleDetail title="Next of kin's phone number" children={entry.more.next_kin_phone_number}/>
                                    <SingleDetail title="Next of kin's address" children={entry.more.next_kin_address}/>
                                    <SingleDetail title="Next of kin's relationship" children={entry.more.next_kin_relationship}/>
                                    <SingleDetail title="Nationality" children={entry.more.nationality}/>
                                    <SingleDetail title="Address" children={entry.more.address}/>
                                    <SingleDetail title="State" children={entry.more.state}/>
                                    <SingleDetail title="LGA" children={entry.more.local_government}/>
                                </>}
                                <SingleDetail title="Date Joined" children={datFormat3(entry.created_at)}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="flex-lg-row-fluid ms-lg-15">
                {checkPermission(sessionData.user, "update wallet") && <WalletUpdate entry={entry}/>}
                {entry.vehicles.length > 0 && <Card className="mb-11">
                    <Card.Header>
                        <Card.Title>Trucks</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={vehicleColumnsSimple} data={vehicleCollection(entry.vehicles)} />
                    </Card.Body>
                </Card>}
                {entry.transactions.length > 0 && <Card className="mb-11">
                    <Card.Header>
                        <Card.Title>Transactions</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={transactionColumnsSimple} data={transactionCollection(entry.transactions)} />
                    </Card.Body>
                </Card>}
                {entry.rides.length > 0 && <Card className="mb-11">
                    <Card.Header>
                        <Card.Title>Rides</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={orderColumns} data={orderCollection(entry.rides)} />
                    </Card.Body>
                </Card>}
                {entry.ratings.length > 0 && <Card className="mb-11">
                    <Card.Header>
                        <Card.Title>Ratings</Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <DataTable columns={ratingColumns} data={ratingCollection(entry.driver_ratings)} />
                    </Card.Body>
                </Card>}
                {entry.payouts.length > 0 && <Card className="mb-11">
                    <Card.Header>
                        <Card.Title>Payouts</Card.Title>
                    </Card.Header>
                    <Card.Body>
                    <DataTable columns={payoutColumnsSimple} data={payoutCollection(entry.payouts)} />
                    </Card.Body>
                </Card>}
            </div>
        </>
    )
}
export default ShowRider
