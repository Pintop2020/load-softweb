import React, { FC, useState } from 'react'
import { Modal, Card, Form } from 'react-bootstrap'
import {NumberFormat} from '../../Reuseable/plugins'
import { useForm } from '@inertiajs/inertia-react'
import { CircularProgress } from '@mui/material'

type Props = {
    entry: any,
}

interface IErrors {
    type: any,
    amount: any,
    note: any,
    reference: any,
}

export const WalletUpdate: FC<Props> = ({
    entry
}) => {
    const [show, setShow] = useState(false)
    const {data, setData, post, processing} = useForm({
        type: '',
        amount: '',
        note: '',
        reference: '',
        user: entry.id
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        type: null,
        amount: null,
        note: null,
        reference: null
    })
    const validateInput = () => {

        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required"
            }
            if (fields.amount && Number(fields.amount) < 100) {
                errorsInit.email = "Please enter a valid amount greater than N100"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/update_wallet', {
                preserveState: false
            })
        }
    }
    return (
        <>
            <Card className="pt-4 mb-6 mb-xl-9">
                <Card.Header className="border-0">
                    <Card.Title className="fw-bolder">Wallet Balance</Card.Title>
                    <div className="card-toolbar">
                        <a href="#" className="btn btn-sm btn-flex btn-light-primary" onClick={(e)=>{
                            e.preventDefault()
                            setShow(true)
                        }}><i className="fas fa-pen"></i>Adjust Balance</a>
                    </div>
                </Card.Header>
                <div className="card-body pt-0">
                    <div className="fw-bolder fs-2">
                        <NumberFormat value={entry.wallet.amount} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />
                    </div>
                </div>
            </Card>
            <Modal className="fade" show={show} onHide={()=> setShow(false)} size="lg" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Adjust Wallet</Modal.Title>
                </Modal.Header>
                <Modal.Body className="scroll-y mx-5 mx-xl-15 my-7">
                    <div className="d-flex text-center mb-9">
                        <div className="w-50 border border-dashed border-gray-300 rounded mx-2 p-4">
                            <div className="fs-6 fw-bold mb-2 text-muted">Current Balance</div>
                            <div className="fs-2 fw-bolder" kt-modal-adjust-balance="current_balance"><NumberFormat value={entry.wallet.amount} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /></div>
                        </div>
                        <div className="w-50 border border-dashed border-gray-300 rounded mx-2 p-4">
                            <div className="fs-6 fw-bold mb-2 text-muted">New Balance</div>
                            <div className="fs-2 fw-bolder" kt-modal-adjust-balance="new_balance">{data.amount && data.type ? <NumberFormat value={data.type == 'credit' ? entry.wallet.amount+Number(data.amount) : entry.wallet.amount-Number(data.amount)} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /> : "--"}</div>
                        </div>
                    </div>
                    <Form className="form" onSubmit={handleSubmit}>
                        <div className="fv-row mb-7">
                            <Form.Label className="required fs-6 fw-bold mb-2">Adjustment Type</Form.Label>
                            <Form.Select className="form-select-solid fw-bolder" name="type" onChange={handleChange} disabled={processing}>
                                <option></option>
                                <option value="credit">Credit</option>
                                <option value="debit">Debit</option>
                            </Form.Select>
                            {formErrors.type && <Form.Text className="text-danger">{formErrors.type}</Form.Text>}
                        </div>
                        <div className="fv-row mb-7">
                            <Form.Label className="required fs-6 fw-bold mb-2">Amount</Form.Label>
                            <Form.Control type='number' step='any' className="form-control-solid" name='amount' onChange={handleChange} disabled={processing} value={data.amount} />
                            {formErrors.amount && <Form.Text className="text-danger">{formErrors.amount}</Form.Text>}
                        </div>
                        <div className="fv-row mb-7">
                            <Form.Label className="required fs-6 fw-bold mb-2">Adjustment note</Form.Label>
                            <Form.Control as={'textarea'} cols={3} type='text' className="form-control-solid rounded-3 mb-5" name='note' onChange={handleChange} disabled={processing} value={data.note} />
                            {formErrors.note && <Form.Text className="text-danger">{formErrors.note}</Form.Text>}
                        </div>
                        <div className="fv-row mb-7">
                            <Form.Label className="required fs-6 fw-bold mb-2">Reference</Form.Label>
                            <Form.Control type='text' className="form-control-solid rounded-3" name='reference' onChange={handleChange} disabled={processing} value={data.reference} />
                            {formErrors.reference && <Form.Text className="text-danger">{formErrors.reference}</Form.Text>}
                        </div>
                        {Number(data.amount) > entry.wallet.amount && data.type == 'debit' && <div className="fs-7 text-danger">User does not have sufficient funds for this transaction. This debit will enter <strong>Lien</strong> for when next money enters this account.</div>}
                        <div className="fs-7 text-muted mb-15">Please be aware that all manual balance changes will be audited by the financial team every fortnight. Please maintain your invoices and receipts until then. Thank you.</div>
                        <div className="text-center">
                            <button type="button" className="btn btn-light me-3" onClick={()=>{
                                setData({...data, type: '', amount: '', note: '', reference: ''})
                            }}>Discard</button>
                            <button disabled={processing} type="submit" className="btn btn-primary"> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }</button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
        </>
    )
}

type LogProps = {
    data1?: any,
    title1?: any,
    data2?: any,
    title2?: any,
    data3?: any,
    title3?: any,
}

type DetailProps = {
    title?: any,
    children?: any,
}

export function getUserType(user: any){
    let resps: any = '';
    if(user.user_type == 'user'){
        resps = 'Customer'
    }else {
        resps = 'Rider'
    }
    return resps
}

export const OverviewBar: FC<LogProps> = ({
    data1, title1, data2, title2, data3, title3
}) => {
    return (
        <div className="d-flex flex-wrap flex-center">
            <div className="border border-gray-300 border-dashed rounded py-3 px-3 mb-3">
                <div className="fs-4 fw-bolder text-gray-700">
                    <span className="w-75px">{data1}</span>
                </div>
                <div className="fw-bold text-muted">{title1}</div>
            </div>
            <div className="border border-gray-300 border-dashed rounded py-3 px-3 mx-4 mb-3">
                <div className="fs-4 fw-bolder text-gray-700">
                    <span className="w-50px">{data2}</span>
                </div>
                <div className="fw-bold text-muted">{title2}</div>
            </div>
            <div className="border border-gray-300 border-dashed rounded py-3 px-3 mb-3">
                <div className="fs-4 fw-bolder text-gray-700">
                    <span className="w-50px">{data3}</span>
                </div>
                <div className="fw-bold text-muted">{title3}</div>
            </div>
        </div>
    )
}

export const SingleDetail: FC<DetailProps> = ({
    title, children
}) => {
    return (
        <>
            <div className="fw-bolder mt-5">{title}</div>
            <div className="text-gray-600">{children}</div>
        </>
    )
}
