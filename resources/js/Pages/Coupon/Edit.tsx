import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Col, Form, Row, Card } from 'react-bootstrap'
import { CircularProgress } from '@mui/material'
import { datFormat5 } from '../../Reuseable/number_utils'

type Props = {
    entry: any,
}

interface IErrors {
    expires: any,
    type: any,
    amount: any
}

const CouponEdit: React.FC<Props> = ({
    entry,
}) => {
    const {data, setData, put, processing} = useForm({
        code: entry.code,
        type: entry.type,
        amount: entry.amount,
        expires: datFormat5(entry.expired_at),
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        type: null,
        amount: null,
        expires: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'code') {
                errorsInit[key] = "This field is required"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            put('/coupons/'+entry.id, {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title={'Edit Coupon'} pages={['Dashboard', 'Coupons', 'Edit coupon']}>
            <div className="row gy-5 g-xl-8">
                <div className="col-xxl-12">
                    <Card>
                        <Card.Header>
                            <Card.Title>Edit coupon</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={handleSubmit} className="">
                                <Row>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Type</Form.Label>
                                            <Form.Select className="form-select-solid fw-bolder" name="type" onChange={handleChange} disabled={processing} value={data.type}>
                                                <option></option>
                                                <option value="percent">Percentage</option>
                                                <option value="amount">Amount</option>
                                            </Form.Select>
                                            {formErrors.type && <Form.Text className="text-danger">{formErrors.type}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Amount</Form.Label>
                                            <Form.Control type='number' step='any' className="form-control-solid" name='amount' onChange={handleChange} disabled={processing} value={data.amount} />
                                            {formErrors.amount && <Form.Text className="text-danger">{formErrors.amount}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="fs-6 fw-bold mb-2">Code</Form.Label>
                                            <Form.Control type='text' className="form-control-solid" name='code' onChange={handleChange} disabled={processing} value={data.code} />
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Expires at</Form.Label>
                                            <Form.Control type='date' className="form-control-solid" name='expires' onChange={handleChange} disabled={processing} value={data.expires} />
                                            {formErrors.expires && <Form.Text className="text-danger">{formErrors.expires}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-4">
                                    <button disabled={processing} type="submit" className={["btn btn-sm btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }
                                    </button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </AdminLayout>
    )
}
export default CouponEdit
