import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Col, Form, Row, Card } from 'react-bootstrap'
import { Select } from '../../Reuseable/plugins'
import { CircularProgress } from '@mui/material'
import { capitalizeFirstLetter } from '../../Reuseable/number_utils'

type Props = {
    users: any,
}

interface IErrors {
    expires: any,
    users: any,
    type: any,
    amount: any
}

const getUserValue = (users: any) => {
    let resps: any = []
    users.map((item: any)=>{
        resps.push({label: capitalizeFirstLetter(item.first_name)+' '+capitalizeFirstLetter(item.last_name) + '  --  '+item.email.toLowerCase(), value: item.id})
    })
    return resps
}

const CouponCreate: React.FC<Props> = ({
    users,
}) => {
    const {data, setData, post, processing} = useForm({
        users: [],
        code: '',
        type: '',
        amount: '',
        expires: '',
        send_all: false,
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        users: null,
        type: null,
        amount: null,
        expires: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'users' && key != 'code' && key != 'send_all') {
                errorsInit[key] = "This field is required"
            }
            if(fields.users.length < 1 && !fields.send_all){
                errorsInit.users = "Please select at least a user to generate coupon for"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/coupons', {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title={'Add Coupons'} pages={['Dashboard', 'Coupons', 'Add coupons']}>
            <div className="row gy-5 g-xl-8">
                <div className="col-xxl-12">
                    <Card>
                        <Card.Header>
                            <Card.Title>Add new coupon</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={handleSubmit} className="">
                                <Form.Group className="mb-3">
                                    <div className="form-check form-check-custom form-check-solid">
                                        <input className="form-check-input" type="checkbox" name="send_all" checked={data.send_all} onChange={handleChange} id="flexCheckDefault"/>
                                        <label className="form-check-label" htmlFor="flexCheckDefault">
                                        Send to all users
                                        </label>
                                    </div>
                                </Form.Group>
                                <Row>
                                    {!data.send_all && <Col sm={12} md={12} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Users</Form.Label>
                                            <Select options={getUserValue(users)} closeMenuOnSelect={false} isMulti name="users" onChange={(selected: any)=>{
                                                // @ts-ignore
                                                setData({...data, users: Array.isArray(selected) ? selected.map(x => x.value) : []})
                                            }} />
                                            {formErrors.users && <Form.Text className="text-danger">{formErrors.users}</Form.Text>}
                                        </Form.Group>
                                    </Col>}
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Type</Form.Label>
                                            <Form.Select className="form-select-solid fw-bolder" name="type" onChange={handleChange} disabled={processing}>
                                                <option></option>
                                                <option value="percent">Percentage</option>
                                                <option value="amount">Amount</option>
                                            </Form.Select>
                                            {formErrors.type && <Form.Text className="text-danger">{formErrors.type}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Amount</Form.Label>
                                            <Form.Control type='number' step='any' className="form-control-solid" name='amount' onChange={handleChange} disabled={processing} value={data.amount} />
                                            {formErrors.amount && <Form.Text className="text-danger">{formErrors.amount}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="fs-6 fw-bold mb-2">Code</Form.Label>
                                            <Form.Control type='text' className="form-control-solid" name='code' onChange={handleChange} disabled={processing} value={data.code} />
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={3} className="mb-3">
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Expires at</Form.Label>
                                            <Form.Control type='date' className="form-control-solid" name='expires' onChange={handleChange} disabled={processing} value={data.expires} />
                                            {formErrors.expires && <Form.Text className="text-danger">{formErrors.expires}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-4">
                                    <button disabled={processing} type="submit" className={["btn btn-sm btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }
                                    </button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </AdminLayout>
    )
}
export default CouponCreate
