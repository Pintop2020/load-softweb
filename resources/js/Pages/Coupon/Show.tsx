import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { CouponStatus, ParseDiscount } from '../../Accessors/Accessors'
import { checkPermission, logoFile, mainSite } from '../../Reuseable/app_config'
import { capitalizeFirstLetter, datFormat3 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import { Row } from 'react-bootstrap'
import route from 'ziggy-js'
import { Link, usePage } from '@inertiajs/inertia-react'

type Props = {
    entry: any,
}

type LogProps = {
    title: string,
    children?: any,
    className?: string
}

const SingleLog: FC<LogProps> = ({
    children,
    title,
    className
}) => {
    return (
        <div className="col-sm-6">
            <div className="fw-bold fs-7 text-gray-600 mb-1">{title}:</div>
            <div className={clsx("fw-bolder fs-6 text-gray-800", className && className)}>{children}</div>
        </div>
    )
}

const CouponShow: FC<Props> = ({
    entry,
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <AdminLayout title={'View coupon'} pages={['Dashboard', 'Coupons', 'View coupon']}>
            <div className="card">
                <div className="card-body p-lg-20">
                    <div className="d-flex flex-column flex-xl-row">
                        <div className="flex-lg-row-fluid me-xl-18 mb-10 mb-xl-0">
                            <div className="mt-n1">
                                <div className="d-flex flex-stack pb-10">
                                    <a href={mainSite}> <img alt="Logo" src={logoFile} className="h-40px" /> </a>
                                    {entry.transactions.length > 0 && checkPermission(sessionData.user, "view transactions") && <Link href={route('transactions.show', entry.transactions[0].id)} className="btn btn-sm btn-success mr-2">View Transaction</Link>}
                                </div>
                                <div className="m-0">
                                    <Row className="g-5 mb-11">
                                        <SingleLog title="Date Created" children={datFormat3(entry.created_at)}/>
                                        <SingleLog title="Amount">
                                            <ParseDiscount amount={entry.amount} type={entry.type} status={entry.status}/>
                                        </SingleLog>
                                        <SingleLog title="Status">
                                            <CouponStatus status={entry.status}/>
                                        </SingleLog>
                                        <SingleLog title="Created For: ">
                                            <div className="text-gray-800">{capitalizeFirstLetter(entry.user.first_name)+' '+capitalizeFirstLetter(entry.user.last_name)}</div><div className="text-muted">{entry.user.email.toLowerCase()}</div><Link href={route('users.show', entry.user.id)} className="btn btn-sm btn-primary mr-2">View Customer</Link>
                                        </SingleLog>
                                    </Row>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default CouponShow
