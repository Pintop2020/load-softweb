import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { couponColumns, couponCollection } from '../../Accessors/Accessors'
import { Link, usePage } from '@inertiajs/inertia-react'
import route from 'ziggy-js'
import { checkPermission } from '../../Reuseable/app_config'

type Props = {
    entries: any,
}

const CouponIndex: React.FC<Props> = ({
    entries,
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <AdminLayout title={'Coupons'} pages={['Dashboard', 'Coupons']} breadAction={checkPermission(sessionData.user, 'create coupons') && <Link href={route('coupons.create')} className="btn btn-sm btn-primary">Add Coupon</Link>}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            <DataTable columns={couponColumns} data={couponCollection(entries)} />
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default CouponIndex
