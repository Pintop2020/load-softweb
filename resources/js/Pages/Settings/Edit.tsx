import React, { FC, useState} from 'react'
import { useForm } from '@inertiajs/inertia-react'
import { Form, Col, Row, Card, InputGroup, FormControl } from 'react-bootstrap'

type Props = {
    entry: any,
}

export const EditSetting: FC<Props> = ({
    entry
}) => {
    const {data, setData, post, processing} = useForm({
        value: Number(entry.value),
        entry_id: entry.id
    })
    const [formErrors, setFormErrors] = useState({
        value: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'entry_id') {
                errorsInit[key] = "This field is required"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/settings/update', {
                preserveState: false
            })
        }
    }
    return (
        <Col sm={12} md={6}>
            <Card>
                <Form onSubmit={handleSubmit}>
                    <Card.Header>
                        <Card.Title>
                            <div className="card-label fw-bolder text-gray-800">{entry.label}</div>

                        </Card.Title>
                    </Card.Header>
                    <Card.Body>
                        <div className="text-gray-400 mt-1 fw-bold fs-6">{entry.description}</div>
                        <Row>
                            <Col lg={12} className="mt-2">
                                <Form.Label>Value</Form.Label>
                                <InputGroup className="mb-3">
                                    <InputGroup.Text>{entry.pre}</InputGroup.Text>
                                    <FormControl type='number' name='value' onChange={handleChange} disabled={processing} value={data.value} />
                                </InputGroup>
                                {/* <Form.Control /> */}
                                {formErrors.value && <Form.Text className="text-danger">{formErrors.value}</Form.Text>}
                            </Col>
                        </Row>
                    </Card.Body>
                    <Card.Footer>
                        <button className="btn btn-primary mt-2" type="submit" disabled={processing}>{processing ? ("updating settings......") : ("Update Setting")}</button>
                    </Card.Footer>
                </Form>
            </Card>
        </Col>
    )
}
