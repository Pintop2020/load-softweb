import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { EditSetting } from './Edit'

type Props = {
    entries: any,
}

const SettingIndex: React.FC<Props> = ({
    entries
}) => {
    return (
        <AdminLayout title={'System Settings'} pages={['Dashboard', 'System Settings']}>
            <div className="row gy-5 g-xl-8">
                {entries.map((item: any, i: any)=>{
                    return <React.Fragment key={i}>
                        <EditSetting entry={item} />
                    </React.Fragment>
                })}
            </div>
        </AdminLayout>
    )
}
export default SettingIndex
