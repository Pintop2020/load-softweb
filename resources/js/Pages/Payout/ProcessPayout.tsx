import React, { FC, useState } from 'react'
import { Modal, Form } from 'react-bootstrap'
import { Link, useForm } from '@inertiajs/inertia-react'
import { CircularProgress } from '@mui/material'

type Props = {
    entry: any,
}

interface IErrors {
    type: any,
    note: any,
}

export const PayoutUpdate: FC<Props> = ({
    entry
}) => {
    const [show, setShow] = useState(false)
    const {data, setData, post, processing} = useForm({
        type: '',
        note: '',
        payout: entry.id
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        type: null,
        note: null,
    })
    const validateInput = () => {

        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/payouts/process', {
                preserveState: false
            })
        }
    }
    return (
        <>
            <Modal className="fade" show={show} onHide={()=> setShow(false)} size="lg" centered>
                <Modal.Header closeButton>
                    <Modal.Title>Process Payout</Modal.Title>
                </Modal.Header>
                <Modal.Body className="scroll-y mx-5 mx-xl-15 my-7">
                    <Form className="form" onSubmit={handleSubmit}>
                        <div className="fv-row mb-7">
                            <Form.Label className="required fs-6 fw-bold mb-2">Action</Form.Label>
                            <Form.Select className="form-select-solid fw-bolder" name="type" onChange={handleChange} disabled={processing}>
                                <option></option>
                                <option value="approve">Approve</option>
                                <option value="decline">Decline</option>
                            </Form.Select>
                            {formErrors.type && <Form.Text className="text-danger">{formErrors.type}</Form.Text>}
                        </div>
                        <div className="fv-row mb-7">
                            <Form.Label className="required fs-6 fw-bold mb-2">Adjustment note</Form.Label>
                            <Form.Control as={'textarea'} cols={3} type='text' className="form-control-solid rounded-3 mb-5" name='note' onChange={handleChange} disabled={processing} value={data.note} />
                            {formErrors.note && <Form.Text className="text-danger">{formErrors.note}</Form.Text>}
                        </div>
                        <div className="text-center">
                            <button type="button" className="btn btn-light me-3" onClick={()=>{
                                setData({...data, type: '', note: ''})
                            }}>Discard</button>
                            <button disabled={processing} type="submit" className="btn btn-primary"> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }</button>
                        </div>
                    </Form>
                </Modal.Body>
            </Modal>
            <Link href='' className="btn btn-sm btn-success mr-2" onClick={(e)=>{
                e.preventDefault()
                setShow(true)
            }}>Process Payout</Link>
        </>
    )
}
