import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { NumberFormat } from '../../Reuseable/plugins'
import { checkPermission } from '../../Reuseable/app_config'
import { Link, usePage } from '@inertiajs/inertia-react'
import clsx from 'clsx'
import bgImage from '../../../assets/media/illustrations/sketchy-1/4.svg'
import route from 'ziggy-js'
import { Col, Row, Card } from 'react-bootstrap'
import ReactApexChart from 'react-apexcharts'
import { OrderStatus } from '../../Accessors/Accessors'

type Props = {
    wallet: any,
    transactions: any,
    payouts: any,
    orders: any,
    trucks: any,
    users: any,
    riders: any,
    histories: any,
    month_label: any,
    payouts_repo: any,
    transaction_repo: any
}

type LogProps = {
    icon: string,
    iconColor?: string,
    title: string,
    value: any,
}

const SingleLog: FC<LogProps> = ({
    icon, iconColor, title, value
}) => {
    return (
        <div className="col-xl-4">
            <a href="#" className="card hoverable card-xl-stretch mb-xl-8">
                <div className="card-body">
                    <i className={clsx(icon, "fs-3x", iconColor && iconColor)}></i>
                    <div className="text-dark fw-bolder fs-2 mb-2 mt-5"><NumberFormat value={value} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /></div>
                    <div className="fw-bold text-muted">{title}</div>
                </div>
            </a>
        </div>
    )
}

type DonutChartProps = {
    arrayData: any,
    labels: any
}

const DonutChartss: FC<DonutChartProps> = ({
    arrayData,
    labels
}) => {
    let donuts: any = {
        series: arrayData,
        options: {
            labels: labels,
            chart: {
                type: 'donut',
            },
            dataLabels: {
                enabled: false
            },
            fill: {
                type: 'gradient',
            },
            legend: {
                formatter: function(val: any, opts: any) {
                  return '<span class="fs-6 fw-bold text-gray-400">'+val + '</span><span class="text-muted">  -  </span><span class="fw-boldest text-gray-700 text-end">' + opts.w.globals.series[opts.seriesIndex]+'%</span>'
                }
            },
            responsive: [{
                breakpoint: 480,
                options: {
                  chart: {
                    width: 200
                  },
                  legend: {
                    position: 'bottom'
                  }
                }
            }]
        },
    }
    return (
        <ReactApexChart className="min-h-auto" options={donuts.options} series={donuts.series} type="donut" width={380}/>
    )
}

function ridersChart(month_label: any, payouts_repo: any, transaction_repo: any){
    let dataArr: any = {
        series: [{
            name: 'Transactions',
            type: 'column',
            data: transaction_repo
          }, {
            name: 'Payouts',
            type: 'line',
            data: payouts_repo
          }],
          options: {
            chart: {
              height: 350,
              type: 'line',
            },
            stroke: {
              width: [0, 4]
            },
            dataLabels: {
              enabled: false,
              enabledOnSeries: [1]
            },
            labels: month_label,
            yaxis: [{
              title: {
                text: 'Transactions',
              },

            }, {
              opposite: true,
              title: {
                text: 'Payouts'
              }
            }]
        },
    }
    return dataArr
}

const Dashboard: FC<Props> = ({
    transactions,
    payouts,
    wallet,
    orders,
    trucks,
    users,
    riders,
    histories,
    month_label,
    payouts_repo,
    transaction_repo
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <AdminLayout title='Dashboard' pages={['Dashboard']}>
            <div className="row g-5 g-xl-8">
                {checkPermission(sessionData.user, "view dashboard logs") &&  <>
                    <SingleLog icon={'fas fa-wallet'} iconColor={'text-success'} title={'Wallet Balance'} value={wallet}/>
                    <SingleLog icon={'fas fa-chart-line'} iconColor={'text-info'} title={'Transactions'} value={transactions}/>
                    <SingleLog icon={'fas fa-money-bill-wave-alt'} iconColor={'text-danger'} title={'Payouts'} value={payouts}/>
                </>}
            </div>
            <Row className="g-5 g-xl-8">
                {/* Simple Space taker */}
                <Col xl={4} className="mb-xl-10">
                    <Card className="h-md-100">
                        <Card.Body className="d-flex flex-column flex-center">
                            <div className="mb-2">
                                <h1 className="fw-bold text-gray-800 text-center lh-lg">Quick truck types
                                <br />
                                <span className="fw-boldest">Add New Truck Type ?</span></h1>
                                <div className="flex-grow-1 bgi-no-repeat bgi-size-contain bgi-position-x-center card-rounded-bottom h-200px mh-200px my-5 my-lg-12" style={{
                                    backgroundImage: `url(${bgImage})`
                                }}></div>
                            </div>
                            <div className="text-center">
                                {checkPermission(sessionData.user, "add vehicle types") && <Link href={route('vehicle_types.create')} className="btn btn-sm btn-primary me-2">Add Truck Type</Link>}
                                {checkPermission(sessionData.user, "view vehicle types") && <Link className="btn btn-sm btn-light" href="/vehicle_types">View Truck Types</Link>}
                            </div>
                        </Card.Body>
                    </Card>
                </Col>
                {/* users data */}
                <Col xl={8} className="mb-5 mb-xl-10">
                    <Row className="g-5 g-xl-10">
                        <Col md={6} xl={6} className="mb-5 mb-xl-10">
                            {/* customers data */}
                            <Card className="card-flush h-md-50 mb-lg-10">
                                <Card.Header className="pt-5">
                                    <Card.Title className="card-title d-flex flex-column">
                                        <span className="fs-2hx fw-bolder text-dark me-2 lh-1">{users.all}</span>
                                        <span className="text-gray-400 pt-1 fw-bold fs-6">Customers</span>
                                    </Card.Title>
                                </Card.Header>
                                <Card.Body className="d-flex flex-column justify-content-end">
                                    <span className="fs-6 fw-boldest text-gray-800 d-block mb-2">Recent Customers</span>
                                    <div className="symbol-group symbol-hover">
                                        {users.recents.map((item: any, i: number)=>{
                                            return <div key={i} className="symbol symbol-35px symbol-circle" title={item.first_name.toUpperCase()}>
                                                <img alt="Pic" src={item.profile_photo_url} />
                                            </div>
                                        })}
                                        {users.all - users.recent > 0 && checkPermission(sessionData.user, 'view users') && <Link href="/users/type/user" className="symbol symbol-35px symbol-circle">
                                            <span className="symbol-label bg-gray-900 text-gray-300 fs-8 fw-bolder">+{users.all - users.recent}</span>
                                        </Link>}
                                    </div>
                                </Card.Body>
                            </Card>
                            {/* orders starts here */}
                            <Card className="card-flush h-md-50 mb-5 mb-lg-10">
                                <Card.Header className="pt-5">
                                    <Card.Title className="d-flex flex-column">
                                        <span className="fs-2hx fw-bolder text-dark me-2 lh-1">{orders.all}</span>
                                        <span className="text-gray-400 pt-1 fw-bold fs-6">Orders</span>
                                    </Card.Title>
                                </Card.Header>
                                <Card.Body className=" d-flex align-items-end pt-0">
                                    <div className="d-flex align-items-center flex-wrap">
                                        <div className="d-flex me-7 me-xxl-10">
                                            <DonutChartss labels={['Pending', 'In progress', 'Delivered', 'Cancelled']} arrayData={[orders.pending, orders.in_progress, orders.delivered, orders.cancelled]}/>
                                        </div>
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col md={6} xl={6} className="mb-5 mb-xl-10">
                            {/* truck starts here */}
                            <Card className="card-flush h-md-50 mb-5 mb-lg-10">
                                <Card.Header className="pt-5">
                                    <Card.Title className="d-flex flex-column">
                                        <span className="fs-2hx fw-bolder text-dark me-2 lh-1">{trucks.all}</span>
                                        <span className="text-gray-400 pt-1 fw-bold fs-6">Trucks</span>
                                    </Card.Title>
                                </Card.Header>
                                <Card.Body className=" d-flex align-items-end pt-0">
                                    <div className="d-flex align-items-center flex-wrap">
                                        <DonutChartss labels={['Blacklisted', 'Available', 'Unavailable']} arrayData={[trucks.blacklisted, trucks.available, trucks.unavailable]}/>
                                    </div>
                                </Card.Body>
                            </Card>
                            {/* Riders here */}
                            <Card className="card-flush h-md-50 mb-lg-10">
                                <Card.Header className="pt-5">
                                    <Card.Title className="card-title d-flex flex-column">
                                        <span className="fs-2hx fw-bolder text-dark me-2 lh-1">{riders.all}</span>
                                        <span className="text-gray-400 pt-1 fw-bold fs-6">Riders</span>
                                    </Card.Title>
                                </Card.Header>
                                <Card.Body className="d-flex flex-column justify-content-end">
                                    <span className="fs-6 fw-boldest text-gray-800 d-block mb-2">Recent Riders</span>
                                    <div className="symbol-group symbol-hover">
                                        {riders.recents.map((item: any, i: number)=>{
                                            return <div key={i} className="symbol symbol-35px symbol-circle" title={item.first_name.toUpperCase()}>
                                                <img alt="Pic" src={item.profile_photo_url} />
                                            </div>
                                        })}
                                        {riders.all - riders.recent > 0 && checkPermission(sessionData.user, 'view dispatches') && <Link href="/users/type/user" className="symbol symbol-35px symbol-circle">
                                            <span className="symbol-label bg-gray-900 text-gray-300 fs-8 fw-bolder">+{riders.all - riders.recent}</span>
                                        </Link>}
                                    </div>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="gy-5 g-xl-10">
                <Col lg={4} className="mb-lg-10">
                    <Card className="card-flush h-xxl-100">
                        <Card.Header className="pt-7">
                            <Card.Title className="align-items-start flex-column">
                                <span className="card-label fw-bolder text-gray-800">Recent Histories</span>
                                <span className="text-gray-400 mt-1 fw-bold fs-6"></span>
                            </Card.Title>
                            {checkPermission(sessionData.user, 'view orders') &&<div className="card-toolbar">
                                <Link href="/orders/type/pending" className="btn btn-sm btn-light">View All</Link>
                            </div>}
                        </Card.Header>
                        <Card.Body>
                            {histories.map((item: any, i: number)=>{
                                return <React.Fragment key={i}>
                                    <div className="m-0">
                                        <div className="d-flex align-items-sm-center mb-5">
                                            <div className="symbol symbol-45px me-4">
                                                <span className="symbol-label bg-primary">
                                                    <span className="svg-icon svg-icon-2x svg-icon-white">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3" d="M21.4 8.35303L19.241 10.511L13.485 4.755L15.643 2.59595C16.0248 2.21423 16.5426 1.99988 17.0825 1.99988C17.6224 1.99988 18.1402 2.21423 18.522 2.59595L21.4 5.474C21.7817 5.85581 21.9962 6.37355 21.9962 6.91345C21.9962 7.45335 21.7817 7.97122 21.4 8.35303ZM3.68699 21.932L9.88699 19.865L4.13099 14.109L2.06399 20.309C1.98815 20.5354 1.97703 20.7787 2.03189 21.0111C2.08674 21.2436 2.2054 21.4561 2.37449 21.6248C2.54359 21.7934 2.75641 21.9115 2.989 21.9658C3.22158 22.0201 3.4647 22.0084 3.69099 21.932H3.68699Z" fill="black" />
                                                            <path d="M5.574 21.3L3.692 21.928C3.46591 22.0032 3.22334 22.0141 2.99144 21.9594C2.75954 21.9046 2.54744 21.7864 2.3789 21.6179C2.21036 21.4495 2.09202 21.2375 2.03711 21.0056C1.9822 20.7737 1.99289 20.5312 2.06799 20.3051L2.696 18.422L5.574 21.3ZM4.13499 14.105L9.891 19.861L19.245 10.507L13.489 4.75098L4.13499 14.105Z" fill="black" />
                                                        </svg>
                                                    </span>
                                                </span>
                                            </div>
                                            <div className="d-flex align-items-center flex-row-fluid flex-wrap">
                                                <div className="flex-grow-1 me-2">
                                                    <a href="#" className="text-gray-400 fs-6 fw-bold">{item.vehicle_types.length > 0 && item.vehicle_types[0].name}</a>
                                                    <span className="text-gray-800 fw-bolder d-block fs-4">{item.more.load_name}</span>
                                                </div>
                                                <OrderStatus status={item.status} />
                                            </div>
                                        </div>
                                        <div className="timeline">
                                            <div className="timeline-item align-items-center mb-7">
                                                <div className="timeline-line w-40px mt-6 mb-n12"></div>
                                                <div className="timeline-icon" style={{ marginLeft: '11px' }}>
                                                    <span className="svg-icon svg-icon-2 svg-icon-danger">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 10C10.9 10 10 10.9 10 12C10 13.1 10.9 14 12 14C13.1 14 14 13.1 14 12C14 10.9 13.1 10 12 10ZM6.39999 9.89999C6.99999 8.19999 8.40001 6.9 10.1 6.4C10.6 6.2 10.9 5.7 10.7 5.1C10.5 4.6 9.99999 4.3 9.39999 4.5C7.09999 5.3 5.29999 7 4.39999 9.2C4.19999 9.7 4.5 10.3 5 10.5C5.1 10.5 5.19999 10.6 5.39999 10.6C5.89999 10.5 6.19999 10.2 6.39999 9.89999ZM14.8 19.5C17 18.7 18.8 16.9 19.6 14.7C19.8 14.2 19.5 13.6 19 13.4C18.5 13.2 17.9 13.5 17.7 14C17.1 15.7 15.8 17 14.1 17.6C13.6 17.8 13.3 18.4 13.5 18.9C13.6 19.3 14 19.6 14.4 19.6C14.5 19.6 14.6 19.6 14.8 19.5Z" fill="black" />
                                                            <path d="M16 12C16 14.2 14.2 16 12 16C9.8 16 8 14.2 8 12C8 9.8 9.8 8 12 8C14.2 8 16 9.8 16 12ZM12 10C10.9 10 10 10.9 10 12C10 13.1 10.9 14 12 14C13.1 14 14 13.1 14 12C14 10.9 13.1 10 12 10Z" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="timeline-content m-0">
                                                    <span className="fs-6 text-gray-400 fw-bold d-block">Pickup</span>
                                                    <span className="fs-6 fw-bolder text-gray-800">{item.more.pickup_address}</span>
                                                </div>
                                            </div>
                                            <div className="timeline-item align-items-center">
                                                <div className="timeline-line w-40px"></div>
                                                <div className="timeline-icon" style={{ marginLeft: '11px' }}>
                                                    <span className="svg-icon svg-icon-2 svg-icon-info">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path opacity="0.3" d="M18.0624 15.3453L13.1624 20.7453C12.5624 21.4453 11.5624 21.4453 10.9624 20.7453L6.06242 15.3453C4.56242 13.6453 3.76242 11.4453 4.06242 8.94534C4.56242 5.34534 7.46242 2.44534 11.0624 2.04534C15.8624 1.54534 19.9624 5.24534 19.9624 9.94534C20.0624 12.0453 19.2624 13.9453 18.0624 15.3453Z" fill="black" />
                                                            <path d="M12.0624 13.0453C13.7193 13.0453 15.0624 11.7022 15.0624 10.0453C15.0624 8.38849 13.7193 7.04535 12.0624 7.04535C10.4056 7.04535 9.06241 8.38849 9.06241 10.0453C9.06241 11.7022 10.4056 13.0453 12.0624 13.0453Z" fill="black" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <div className="timeline-content m-0">
                                                    <span className="fs-6 text-gray-400 fw-bold d-block">Destination</span>
                                                    <span className="fs-6 fw-bolder text-gray-800">{item.more.destination_address}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="separator separator-dashed my-6"></div>
                                </React.Fragment>
                            })}
                        </Card.Body>
                    </Card>
                </Col>
                <Col lg={8} className="mb-5 mb-xxl-10">
                    <Card className="card-flush h-xxl-100">
                        <Card.Header className="pt-5">
                            <Card.Title className="card-title d-flex flex-column">Transactions/Payouts Overview</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <ReactApexChart
                                options={ridersChart(month_label, payouts_repo, transaction_repo).options}
                                series={ridersChart(month_label, payouts_repo, transaction_repo).series}
                                width="100%"
                                height="380"
                            />
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </AdminLayout>
    )
}
export default Dashboard
