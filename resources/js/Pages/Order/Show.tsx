import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable } from '../../Components/Components'
import { vehicleColumns, vehicleCollection, OrderStatus, priceColumns, priceCollection } from '../../Accessors/Accessors'
import { Link } from '@inertiajs/inertia-react'
import route from 'ziggy-js'
import { capitalizeFirstLetter, datFormat3 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import {NumberFormat} from '../../Reuseable/plugins'

type Props = {
    entry: any,
}

type LogProps = {
    title: string,
    children?: any,
    titleClass?: string,
    childClass?: string
}

const SingleLog: FC<LogProps> = ({
    title,
    children,
    titleClass,
    childClass
}) => {
    return (
        <tr>
            <td className={clsx('text-gray-400', titleClass && titleClass)}>{title}:</td>
            <td className={clsx('text-gray-800', childClass && childClass)}>{children}</td>
        </tr>
    )
}

type TableProps = {
    title: string,
    children?: any
}

const SingleTable: FC<TableProps> = ({
    title,
    children,
}) => {
    return (
        <div className="card card-flush pt-3 mb-5 mb-xl-10">
            <div className="card-header">
                <div className="card-title">
                    <h2>{title}</h2>
                </div>
            </div>
            <div className="card-body pt-0">
                {children}
            </div>
        </div>
    )
}

const OrderShow: FC<Props> = ({
    entry,
}) => {
    return (
        <AdminLayout title="Viewing Order" pages={['Dashboard', 'Orders', 'View order']}>
            <div className="d-flex flex-column flex-lg-row">
                <div className="flex-lg-row-fluid me-lg-15 order-2 order-lg-1 mb-10 mb-lg-0">
                    <div className="card card-flush pt-3 mb-5 mb-xl-10">
                        <div className="card-header">
                            <div className="card-title">
                                <h2 className="fw-bolder">Order Details</h2>
                            </div>
                        </div>
                        <div className="card-body pt-3">
                            <div className="mb-10">
                                <div className="d-flex flex-wrap py-5">
                                    <div className="flex-equal me-5">
                                        <table className="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <tbody>
                                                <SingleLog title="Name" titleClass='min-w-175px w-175px' childClass='min-w-200px' children={entry.more.load_name}/>
                                                <SingleLog title="Vehicle requested">
                                                    <i className={clsx(entry.vehicle_types[0].icon, "fs-2x text-info mr-2")}/>
                                                </SingleLog>
                                                <SingleLog title='Pickup address'>{entry.more.pickup_address}</SingleLog>
                                                <SingleLog title='Destination address'>{entry.more.destination_address}</SingleLog>
                                                <SingleLog title='Receiver phone number'>{entry.more.receiver_phone_number}</SingleLog>
                                                <SingleLog title='Estimated distance'>{entry.more.distance.toFixed(2)+' km'}</SingleLog>
                                                <SingleLog title='Capacity'>{entry.more.capacity}</SingleLog>
                                                <SingleLog title='Description'>{entry.more.load_description}</SingleLog>
                                                <SingleLog title='Date booked'>{datFormat3(entry.created_at)}</SingleLog>
                                            </tbody>
                                        </table>
                                        <br/><br/>
                                        {entry.photos.map((item:any, i:number)=>{
                                            return <img className="mr-11" style={{
                                                width: '160px', height: '160px', objectFit: 'cover', marginLeft: '20px'
                                            }} key={i} src={"/storage/"+item.image_path}/>
                                        })}

                                    </div>
                                    <div className="flex-equal">
                                        <table className="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <tbody>
                                                <SingleLog title="Status"><OrderStatus status={entry.status} /></SingleLog>
                                                <SingleLog title='Estimated Price'><NumberFormat value={entry.more.estimated_price} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /></SingleLog>
                                                {entry.more.original_price && <SingleLog title='Initial Price'><NumberFormat value={entry.more.original_price} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /></SingleLog>}
                                                {entry.more.price && <SingleLog title='Final Price'><NumberFormat value={entry.more.price} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /></SingleLog>}
                                                {entry.transactions && entry.transactions.length > 0 && <>
                                                    <SingleLog title='Date Paid'>{datFormat3(entry.created_at)}</SingleLog>
                                                    <SingleLog title=''><Link href={route('transactions.show', entry.transactions[0].id)} className="btn btn-primary btn-sm">View Invoice</Link></SingleLog>
                                                </>}
                                                {entry.vehicles && entry.vehicles.length > 0 && <>
                                                    {entry.vehicles[0].pivot.created_at && <SingleLog title='Time accepted'>{datFormat3(entry.vehicles[0].pivot.created_at)}</SingleLog>}
                                                    {entry.vehicles[0].pivot.time_arrived && <SingleLog title='Arrived at pickup'>{datFormat3(entry.vehicles[0].pivot.time_arrived)}</SingleLog>}
                                                    {entry.vehicles[0].pivot.time_picked && <SingleLog title='Picked up at'>{datFormat3(entry.vehicles[0].pivot.time_picked)}</SingleLog>}
                                                    {entry.vehicles[0].pivot.time_cancelled && <SingleLog title='Cancelled at'>{datFormat3(entry.vehicles[0].pivot.time_cancelled)}</SingleLog>}
                                                    {entry.vehicles[0].pivot.time_delivered && <SingleLog title='Delivered at'>{datFormat3(entry.vehicles[0].pivot.time_delivered)}</SingleLog>}
                                                    <tr><td colSpan={2}><Link href={route('vehicles.show', entry.vehicles[0].id)} className="btn btn-primary btn-sm">View vehicle</Link></td></tr>
                                                </>}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {entry.prices.length > 0 && <SingleTable title="Negotiated prices">
                        <DataTable columns={priceColumns} data={priceCollection(entry.prices)} />
                    </SingleTable>}
                    {entry.rejected_orders.length > 0 && <SingleTable title="Declined vehicles">
                        <DataTable columns={vehicleColumns} data={vehicleCollection(entry.rejected_orders)} />
                    </SingleTable>}
                </div>
                <div className="flex-column flex-lg-row-auto w-lg-250px w-xl-300px mb-10 order-1 order-lg-2">
                    <div className="card card-flush mb-11" data-kt-sticky="true" data-kt-sticky-name="subscription-summary" data-kt-sticky-offset="{default: false, lg: '200px'}" data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="150px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
                        <div className="card-header">
                            <div className="card-title">
                                <h2>User Details</h2>
                            </div>
                        </div>
                        <div className="card-body pt-0 fs-6">
                            <div className="mb-7">
                                <div className="d-flex align-items-center">
                                    <div className="symbol symbol-60px symbol-circle me-3">
                                        <img alt="Pic" src={entry.user.profile_photo_url} />
                                    </div>
                                    <div className="d-flex flex-column">
                                        <a href="#" className="fs-4 fw-bolder text-gray-900 text-hover-primary me-2">{capitalizeFirstLetter(entry.user.first_name)+ ' ' + capitalizeFirstLetter(entry.user.last_name)}</a>
                                        <a href="#" className="fw-bold text-gray-600 text-hover-primary">{entry.user.email.toLowerCase()}</a>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-0">
                                <Link href={route('users.show', entry.user.id)} className="btn btn-primary" id="kt_subscriptions_create_button">View User</Link>
                            </div>
                        </div>
                    </div>
                    {entry.riders && entry.riders.length > 0 &&
                    <div className="card card-flush mb-11" data-kt-sticky="true" data-kt-sticky-name="subscription-summary" data-kt-sticky-offset="{default: false, lg: '200px'}" data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="150px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
                        <div className="card-header">
                            <div className="card-title">
                                <h2>Rider Details</h2>
                            </div>
                        </div>
                        <div className="card-body pt-0 fs-6">
                            <div className="mb-7">
                                <div className="d-flex align-items-center">
                                    <div className="symbol symbol-60px symbol-circle me-3">
                                        <img alt="Pic" src={entry.riders[0].profile_photo_url} />
                                    </div>
                                    <div className="d-flex flex-column">
                                        <a href="#" className="fs-4 fw-bolder text-gray-900 text-hover-primary me-2">{capitalizeFirstLetter(entry.riders[0].first_name)+ ' ' + capitalizeFirstLetter(entry.riders[0].last_name)}</a>
                                        <a href="#" className="fw-bold text-gray-600 text-hover-primary">{entry.riders[0].email.toLowerCase()}</a>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-0">
                                <Link href={route('users.show', entry.riders[0].id)} className="btn btn-primary" id="kt_subscriptions_create_button">View Rider</Link>
                            </div>
                        </div>
                    </div>}
                </div>
            </div>
        </AdminLayout>
    )
}
export default OrderShow
