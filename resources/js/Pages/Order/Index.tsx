import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { orderColumns, orderCollection } from '../../Accessors/Accessors'

type Props = {
    entries: any,
    title: string
}

const OrderIndex: React.FC<Props> = ({
    entries,
    title
}) => {
    return (
        <AdminLayout title={title} pages={['Dashboard', 'Orders', title+' orders']}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            <DataTable columns={orderColumns} data={orderCollection(entries)} />
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default OrderIndex
