import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { complaintColumns, complaintCollection } from '../../Accessors/Accessors'

type Props = {
    entries: any,
}

const ComplaintIndex: React.FC<Props> = ({
    entries,
}) => {
    return (
        <AdminLayout title={'Complaints'} pages={['Dashboard', 'Complaints']}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            <DataTable columns={complaintColumns} data={complaintCollection(entries)} />
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default ComplaintIndex
