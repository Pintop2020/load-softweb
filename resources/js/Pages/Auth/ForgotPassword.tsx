import React, { useState} from 'react'
import { useForm } from '@inertiajs/inertia-react'
import { AuthLayout } from '../../Layouts/Layouts'
import CircularProgress from '@mui/material/CircularProgress'
import route from 'ziggy-js'

type Props = {
    status: any
}

interface IErrors {
    email: any,
}

const ForgotPassword: React.FC<Props> = ({status}) => {
    const {data, setData, post, processing, errors} = useForm({
        email: '',
    })
    const [signinErrors, setSigninErrors] = useState<IErrors>({
        email: null,
    })
    const validateInput = () => {
        const validMail = /^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        const errorsInit: any = {}
        let fields: any = { ...data }

        if (fields.email && !fields.email.match(validMail)) {
            errorsInit.email = "Please enter a valid email address"
        }
        setSigninErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...signinErrors}
        erroSign[target.name] = null
        setSigninErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post(route('password.email'))
        }
    }
    return (
        <AuthLayout title={'Forget Password'}>
            <form onSubmit={handleSubmit} className="form w-100" id="kt_sign_in_form">
                <div className="text-center mb-10">
                    <h1 className="text-dark mb-3">Forgot your password?</h1>
                </div>
                <p className="text-muted text-center mb-4 mt-3">No problem!!! Just let us know your email address and we will email you a password reset link that will allow you to choose a new one..</p>
                {status && <div className="mb-4 font-medium text-sm text-success">{status}</div>}
                <div className="fv-row mb-10">
                    <label className="form-label fs-6 fw-bolder text-dark required">Email</label>
                    <input className="form-control form-control-lg form-control-solid" type="email" name="email" value={data.email} onChange={handleChange} disabled={processing} />
                    {errors.email && <div className="text-danger">{errors.email}</div>}
                    {signinErrors.email && <div className="text-danger">{signinErrors.email}</div>}
                </div>
                <div className="text-center">
                    <button disabled={processing} type="submit" id="kt_sign_in_submit" className={["btn btn-lg btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Continue</span> }
                    </button>
                </div>
            </form>
        </AuthLayout>
    )
}
export default ForgotPassword
