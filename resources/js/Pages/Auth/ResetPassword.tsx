import React, { useState} from 'react'
import { useForm } from '@inertiajs/inertia-react'
import { AuthLayout } from '../../Layouts/Layouts'
import CircularProgress from '@mui/material/CircularProgress'
import route from 'ziggy-js'

type Props = {
    token: boolean
}

interface IErrors {
    email: any,
    password: any,
    password_confirmation: any
}

function getEmail() {
    const queryString = window.location.search
    const urlParams = new URLSearchParams(queryString)
    return urlParams.get('email')
}

const ResetPassword: React.FC<Props> = ({token}) => {
    let email: any = getEmail()!.toString()
    const {data, setData, post, processing, errors} = useForm({
        token: token,
        email: email,
        password: '',
        password_confirmation: '',
    })
    const [signinErrors, setSigninErrors] = useState<IErrors>({
        email: null,
        password: null,
        password_confirmation: null
    })
    const validateInput = () => {
        const validMail = /^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required"
            }
            if (fields.email && !fields.email.match(validMail)) {
                errorsInit.email = "Please enter a valid email address"
            }
            if (fields.password && fields.password.length < 8) {
                errorsInit.password = "Password must be at least 8 characters"
              }
            if (fields.password_confirmation &&
                fields.password_confirmation !== fields.password) {
                errorsInit.password_confirmation = "Your password does not match"
            }
        }
        setSigninErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...signinErrors}
        erroSign[target.name] = null
        setSigninErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post(route('password.update'))
        }
    }
    return (
        <AuthLayout title={'Reset Password'}>
            <form onSubmit={handleSubmit} className="form w-100" id="kt_sign_in_form">
                <div className="text-center mb-10">
                    <h1 className="text-dark mb-3">Reset Password</h1>
                </div>
                <div className="fv-row mb-10">
                    <label className="form-label fs-6 fw-bolder text-dark">Email</label>
                    <input className="form-control form-control-lg form-control-solid" type="email" name="email" value={data.email} onChange={handleChange} disabled={processing} />
                    {errors.email && <div className="text-danger">{errors.email}</div>}
                    {signinErrors.email && <div className="text-danger">{signinErrors.email}</div>}
                </div>
                <div className="fv-row mb-10">
                    <label className="form-label fw-bolder text-dark fs-6 mb-0">New Password</label>
                    <input type="password" name="password" className="form-control form-control-lg form-control-solid" value={data.password} onChange={handleChange} disabled={processing} />
                    {errors.password && <div className="text-danger">{errors.password}</div>}
                    {signinErrors.password && <div className="text-danger">{signinErrors.password}</div>}
                </div>
                <div className="fv-row mb-10">
                    <label className="form-label fw-bolder text-dark fs-6 mb-0">Confirm Password</label>
                    <input type="password" name="password_confirmation" className="form-control form-control-lg form-control-solid" value={data.password_confirmation} onChange={handleChange} disabled={processing} />
                    {signinErrors.password_confirmation && <div className="text-danger">{signinErrors.password_confirmation}</div>}
                </div>
                <div className="text-center">
                    <button disabled={processing} type="submit" id="kt_sign_in_submit" className={["btn btn-lg btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Continue</span> }
                    </button>
                </div>
            </form>
        </AuthLayout>
    )
}
export default ResetPassword
