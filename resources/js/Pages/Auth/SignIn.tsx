import React, { useState} from 'react'
import { Link, useForm } from '@inertiajs/inertia-react'
import { AuthLayout } from '../../Layouts/Layouts'
import CircularProgress from '@mui/material/CircularProgress'
import route from 'ziggy-js'

type Props = {
    canResetPassword: boolean
}

interface IErrors {
    email: any,
    password: any,
}

const Login: React.FC<Props> = ({canResetPassword}) => {
    const {data, setData, post, processing, errors} = useForm({
        email: '',
        password: '',
        remember: false,
    })
    const [signinErrors, setSigninErrors] = useState<IErrors>({
        email: null,
        password: null,
    })
    const validateInput = () => {
        const validMail = /^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'remember') {
                errorsInit[key] = "This field is required"
            }
            if (fields.email && !fields.email.match(validMail)) {
                errorsInit.email = "Please enter a valid email address"
            }
            if (fields.password && fields.password.length < 8) {
                errorsInit.password = "Password must be at least 8 characters"
            }
        }
        setSigninErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value = target.type === 'checkbox'
            ? target.checked
            : target.value
        setData(target.name, value)
        let erroSign: any = {...signinErrors}
        erroSign[target.name] = null
        setSigninErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('login')
        }
    }
    return (
        <AuthLayout title={'Sign In'}>
            <form onSubmit={handleSubmit} className="form w-100" id="kt_sign_in_form">
                <div className="text-center mb-10">
                    <h1 className="text-dark mb-3">Sign In to Dashboard</h1>
                </div>
                <div className="fv-row mb-10">
                    <label className="form-label fs-6 fw-bolder text-dark">Email</label>
                    <input className="form-control form-control-lg form-control-solid" type="email" name="email" value={data.email} onChange={handleChange} disabled={processing} />
                    {errors.email && <div className="text-danger">{errors.email}</div>}
                    {signinErrors.email && <div className="text-danger">{signinErrors.email}</div>}
                </div>
                <div className="fv-row mb-10">
                    <div className="d-flex flex-stack mb-2">
                        <label className="form-label fw-bolder text-dark fs-6 mb-0">Password</label>
                        {canResetPassword && <Link href={route('password.request')} className="link-primary fs-6 fw-bolder">Forgot Password ?</Link>}
                    </div>
                    <input type="password" name="password" className="form-control form-control-lg form-control-solid" value={data.password} onChange={handleChange} disabled={processing} />
                    {errors.password && <div className="text-danger">{errors.password}</div>}
                    {signinErrors.password && <div className="text-danger">{signinErrors.password}</div>}
                </div>
                <div className="text-center">
                    <button disabled={processing} type="submit" id="kt_sign_in_submit" className={["btn btn-lg btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Continue</span> }
                    </button>
                </div>
            </form>
        </AuthLayout>
    )
}
export default Login
