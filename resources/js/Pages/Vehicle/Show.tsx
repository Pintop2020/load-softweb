import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable } from '../../Components/Components'
import { VehicleStatus, VehicleStatus2, VehicleBlacklisted, orderColumns, orderCollection, complaintColumns, complaintCollection } from '../../Accessors/Accessors'
import { Link, usePage } from '@inertiajs/inertia-react'
import route from 'ziggy-js'
import { capitalizeFirstLetter, datFormat2 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import {NumberFormat, Swal} from '../../Reuseable/plugins'
import { checkPermission } from '../../Reuseable/app_config'
import { ProcessVehicle } from './Process'
import { Inertia } from '@inertiajs/inertia'

type Props = {
    entry: any,
}

type LogProps = {
    title: string,
    children?: any,
    titleClass?: string,
    childClass?: string
}

const SingleLog: FC<LogProps> = ({
    title,
    children,
    titleClass,
    childClass
}) => {
    return (
        <tr>
            <td className={clsx('text-gray-400', titleClass && titleClass)}>{title}:</td>
            <td className={clsx('text-gray-800', childClass && childClass)}>{children}</td>
        </tr>
    )
}

type TableProps = {
    title: string,
    children?: any
}

const SingleTable: FC<TableProps> = ({
    title,
    children,
}) => {
    return (
        <div className="card card-flush pt-3 mb-5 mb-xl-10">
            <div className="card-header">
                <div className="card-title">
                    <h2>{title}</h2>
                </div>
            </div>
            <div className="card-body pt-0">
                {children}
            </div>
        </div>
    )
}

const VehicleShow: FC<Props> = ({
    entry,
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <AdminLayout title="Viewing Vehicle" pages={['Dashboard', 'Trucks', entry.is_available ? 'Available trucks':'Unavailable trucks', capitalizeFirstLetter(entry.vehicle_types[0].name)+' details']}>
            <div className="d-flex flex-column flex-lg-row">
                <div className="flex-lg-row-fluid me-lg-15 order-2 order-lg-1 mb-10 mb-lg-0">
                    <div className="card card-flush pt-3 mb-5 mb-xl-10">
                        <div className="card-header">
                            <div className="card-title">
                                <h2 className="fw-bolder">Vehicle Details</h2>
                            </div>
                            <div className="card-toolbar">
                                {checkPermission(sessionData.user, 'approve vehicles') && entry.status == 'pending' &&
                                    <ProcessVehicle entry={entry} />
                                }
                                {checkPermission(sessionData.user, 'blacklist vehicles') && <>{!entry.is_blacklisted ?
                                    <Link href={"#"} className="btn btn-light-danger" onClick={(e)=>{
                                        e.preventDefault()
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: "",
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Proceed'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                Inertia.get("/vehicles/blaclist/add/"+entry.id, {
                                                    preserveState: false
                                                })
                                            }
                                        })
                                    }}>Blacklist vehicle</Link> :
                                    <Link href={"#"} onClick={(e)=>{
                                        e.preventDefault()
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: "",
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Proceed'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                Inertia.get("/vehicles/blaclist/remove/"+entry.id, {
                                                    preserveState: false
                                                })
                                            }
                                        })
                                    }} className="btn btn-light-success">Remove from blacklists</Link>}</>
                                }
                            </div>
                        </div>
                        <div className="card-body pt-3">
                            <div className="mb-10">
                                <h5 className="mb-4"></h5>
                                <div className="d-flex flex-wrap py-5">
                                    <div className="flex-equal me-5">
                                        <table className="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <tbody>
                                                <SingleLog title="Name" titleClass='min-w-175px w-175px' childClass='min-w-200px' children={entry.name}/>
                                                <SingleLog title="Type">
                                                    <i className={clsx(entry.vehicle_types[0].icon, "fs-2x text-info mr-2")}/>
                                                </SingleLog>
                                                <SingleLog title='Color'>{entry.more.color}</SingleLog>
                                                <SingleLog title='Availability'><VehicleStatus isAvailable={entry.is_available}/></SingleLog>
                                                <SingleLog title='Status'><VehicleStatus2 status={entry.status}/></SingleLog>
                                                {entry.is_blacklisted && <SingleLog title=''><VehicleBlacklisted isBlaclisted={entry.is_blacklisted}/></SingleLog>}
                                                {entry.status == 'rejected' && <SingleLog title='Reason for rejecting'><span className="text-muted">{entry.notes}</span></SingleLog>}
                                                {entry.is_blacklisted && <SingleLog title='Reason for blcklisting'><span className="text-muted">{entry.notes}</span></SingleLog>}
                                                <SingleLog title='Full Capacity Price Per Kilometre'><NumberFormat value={entry.more.capacity_per_kilometre_price} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} /></SingleLog>
                                                <SingleLog title='Front Shot'><img width="50px" src={"/storage/"+entry.more.front_shot}/></SingleLog>
                                                <SingleLog title='Back Shot'><img width="50px" src={"/storage/"+entry.more.back_shot}/></SingleLog>
                                                <SingleLog title='Side Shot'><img width="50px" src={"/storage/"+entry.more.side_shot}/></SingleLog>
                                                <SingleLog title='Interior Shot'><img width="50px" src={"/storage/"+entry.more.interior_shot}/></SingleLog>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="flex-equal">
                                        <table className="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <tbody>
                                                <SingleLog title='Current Longitude'>{entry.current_longitude}</SingleLog>
                                                <SingleLog title='Current Latitude'>{entry.current_latitude}</SingleLog>
                                                <SingleLog title='Current Address'>{entry.current_address}</SingleLog>
                                                <SingleLog title='Plate number'>{entry.more.plate_number}</SingleLog>
                                                <SingleLog title='Documents'><Link href={"/storage/"+entry.more.documents} className="btn btn-sm btn-primary">Download documents</Link></SingleLog>
                                                <SingleLog title='Date added'>{datFormat2(entry.created_at)}</SingleLog>
                                                <SingleLog title='Date last updated'>{datFormat2(entry.updatedd_at)}</SingleLog>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {entry.orders.length > 0 && <SingleTable title="Orders">
                        <DataTable columns={orderColumns} data={orderCollection(entry.orders)} />
                    </SingleTable>}
                    {entry.rejected_orders.length > 0 && <SingleTable title="Rejected orders">
                        <DataTable columns={orderColumns} data={orderCollection(entry.rejected_orders)} />
                    </SingleTable>}
                    {entry.complaints.length > 0 && <SingleTable title="Submitted Complaints">
                        <DataTable columns={complaintColumns} data={complaintCollection(entry.complaints)} />
                    </SingleTable>}
                </div>
                <div className="flex-column flex-lg-row-auto w-lg-250px w-xl-300px mb-10 order-1 order-lg-2">
                    <div className="card card-flush mb-0" data-kt-sticky="true" data-kt-sticky-name="subscription-summary" data-kt-sticky-offset="{default: false, lg: '200px'}" data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="150px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
                        <div className="card-header">
                            <div className="card-title">
                                <h2>Rider Details</h2>
                            </div>
                        </div>
                        <div className="card-body pt-0 fs-6">
                            <div className="mb-7">
                                <div className="d-flex align-items-center">
                                    <div className="symbol symbol-60px symbol-circle me-3">
                                        <img alt="Pic" src={entry.user.profile_photo_url} />
                                    </div>
                                    <div className="d-flex flex-column">
                                        <a href="#" className="fs-4 fw-bolder text-gray-900 text-hover-primary me-2">{capitalizeFirstLetter(entry.user.first_name)+ ' ' + capitalizeFirstLetter(entry.user.last_name)}</a>
                                        <a href="#" className="fw-bold text-gray-600 text-hover-primary">{entry.user.email.toLowerCase()}</a>
                                    </div>
                                </div>
                            </div>
                            <div className="mb-0">
                                {checkPermission(sessionData.user, "view users") && <Link href={route('users.show', entry.user.id)} className="btn btn-primary">View Rider</Link>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default VehicleShow
