import React, { FC, useState} from 'react'
import { Link, useForm } from '@inertiajs/inertia-react'
import { Form, Col, Row, Modal } from 'react-bootstrap'

type Props = {
    entry: any
}

export const ProcessVehicle: FC<Props> = ({
    entry
}) => {
    const [showModal, setShowModal] = useState(false)
    const {data, setData, post, processing} = useForm({
        entry_id: entry.id,
        notes: '',
        status: 'pending'
    })
    const [formErrors, setFormErrors] = useState({
        notes: null,
        status: null
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'notes') {
                errorsInit[key] = "This field is required"
            }
            if(fields.status == 'rejected' && !fields.notes){
                errorsInit.notes = "Please enter reason for rejecting this truck"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget;
        const value = target.value;
        setData(target.name, value);
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign);
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/vehicles/process/validate', {
                preserveState: false
            })
        }
    }
    return (
        <>
            <Modal show={showModal} centered onHide={()=>{ setShowModal(false) }}>
                <Form onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Approve / Reject Truck
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col lg={12} className="mt-2">
                                <Form.Label>Status</Form.Label>
                                <Form.Select name='status' value={data.status} onChange={handleChange} disabled={processing}>
                                    <option value='pending'>Pending</option>
                                    <option value='approved'>Approve</option>
                                    <option value='rejected'>Reject</option>
                                </Form.Select>
                                {formErrors.status && <Form.Text className="text-danger">{formErrors.status}</Form.Text>}
                            </Col>
                            {data.status == 'rejected' && <Col lg={12} className="mt-2">
                                <Form.Label>Reason</Form.Label>
                                <Form.Control as={'textarea'} cols={3} name='notes' value={data.notes} onChange={handleChange} disabled={processing}/>
                                {formErrors.notes && <Form.Text className="text-danger">{formErrors.notes}</Form.Text>}
                            </Col>}
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary mt-2" type="submit" disabled={processing}>{processing ? ("updating order......") : ("Update Order")}</button>
                    </Modal.Footer>
                </Form>
            </Modal>
            <Link href="#" className="btn btn-light-warning" onClick={(e)=>{ if(!processing){ e.preventDefault(); setShowModal(true) } }}>Approve / Reject Truck</Link>
        </>
    )
}
