import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { vehicleColumns, vehicleCollection } from '../../Accessors/Accessors'

type Props = {
    entries: any,
    title: string
}

const VehicleIndex: React.FC<Props> = ({
    entries,
    title
}) => {
    return (
        <AdminLayout title={title} pages={['Dashboard', 'Vehicles', title+' vehicles']}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            <DataTable columns={vehicleColumns} data={vehicleCollection(entries)} />
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default VehicleIndex
