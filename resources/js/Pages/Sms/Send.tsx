import React, { FC, useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Form, Card } from 'react-bootstrap'
import { Select } from '../../Reuseable/plugins'

type Props = {
    users: string,
}

interface IErrors {
    message?: any,
    users?: any,
}

function getUsers(users: any) {
    var resps: any = []
    users.map((item: any)=>{
        resps.push({value: item.phone_numbe, label: item.first_name+" -- "+item.email+" - "+item.phone_number})
    })
    return resps
}

const SendSms: FC<Props> = ({
    users,
}) => {
    const {data, setData, post, processing} = useForm({
        message: '',
        users: [],
        send_all: false
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        message: null,
        users: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'send_all') {
                errorsInit[key] = "This field is required"
            }
            if(!fields.send_all && fields.users.length < 1){
                errorsInit.users = "Please select at least a user to send sms to"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        setData(target.name, value)
        let errorSign: any = {...formErrors}
        errorSign[target.name] = null
        setFormErrors(errorSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/send_sms', {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title="Send SMS" pages={['Dashboard', 'SMS']}>
            <Card>
                <Card.Header>
                    <Card.Title>Send SMS</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Form onSubmit={handleSubmit}>
                        <Form.Group className="mb-3">
                            <div className="form-check form-check-custom form-check-solid">
                                <input className="form-check-input" type="checkbox" name="send_all" checked={data.send_all} onChange={handleChange} id="flexCheckDefault"/>
                                <label className="form-check-label" htmlFor="flexCheckDefault">
                                Send to all users
                                </label>
                            </div>
                        </Form.Group>
                        {!data.send_all && <Form.Group>
                            <Form.Label>Users</Form.Label>
                            <Select options={getUsers(users)} closeMenuOnSelect={false} isMulti name="users" onChange={(selected)=>{
                                // @ts-ignore
                                setData({...data, users: Array.isArray(selected) ? selected.map(x => x.value) : []});
                            }} />
                            {formErrors.users && <Form.Text className="text-danger">{formErrors.users}</Form.Text>}
                        </Form.Group>}
                        <Form.Group>
                            <Form.Label>Message</Form.Label>
                            <Form.Control name="message" as="textarea" cols={3} value={data.message} onChange={handleChange} disabled={processing} />
                            {formErrors.message && <Form.Text className="text-danger">{formErrors.message}</Form.Text>}
                        </Form.Group>
                        <button type="submit" className="btn btn-sm btn-primary mt-2" disabled={processing}>{processing ? "Sending Message":"Send Message"}</button>
                    </Form>
                </Card.Body>
            </Card>
        </AdminLayout>
    )
}
export default SendSms
