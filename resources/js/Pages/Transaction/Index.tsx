import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { transactionColumns, transactionCollection } from '../../Accessors/Accessors'

type Props = {
    entries: any,
    title: string
}

const TransactionIndex: React.FC<Props> = ({
    entries,
    title
}) => {
    return (
        <AdminLayout title={title + ' Transaction'} pages={['Dashboard', 'Transactions', title+' transactions']}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            <DataTable columns={transactionColumns} data={transactionCollection(entries)} />
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default TransactionIndex
