import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { TranStatus } from '../../Accessors/Accessors'
import { checkPermission, logoFile, mainSite } from '../../Reuseable/app_config'
import { capitalizeFirstLetter, datFormat3 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import { Row } from 'react-bootstrap'
import {NumberFormat} from '../../Reuseable/plugins'
import route from 'ziggy-js'
import { Link, usePage } from '@inertiajs/inertia-react'

type Props = {
    entry: any,
    title: any
}

type LogProps = {
    title: string,
    children?: any,
    className?: string
}

const SingleLog: FC<LogProps> = ({
    children,
    title,
    className
}) => {
    return (
        <div className="col-sm-6">
            <div className="fw-bold fs-7 text-gray-600 mb-1">{title}:</div>
            <div className={clsx("fw-bolder fs-6 text-gray-800", className && className)}>{children}</div>
        </div>
    )
}

const SingleLogSimple: FC<LogProps> = ({
    children,
    title,
    className
}) => {
    return (
        <div className="mb-6">
            <div className="fw-bold text-gray-600 fs-7">{title}:</div>
            <div className={clsx("fw-bolder text-gray-800 fs-6", className && className)}>{children}</div>
        </div>
    )
}

const TransactionShow: FC<Props> = ({
    entry,
    title,
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <AdminLayout title={'View Invoice'} pages={['Dashboard', 'Transactions', title+' transactions', 'View invoice']}>
            <div className="card">
                <div className="card-body p-lg-20">
                    <div className="d-flex flex-column flex-xl-row">
                        <div className="flex-lg-row-fluid me-xl-18 mb-10 mb-xl-0">
                            <div className="mt-n1">
                                <div className="d-flex flex-stack pb-10">
                                    <a href={mainSite}> <img alt="Logo" src={logoFile} className="h-40px" /> </a>
                                    {entry.orders.length > 0 && checkPermission(sessionData.user, "view orders") && <Link href={route('orders.show', entry.orders[0].id)} className="btn btn-sm btn-success mr-2">View Order</Link>}
                                    {entry.payouts.length > 0 && checkPermission(sessionData.user, "view payouts") && <Link href={route('payouts.show', entry.payouts[0].id)} className="btn btn-sm btn-success mr-2">View Payout</Link>}
                                </div>
                                <div className="m-0">
                                    <div className="fw-bolder fs-3 text-gray-800 mb-8">Invoice #{entry.id.toString().padStart(6, "0")}</div>
                                    <Row className="g-5 mb-11">
                                        <SingleLog title="Date paid" children={datFormat3(entry.created_at)}/>
                                        <SingleLog title="Amount">
                                            {entry.more.type == 'debit' && <span className="text-danger"><NumberFormat value={entry.more.amount} displayType={"text"} thousandSeparator={true} prefix={"- "} suffix=" NGN" decimalSeparator="." decimalScale={2} /></span>}
                                            {entry.more.type == 'credit' && <span className="text-success"><NumberFormat value={entry.more.amount} displayType={"text"} thousandSeparator={true} prefix={"+ "} suffix=" NGN" decimalSeparator="." decimalScale={2} /></span>}
                                        </SingleLog>
                                        {entry.more.others.fee != null && <SingleLog title="Transaction Fee">
                                            <span className="text-danger"><NumberFormat value={entry.more.others.fee} displayType={"text"} thousandSeparator={true} prefix={"- "} suffix=" NGN" decimalSeparator="." decimalScale={2} /></span>
                                        </SingleLog>}
                                        <SingleLog title="Naration" children={entry.more.description}/>
                                        {entry.coupons.length > 0 && <>
                                            <SingleLog title="Coupon">
                                                <span className="text-success"><NumberFormat value={entry.coupons[0].pivot.amount_discounted} displayType={"text"} thousandSeparator={true} prefix={"+ "} suffix=" NGN" decimalSeparator="." decimalScale={2} /></span><br/>
                                                <Link href={route('coupons.show', entry.coupons[0].id)} className="btn btn-sm btn-info">View Coupon</Link>
                                            </SingleLog>
                                        </>}
                                    </Row>
                                    <Row className="g-5 mb-12">
                                        <SingleLog title="Issued For: ">
                                            <div className="text-gray-800">{capitalizeFirstLetter(entry.user.first_name)+' '+capitalizeFirstLetter(entry.user.last_name)}</div><div className="text-muted">{entry.user.email.toLowerCase()}</div><Link href={route('users.show', entry.user.id)} className="btn btn-sm btn-primary mr-2">View Customer</Link>
                                        </SingleLog>
                                        <SingleLog title="Reference">
                                            <div className="text-primary">{entry.more.reference}</div>
                                        </SingleLog>
                                    </Row>
                                </div>
                            </div>
                        </div>
                        {entry.orders.length > 0 && <div className="m-0">
                            <div className="d-print-none border border-dashed border-gray-300 card-rounded h-lg-100 min-w-md-350px p-9 bg-lighten">
                                <div className="mb-8">
                                    <TranStatus status={entry.more.status} />
                                </div>
                                <h6 className="mb-8 fw-boldest text-gray-600 text-hover-primary">ORDER DETAILS</h6>
                                <SingleLogSimple title="Order Name" children={entry.orders[0].more.load_name}/>
                                <SingleLogSimple title="Pickup Address" children={entry.orders[0].more.pickup_address}/>
                                <SingleLogSimple title="Destination Address" children={entry.orders[0].more.destination_address}/>
                                <SingleLogSimple title="Distance" children={entry.orders[0].more.estimated_distance+" km"}/>
                                <h6 className="mb-8 fw-boldest text-gray-600 text-hover-primary">VEHICLE DETAILS</h6>
                                <SingleLogSimple title="Name" children={entry.orders[0].vehicles[0].name} />
                                {checkPermission(sessionData.user, "view vehicles") && <Link href={route('vehicles.show', entry.orders[0].vehicles[0].id)} className="btn btn-sm btn-info mr-2">View Vehicle</Link>}
                            </div>
                        </div>}
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default TransactionShow
