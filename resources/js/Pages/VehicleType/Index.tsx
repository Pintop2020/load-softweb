import React from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { vehicleTypeColumns, vehicleTypeCollection } from '../../Accessors/Accessors'
import { Link, usePage } from '@inertiajs/inertia-react'
import route from 'ziggy-js'
import { checkPermission } from '../../Reuseable/app_config'

type Props = {
    entries: any,
}

const VehicleTypeIndex: React.FC<Props> = ({
    entries,
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    return (
        <AdminLayout title={"Truck Type"} pages={['Dashboard', 'Truck Type']} breadAction={checkPermission(sessionData.user, 'create vehicle types') && <Link href={route('vehicle_types.create')} className="btn btn-sm btn-primary">Add Type</Link>}>
            <div className="row gy-5 g-xl-8">
			    <div className="col-xxl-12">
                    <Filter />
                </div>
                <div className="col-xxl-12">
                    <div className="card">
                        <div className="card-body pt-0">
                            <DataTable columns={vehicleTypeColumns} data={vehicleTypeCollection(entries)} />
                        </div>
                    </div>
                </div>
            </div>
        </AdminLayout>
    )
}
export default VehicleTypeIndex
