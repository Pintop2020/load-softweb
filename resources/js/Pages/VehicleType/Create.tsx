import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Col, Form, Row, Card } from 'react-bootstrap'
import { CircularProgress } from '@mui/material'

interface IErrors {
    name: any,
    icon: any,
    estimated_capacity_per_kilometre_price: any,
}

const VehicleTypeCreate = () => {
    const {data, setData, post, processing} = useForm({
        name: '',
        icon: '',
        estimated_capacity_per_kilometre_price: ''
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        name: null,
        icon: null,
        estimated_capacity_per_kilometre_price: ''
    })
    const validateInput = () => {
        const errorsInit: any = {};
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required";
            }
        }
        setFormErrors(errorsInit);
        if (Object.entries(errorsInit).length === 0) {
            return true;
        } else {
            return false;
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget;
        const value = target.value;
        setData(target.name, value);
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign);
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault();
        const validated = validateInput();
        if (validated) {
            post('/vehicle_types', {
                preserveState: false
            });
        }
    }
    return (
        <AdminLayout title={'Add truck type'} pages={['Dashboard', 'Truck types', 'Add truck type']}>
            <div className="row gy-5 g-xl-8">
                <div className="col-xxl-12">
                    <Card>
                        <Card.Header>
                            <Card.Title>Add new type</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={handleSubmit} className="">
                                <Row>
                                    <Col sm={12} md={4} lg={4}>
                                        <Form.Group>
                                            <Form.Label className="required">Name</Form.Label>
                                            <Form.Control name="name" type="text" value={data.name} onChange={handleChange} disabled={processing} />
                                            {formErrors.name && <Form.Text className="text-danger">{formErrors.name}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={4} lg={4}>
                                        <Form.Group>
                                            <Form.Label className="required">Icon Class <small className="text-muted">(FontAwesome, Bootstrap Icon)</small></Form.Label>
                                            <Form.Control name="icon" type="text" value={data.icon} onChange={handleChange} disabled={processing} />
                                            {formErrors.icon && <Form.Text className="text-danger">{formErrors.icon}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={4} lg={4}>
                                        <Form.Group>
                                            <Form.Label className="required">Estimated price per kilometre</Form.Label>
                                            <Form.Control name="estimated_capacity_per_kilometre_price" type="number" value={data.estimated_capacity_per_kilometre_price} onChange={handleChange} disabled={processing} />
                                            {formErrors.estimated_capacity_per_kilometre_price && <Form.Text className="text-danger">{formErrors.estimated_capacity_per_kilometre_price}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-4">
                                    <button disabled={processing} type="submit" className={["btn btn-sm btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }
                                    </button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </AdminLayout>
    )
}
export default VehicleTypeCreate
