import React, { FC } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable } from '../../Components/Components'
import { vehicleColumns, vehicleCollection } from '../../Accessors/Accessors'
import { Link } from '@inertiajs/inertia-react'
import route from 'ziggy-js'
import { capitalizeFirstLetter, datFormat2 } from '../../Reuseable/number_utils'
import clsx from 'clsx'
import {NumberFormat, Swal} from '../../Reuseable/plugins'
import { Inertia } from '@inertiajs/inertia'

type Props = {
    entry: any,
}

type LogProps = {
    title: string,
    children?: any,
    titleClass?: string,
    childClass?: string
}

const SingleLog: FC<LogProps> = ({
    title,
    children,
    titleClass,
    childClass
}) => {
    return (
        <tr>
            <td className={clsx('text-gray-400', titleClass && titleClass)}>{title}:</td>
            <td className={clsx('text-gray-800', childClass && childClass)}>{children}</td>
        </tr>
    )
}

type TableProps = {
    title: string,
    children?: any
}

const SingleTable: FC<TableProps> = ({
    title,
    children,
}) => {
    return (
        <div className="card card-flush pt-3 mb-5 mb-xl-10">
            <div className="card-header">
                <div className="card-title">
                    <h2>{title}</h2>
                </div>
            </div>
            <div className="card-body pt-0">
                {children}
            </div>
        </div>
    )
}

const VehicleTypeShow: FC<Props> = ({
    entry,
}) => {
    return (
        <AdminLayout title="Viewing Truck Type" pages={['Dashboard', 'Truck Types', capitalizeFirstLetter(entry.name)+' details']}>
            <div className="d-flex flex-column flex-lg-row">
                <div className="flex-lg-row-fluid me-lg-15 order-2 order-lg-1 mb-10 mb-lg-0">
                    <div className="card card-flush pt-3 mb-5 mb-xl-10">
                        <div className="card-header">
                            <div className="card-title">
                                <h2 className="fw-bolder">Truck Details</h2>
                            </div>
                            <div className="card-toolbar">
                                <Link href="#" className="btn btn-light-danger mr-5" onClick={(e)=>{
                                    e.preventDefault()
                                    Swal.fire({
                                        title: 'Are you sure?',
                                        text: "",
                                        icon: 'warning',
                                        showCancelButton: true,
                                        confirmButtonColor: '#3085d6',
                                        cancelButtonColor: '#d33',
                                        confirmButtonText: 'Proceed'
                                    }).then((result: any) => {
                                        if (result.isConfirmed) {
                                            Inertia.delete(route('vehicle_types.destroy', entry.id), {
                                                preserveState: false
                                            })
                                        }
                                    })
                                }}>Delete</Link>
                                <Link href={route('vehicle_types.edit', entry.id)} className="btn btn-light-info mr-5">Edit</Link>
                            </div>
                        </div>
                        <div className="card-body pt-3">
                            <div className="mb-10">
                                <h5 className="mb-4"></h5>
                                <div className="d-flex flex-wrap py-5">
                                    <div className="flex-equal me-5">
                                        <table className="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <tbody>
                                                <SingleLog title="Name" titleClass='min-w-175px w-175px' childClass='min-w-200px' children={entry.name}/>
                                                <SingleLog title="Icon">
                                                    <i className={clsx(entry.icon, "fs-2x text-info mr-2")}/>
                                                </SingleLog>
                                                <SingleLog title="Trucks">
                                                    {entry.vehicles.length + " trucks"}
                                                </SingleLog>
                                                <SingleLog title="Estimated Price/KM">
                                                    <NumberFormat value={entry.estimated_capacity_per_kilometre_price} displayType={'text'} thousandSeparator={true} prefix={'₦'} decimalSeparator='.' decimalScale={2} />
                                                </SingleLog>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div className="flex-equal">
                                        <table className="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <tbody>
                                                <SingleLog title='Date added'>{datFormat2(entry.created_at)}</SingleLog>
                                                <SingleLog title='Date last updated'>{datFormat2(entry.updatedd_at)}</SingleLog>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {entry.vehicles.length > 0 && <SingleTable title="Trucks">
                        {/* <DataTable columns={vehicleColumns} data={vehicleCollection(entry.vehicles)} /> */}
                    </SingleTable>}
                </div>
            </div>
        </AdminLayout>
    )
}
export default VehicleTypeShow
