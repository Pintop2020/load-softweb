import React, { useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { useForm } from '@inertiajs/inertia-react'
import { Col, Form, Row, Card } from 'react-bootstrap'
import { Select } from '../../Reuseable/plugins'
import { CircularProgress } from '@mui/material'

type Props = {
    permissions: any,
}

interface IErrors {
    name: any,
    permissions: any,
}

function getPermissions(permissions: any){
    var resps: any = []
    permissions.map((item: any)=>{
        resps.push({value: item.id, label: item.name})
    })
    return resps
}

const RoleCreate: React.FC<Props> = ({
    permissions,
}) => {
    const {data, setData, post, processing} = useForm({
        name: '',
        permissions: [],
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        name: null,
        permissions: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: any) => {
        const target: any = e.currentTarget
        const value = target.value
        setData(target.name, value)
        let erroSign: any = {...formErrors}
        erroSign[target.name] = null
        setFormErrors(erroSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/roles', {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title={'Add role'} pages={['Dashboard', 'Roles', 'Add role']}>
            <div className="row gy-5 g-xl-8">
                <div className="col-xxl-12">
                    <Card>
                        <Card.Header>
                            <Card.Title>Add new role</Card.Title>
                        </Card.Header>
                        <Card.Body>
                            <Form onSubmit={handleSubmit} className="">
                                <Row>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Name</Form.Label>
                                            <Form.Control name="name" type="text" value={data.name} onChange={handleChange} disabled={processing} />
                                            {formErrors.name && <Form.Text className="text-danger">{formErrors.name}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                    <Col sm={12} md={6} lg={6}>
                                        <Form.Group>
                                            <Form.Label className="required fs-6 fw-bold mb-2">Permissions</Form.Label>
                                            <Select options={getPermissions(permissions)} closeMenuOnSelect={false} isMulti name="permissions" onChange={(selected)=>{
                                                // @ts-ignore
                                                setData({...data, permissions: Array.isArray(selected) ? selected.map(x => x.value) : []})
                                            }} />
                                            {formErrors.permissions && <Form.Text className="text-danger">{formErrors.permissions}</Form.Text>}
                                        </Form.Group>
                                    </Col>
                                </Row>
                                <div className="text-center mt-4">
                                    <button disabled={processing} type="submit" className={["btn btn-sm btn-primary mb-5", processing ? "":" w-100"].join("")}> {processing ? <><CircularProgress color="inherit" /></> : <span className="indicator-label">Submit</span> }
                                    </button>
                                </div>
                            </Form>
                        </Card.Body>
                    </Card>
                </div>
            </div>
        </AdminLayout>
    )
}
export default RoleCreate
