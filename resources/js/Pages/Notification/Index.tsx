import React, { FC, useState } from 'react'
import { AdminLayout } from '../../Layouts/Layouts'
import { DataTable, Filter } from '../../Components/Components'
import { notificationColumns, notificationCollection } from '../../Accessors/Accessors'
import { useForm, usePage } from '@inertiajs/inertia-react'
import { Row, Col, Modal, Form } from 'react-bootstrap'
import { capitalizeFirstLetter } from '../../Reuseable/number_utils'
import { checkPermission } from '../../Reuseable/app_config'
import { Select } from '../../Reuseable/plugins'

type Props = {
    entries: any,
    users: string,
    read: string,
    unread: string,
    all: string,
}

interface IErrors {
    subject?: any,
    message?: any,
    users?: any
}

function getUsers(users: any) {
    var resps: any = []
    users.map((item: any)=>{
        resps.push({value: item.id, label: item.first_name+" -- "+item.email+" - "+item.phone_number})
    })
    return resps
}

const NotificationIndex: FC<Props> = ({
    entries,
    users,
    read,
    unread,
    all
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    const [showModal, setShowModal] = useState(false)
    const {data, setData, post, processing} = useForm({
        message: '',
        subject: '',
        users: [],
        send_all: false,
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        subject: null,
        message: null,
        users: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key] && key != 'users' && key != 'send_all') {
                errorsInit[key] = "This field is required"
            }
            if(!fields.send_all && fields.users.length < 1){
                errorsInit.users = "Please select at least a user to notify"
            }
        }
        setFormErrors(errorsInit)
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value = target.type === 'checkbox'
        ? target.checked
        : target.value
        setData(target.name, value)
        let errorSign: any = {...formErrors}
        errorSign[target.name] = null
        setFormErrors(errorSign)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            post('/send_notifications', {
                preserveState: false
            })
        }
    }
    return (
        <AdminLayout title="Notifications" pages={['Dashboard', 'Notifications']}>
            <Row>
                <Col lg={12}>
                    <div className="card">
                        <div className="card-body">
                            <Row>
                                <Col lg={4}>
                                    <div className="media">
                                        <div className="me-3">
                                            <img src={sessionData.user.profile_photo_url} alt="" className="avatar-md rounded-circle img-thumbnail"/>
                                        </div>
                                        <div className="media-body align-self-center ml-2">
                                            <div className="text-muted">
                                                <p className="mb-2"></p>
                                                <h5 className="mb-1">{capitalizeFirstLetter(sessionData.user.first_name)} {capitalizeFirstLetter(sessionData.user.last_name)}</h5>
                                                <p className="mb-0">{sessionData.user.roles[0].name.toUpperCase()}</p>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                <Col lg={4} className="align-self-center">
                                    <div className="text-lg-center mt-4 mt-lg-0">
                                        <div className="row">
                                            <div className="col-4">
                                                <div>
                                                    <p className="text-muted text-truncate mb-2">All Notifications</p>
                                                    <h5 className="mb-0">{all}</h5>
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div>
                                                    <p className="text-muted text-truncate mb-2">Read</p>
                                                    <h5 className="mb-0">{read}</h5>
                                                </div>
                                            </div>
                                            <div className="col-4">
                                                <div>
                                                    <p className="text-muted text-truncate mb-2">Unread</p>
                                                    <h5 className="mb-0">{unread}</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                                {checkPermission(sessionData.user, "send notifications") && <Col lg={4} className="d-none d-lg-block">
                                    <div className="clearfix mt-4 mt-lg-0">
                                        <div className="dropdown float-end">
                                            <button type="button" className="btn btn-sm btn-danger waves-effect waves-light float-right" onClick={(e)=>{
                                                e.preventDefault()
                                                setShowModal(true)
                                            }}><i className="fas fa-envelope-open-text"></i> Send Notification</button>
                                        </div>
                                    </div>
                                </Col>}
                            </Row>
                        </div>
                    </div>
                </Col>
            </Row>
            <Row className="mt-2">
                <Col lg={12}>
                    <Filter />
                </Col>
                <Col lg={12}>
                    <div className="card">
                        <div className="card-body">
                            <DataTable columns={notificationColumns} data={notificationCollection(entries)} />
                        </div>
                    </div>
                </Col>
            </Row>
            <Modal show={showModal} centered onHide={()=>{ setShowModal(false) }}>
                <Form onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            Send Notifications
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3">
                            <div className="form-check form-check-custom form-check-solid">
                                <input className="form-check-input" type="checkbox" name="send_all" checked={data.send_all} onChange={handleChange} id="flexCheckDefault"/>
                                <label className="form-check-label" htmlFor="flexCheckDefault">
                                Send to all users
                                </label>
                            </div>
                        </Form.Group>
                        {!data.send_all && <Form.Group className="mb-3">
                            <Form.Label>Users</Form.Label>
                            <Select options={getUsers(users)} closeMenuOnSelect={false} isMulti name="users" onChange={(selected: any)=>{
                                // @ts-ignore
                                setData({...data, users: Array.isArray(selected) ? selected.map(x => x.value) : []})
                            }} />
                            {formErrors.users && <Form.Text className="text-danger">{formErrors.users}</Form.Text>}
                        </Form.Group>}
                        <Form.Group className="mb-3">
                            <Form.Label>Subject</Form.Label>
                            <Form.Control name="subject" type="text" value={data.subject} onChange={handleChange} disabled={processing} />
                            {formErrors.subject && <Form.Text className="text-danger">{formErrors.subject}</Form.Text>}
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Message</Form.Label>
                            <Form.Control name="message" as="textarea" cols={3} value={data.message} onChange={handleChange} disabled={processing} />
                            {formErrors.message && <Form.Text className="text-danger">{formErrors.message}</Form.Text>}
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="btn btn-primary btn-sm mt-2" type="submit" disabled={processing}>{processing ? ("sending notifications......") : ("Send Notification")}</button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </AdminLayout>
    )
}
export default NotificationIndex
