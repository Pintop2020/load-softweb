import React, { useEffect } from 'react'
import { usePage } from '@inertiajs/inertia-react';
import { logoFile,  mainSite } from '../Reuseable/app_config';
import {Helmet} from "react-helmet"
// @ts-ignore
import illustratedBg from '../../assets/media/illustrations/sketchy-1/14.png';

type Props = {
    title: string,
    children?: React.ReactNode;
}

const AuthLayout: React.FC<Props> = ({
    title,
    children,
}) => {
    const { appName } = usePage().props;
    useEffect(() => {
        document.body.classList.add('bg-white')
        return () => {
            document.body.classList.remove('bg-white')
        }
      }, []);
    return (
        <>
            <Helmet>
                <title>{[title, appName].join(" || ")}</title>
            </Helmet>
            <div className="d-flex flex-column flex-root">
                <div className="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style={{
                    backgroundImage: `url(${illustratedBg})`, position: 'absolute', bottom: 0, top: 0, right: 0, left: 0
                 }}>
                    <div className="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                        <a href={mainSite} className="mb-12">
                            <img alt="Logo" src={logoFile} className="h-60px" />
                        </a>
                        <div className="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                            {children}
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
};
export default AuthLayout;
