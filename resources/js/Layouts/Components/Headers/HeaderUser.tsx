import { Link } from '@inertiajs/inertia-react'
import React, { useEffect } from 'react'
import { capitalizeFirstLetter } from '../../../Reuseable/number_utils'
import route from 'ziggy-js'
import { Inertia } from '@inertiajs/inertia'

type Props = {
    user: any
}

const handleLogout = (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    Inertia.post(route('logout'));
};

const HeaderUser: React.FC<Props> = ({user}) => {
    useEffect(() => {
        let specifiedElement: any = document.getElementById('kt_pp_user')
        let dd:any = document.getElementById("kt_header_user_toggle")
        dd.addEventListener('click', function() {
            specifiedElement.classList.toggle('show')
        });
        document.addEventListener('click', function(event) {
            let isClickInside:any = specifiedElement.contains(event.target)
            let isClickOn: any = dd.contains(event.target)
            if (!isClickInside && !isClickOn) {
                if(specifiedElement.classList.contains('show')){
                    specifiedElement.classList.remove('show')
                }
            }
        });
    }, []);
  return (
    <div className="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
        {/* begin::Menu wrapper */}
        <div className="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" id="kt_header_user_toggle">
            <img src={user.profile_photo_url} alt="user" />
        </div>
        {/* begin::Menu */}
        <div className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true" style={{
            zIndex: 105, position: 'fixed', inset: '0px 0px auto auto', margin: '0px', transform: 'translate3d(-30px, 65px, 0px)'
         }} data-popper-placement="bottom-end" id="kt_pp_user">
            {/* begin::Menu item */}
            <div className="menu-item px-3">
                <div className="menu-content d-flex align-items-center px-3">
                    <div className="symbol symbol-50px me-5">
                        <img alt="Logo" src={user.profile_photo_url} />
                    </div>
                    <div className="d-flex flex-column">
                        <div className="fw-bolder d-flex align-items-center fs-5">{capitalizeFirstLetter(user.first_name)} {capitalizeFirstLetter(user.last_name)}
                        <span className="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">{capitalizeFirstLetter(user.roles[0].name)}</span></div>
                        <a href="#" className="fw-bold text-muted text-hover-primary fs-7">{user.email.toLowerCase()}</a>
                    </div>
                </div>
            </div>
            {/* end::Menu item */}
            <div className="separator my-2"></div>
            <div className="menu-item px-5">
                <Link href="/profile" className="menu-link px-5">My Profile</Link>
            </div>
            <div className="separator my-2"></div>
            <div className="menu-item px-5">
                <a href="/logout" onClick={handleLogout} className="menu-link px-5">Sign Out</a>
            </div>
            {/* end::Menu item */}
        </div>
        {/* end::Menu */}
        {/* end::Menu wrapper */}
    </div>
  )
}

export {HeaderUser}
