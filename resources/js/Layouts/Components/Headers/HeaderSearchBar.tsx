import React, { useEffect } from 'react'
// import { AdvancedSearch } from './SearchBar/AdvancedSearch';
import { SimpleSearch } from './SearchBar/SimpleSearch';

const HeaderSearchBar = () => {
    useEffect(() => {
        let specifiedElement: any = document.getElementById('kt_pp_search')
        let dd:any = document.getElementById("kt_header_search_toggle")
        dd.addEventListener('click', function() {
            specifiedElement.classList.toggle('show')
        });
        document.addEventListener('click', function(event) {
            let isClickInside:any = specifiedElement.contains(event.target)
            let isClickOn: any = dd.contains(event.target)
            if (!isClickInside && !isClickOn) {
                if(specifiedElement.classList.contains('show')){
                    specifiedElement.classList.remove('show')
                }
            }
        });
    }, []);
  return (
    <div className="d-flex align-items-stretch ms-1 ms-lg-3">
        {/* begin::Search */}
        <div id="kt_header_search" className="d-flex align-items-stretch" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="menu" data-kt-menu-trigger="auto" data-kt-menu-overflow="false" data-kt-menu-permanent="true" data-kt-menu-placement="bottom-end">
            {/* begin::Search toggle */}
            <div className="d-flex align-items-center" data-kt-search-element="toggle" id="kt_header_search_toggle">
                <div className="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px">
                    <span className="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            {/* end::Search toggle */}
            {/* begin::Menu */}
            <div data-kt-search-element="content" className="menu menu-sub menu-sub-dropdown p-7 w-325px w-md-375px" id="kt_pp_search" style={{
                zIndex: 105, position: 'fixed', inset: '0px 0px auto auto', margin: '0px', transform: 'translate3d(-179px, 65px, 0px)'
             }} data-popper-placement="bottom-end">
                {/* begin::Simple Search */}
                    <SimpleSearch />
                {/* end::Wrapper */}
                {/* begin::Simple Search */}
                    {/* <AdvancedSearch /> */}
                {/* end::Preferences */}
            </div>
            {/* end::Menu */}
        </div>
        {/* end::Search */}
    </div>
  )
}

export {HeaderSearchBar}
