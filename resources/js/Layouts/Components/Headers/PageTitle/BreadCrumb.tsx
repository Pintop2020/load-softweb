import React from 'react'
import { mainSite } from '../../../../Reuseable/app_config'

type Props = {
    pages: any,
}

function getPages(pages: any){
    var resps: any = []
    pages.forEach((page: any, i: number)=>{
        if(i == 0){
            resps.push(page)
            resps.push('bullet')
        }
    })
    return resps
}

const BreadCrumb: React.FC<Props> = ({pages}) => {
  return (
    <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
        <h1 className="d-flex align-items-center text-dark fw-bolder fs-3 my-1">{pages[pages.length-1]}</h1>
        <span className="h-20px border-gray-200 border-start mx-4"></span>
        <ul className="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
            <li className="breadcrumb-item text-muted">
                <a href={mainSite} className="text-muted text-hover-primary">Home</a>
            </li>
            <li className="breadcrumb-item"><span className="bullet bg-gray-200 w-5px h-2px"></span></li>
            {pages.map((item: string, i:number)=>{
                return i === pages.length -1 ? <li key={i} className="breadcrumb-item text-dark">{item}</li> : <React.Fragment key={i}><li className="breadcrumb-item text-muted">{item}</li><li className="breadcrumb-item"><span className="bullet bg-gray-200 w-5px h-2px"></span></li></React.Fragment>
            })}
        </ul>
    </div>
  )
}

export {BreadCrumb}
