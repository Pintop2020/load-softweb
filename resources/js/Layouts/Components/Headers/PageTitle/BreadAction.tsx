import React from 'react'

type Props = {
    children: any,
}

const BreadAction: React.FC<Props> = ({children}) => {
  return (
    <div className="d-flex align-items-right py-1">
        {/* begin::Wrapper */}
            {children}
        {/* end::Wrapper */}
    </div>
  )
}

export {BreadAction}
