import React from 'react'
import { BreadAction } from './BreadAction'
import { BreadCrumb } from './BreadCrumb'

type Props = {
    pages: any,
    children?: any
}

const BreadDefault: React.FC<Props> = ({pages, children}) => {
  return (
    <div id="kt_toolbar_container" className="container-fluid d-flex flex-stack">
        {/* begin::Page title */}
        <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" className="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
            {/* begin::title */}
            <BreadCrumb pages={pages} />
            {/* end::title */}
        </div>
        {/* end::Page title */}
        {/* begin::action */}
            {children && <BreadAction children={children} />}
        {/* end::action */}
    </div>
  )
}

export default BreadDefault
