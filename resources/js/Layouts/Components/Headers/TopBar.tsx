import React from 'react'
// import { HeaderNotifications } from './HeaderNotifications'
import { HeaderSearchBar } from './HeaderSearchBar'
import { HeaderUser } from './HeaderUser'

type Props = {
    user: any,
    notifs_count: number,
    notifs: any
}

const TopBar: React.FC<Props> = ({user, notifs_count, notifs}) => {
  return (
    <div className="d-flex align-items-stretch flex-shrink-0">
        {/* begin::Toolbar wrapper */}
        <div className="d-flex align-items-stretch flex-shrink-0">
            {/* begin::Search */}
            <HeaderSearchBar />
            {/* end::Search */}
            {/* begin::Notifications */}
            {/* <HeaderNotifications notifs_count={notifs_count} notifs={notifs}/> */}
            {/* end::Notifications */}
            {/* begin::User */}
            <HeaderUser user={user}/>
            {/* end::User */}
        </div>
        {/* end::Toolbar wrapper */}
    </div>
  )
}

export {TopBar}
