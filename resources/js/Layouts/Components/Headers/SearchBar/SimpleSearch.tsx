import { useForm } from '@inertiajs/inertia-react';
import { CircularProgress } from '@mui/material';
import React, { useState } from 'react'

interface IErrors {
    q: any,
}

const SimpleSearch = () => {
    const {data, setData, get, processing} = useForm({
        q: '',
    });
    const [formErrors, setFormErrors] = useState<IErrors>({
        q: null,
    });
    const validateInput = () => {

        const errorsInit: any = {};
        let fields: any = { ...data };

        for (const key in fields) {
            if (!fields.q) {
                errorsInit[key] = "This field is required";
            }
        }
        setFormErrors(errorsInit);
        if (Object.entries(errorsInit).length === 0) {
            return true;
        } else {
            return false;
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget;
        const value = target.value;
        setData(target.name, value);
        let erroSign: any = {...formErrors};
        erroSign[target.name] = null;
        setFormErrors(erroSign);
    };
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault();
        const validated = validateInput();
        if (validated) {
            get('search');
        }
    };
  return (
    <div data-kt-search-element="wrapper" id="d-kt-d">
        {/* begin::Form */}
        <form data-kt-search-element="form" onSubmit={handleSubmit} className="w-100 position-relative mb-3" autoComplete="off">
            <span className="svg-icon svg-icon-2 svg-icon-lg-1 svg-icon-gray-500 position-absolute top-50 translate-middle-y ms-0">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                </svg>
            </span>
            <input type="text" className="form-control form-control-flush ps-10" name="q" placeholder="Search..." data-kt-search-element="input" value={data.q} onChange={handleChange} disabled={processing} />
            {formErrors.q && <div className="text-danger">{formErrors.q}</div>}
            {processing && <CircularProgress color="inherit" />}
            <span className="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 d-none" data-kt-search-element="clear">
                <span className="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                    </svg>
                </span>
            </span>
            {/* begin::Toolbar */}
            <div className="position-absolute top-50 end-0 translate-middle-y" data-kt-search-element="toolbar">
                {/* begin::Advanced search toggle */}
                {/* <div data-kt-search-element="advanced-options-form-show" className="btn btn-icon w-20px btn-sm btn-active-color-primary" data-bs-toggle="tooltip" title="Show more search options">
                    <span className="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                        </svg>
                    </span>
                </div> */}
                {/* end::Advanced search toggle */}
            </div>
            {/* end::Toolbar */}
        </form>
        {/* end::Form */}
    </div>
  )
}

export {SimpleSearch}
