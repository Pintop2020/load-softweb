import { useForm } from '@inertiajs/inertia-react';
import { CircularProgress } from '@mui/material';
import React, { useState } from 'react'

interface IErrors {
    q: any,
    type: any
}

const AdvancedSearch = () => {
    const {data, setData, get, processing} = useForm({
        q: '',
        type: ''
    });
    const [formErrors, setFormErrors] = useState<IErrors>({
        q: null,
        type: null
    });
    const validateInput = () => {

        const errorsInit: any = {};
        let fields: any = { ...data };

        for (const key in fields) {
            if (!fields.q) {
                errorsInit[key] = "This field is required";
            }
        }
        setFormErrors(errorsInit);
        if (Object.entries(errorsInit).length === 0) {
            return true;
        } else {
            return false;
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget;
        const value = target.type === 'radio'
        ? target.checked
        : target.value;
        setData(target.name, value);
        let erroSign: any = {...formErrors};
        erroSign[target.name] = null;
        setFormErrors(erroSign);
    };
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault();
        const validated = validateInput();
        if (validated) {
            get('login');
        }
    };
  return (
    <form data-kt-search-element="advanced-options-form" onSubmit={handleSubmit} className="pt-1 d-none">
        <h3 className="fw-bold text-dark mb-7">Advanced Search</h3>
        <div className="mb-5">
            <input type="text" className="form-control form-control-sm form-control-solid" placeholder="Contains the word" name="q" onChange={handleChange} disabled={processing} />
        </div>
        <div className="mb-5">
            <div className="nav-group nav-group-fluid">
                <label>
                    <input type="radio" className="btn-check" name="type" value="all" checked={data.type === 'all'} disabled={processing} onChange={handleChange} />
                    <span className="btn btn-sm btn-color-muted btn-active btn-active-primary">All</span>
                </label>
                <label>
                    <input type="radio" className="btn-check" name="type" value="users" checked={data.type === 'users'} disabled={processing} onChange={handleChange} />
                    <span className="btn btn-sm btn-color-muted btn-active btn-active-primary px-4">Users</span>
                </label>
                <label>
                    <input type="radio" className="btn-check" name="type" value="orders" checked={data.type === 'orders'} disabled={processing} onChange={handleChange} />
                    <span className="btn btn-sm btn-color-muted btn-active btn-active-primary px-4">Orders</span>
                </label>
                <label>
                    <input type="radio" className="btn-check" name="type" value="riders" checked={data.type === 'riders'} disabled={processing} onChange={handleChange} />
                    <span className="btn btn-sm btn-color-muted btn-active btn-active-primary px-4">Riders</span>
                </label>
            </div>
        </div>
        <div className="d-flex justify-content-end">
            <button type="reset" className="btn btn-sm btn-light fw-bolder btn-active-light-primary me-2" data-kt-search-element="advanced-options-form-cancel">Cancel</button>
            <button disabled={processing} type="submit" className={["btn btn-sm fw-bolder btn-primary", processing ? "":" w-100"].join("")} data-kt-search-element="advanced-options-form-search"> {processing ? <CircularProgress color="inherit" /> : <span>Search</span>}</button>
        </div>
    </form>
  )
}

export {AdvancedSearch}
