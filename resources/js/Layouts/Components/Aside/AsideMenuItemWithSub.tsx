import React, { useState } from 'react'
import clsx from 'clsx'
import {checkIsActive} from '../../../helpers/helpers'

type Props = {
    item: any,
    hasBullet?: boolean
    children?: any
}

const AsideMenuItemWithSub: React.FC<Props> = ({
  item,
  hasBullet,
  children
}) => {
  const isActive = checkIsActive(item)
  const [show, setShow] = useState(isActive)
  const handleClick = () => {
    setShow(!show)
}

  return (
    <div
      className={clsx('menu-item', {'here show': show}, 'menu-accordion')}
      data-kt-menu-trigger='click' onClick={handleClick}
    >
      <span className='menu-link'>
        {hasBullet && (
          <span className='menu-bullet'>
            <span className='bullet bullet-dot'></span>
          </span>
        )}
        <span className='menu-icon'>
            <i className={clsx('bi fs-3', item.icon)}></i>
        </span>
        <span className='menu-title'>{item.name}</span>
        <span className='menu-arrow'></span>
      </span>
      <div className={clsx('menu-sub menu-sub-accordion', {'menu-active-bg': isActive})}>
        {children}
      </div>
    </div>
  )
}

export {AsideMenuItemWithSub}
