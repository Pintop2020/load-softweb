import React from 'react'
import clsx from 'clsx'
import { Link } from '@inertiajs/inertia-react'
import { checkIsActive } from '../../../helpers/helpers'

type Props = {
    item: any,
    hasBullet?: boolean
    children?: any
}

const AsideMenuItem: React.FC<Props> = ({
  item,
  hasBullet = false,
  children
}) => {
  const isActive = checkIsActive(item)

  return (
    <div className='menu-item'>
      <Link className={clsx('menu-link without-sub', {active: isActive})} href={item.link}>
        {hasBullet && (
          <span className='menu-bullet'>
            <span className='bullet bullet-dot'></span>
          </span>
        )}
        <span className='menu-icon'>
            <i className={clsx('bi fs-3', item.icon)}></i>
        </span>
        <span className='menu-title'>{item.name}</span>
      </Link>
      {children}
    </div>
  )
}

export {AsideMenuItem}
