/* eslint-disable react/jsx-no-target-blank */
import React from 'react'
import {AsideMenuItemWithSub} from './AsideMenuItemWithSub'
import {AsideMenuItem} from './AsideMenuItem'
import { dashboardLinks } from '../../../Reuseable/links'
import { checkPermission } from '../../../Reuseable/app_config'

type Props = {
    user: any
}

const AsideMenuMain: React.FC<Props> = ({user}) => {
    return (
    <>
        {dashboardLinks.map((item, i)=>{
            if(checkPermission(user, item.permission)) return <span key={i}>
                <div className='menu-item'>
                    <div className='menu-content pt-8 pb-2'>
                    <span className='menu-section text-muted text-uppercase fs-8 ls-1'>{item.name}</span>
                    </div>
                </div>
                {item.children.map((child, d)=>{
                    if(checkPermission(user, child.permission)) return child.subs.length > 0 ?
                    <AsideMenuItemWithSub
                        key={d}
                        item={child}>
                        {child.subs.map((sub, m)=>{
                            if(checkPermission(user, sub.permission)) return <AsideMenuItem key={m} item={sub} hasBullet={true} />
                        })}
                    </AsideMenuItemWithSub> : <AsideMenuItem
                        key={d}
                        item={child}
                    />
                })}
            </span>
        })}
    </>
  )
}
export default AsideMenuMain
