import React, { useEffect } from 'react'
// @ts-ignore
import { Head } from '@inertiajs/inertia-react'
import 'react-toastify/dist/ReactToastify.css'
import { usePage } from '@inertiajs/inertia-react'
import {  toast, ToastContainer } from '../Reuseable/plugins'
import { AsideDefault } from './Components/Aside/AsideDefault'
import BreadDefault from './Components/Headers/PageTitle/BreadDefault'
import { Header } from './Components/Headers/Header'


type Props = {
    title?: string,
    children?: any,
    pages?: any,
    breadAction?: any
}

const AdminLayout: React.FC<Props> = ({
    title,
    children,
    pages,
    breadAction
}) => {
    const { flash, appName, auth } = usePage().props
    // @ts-ignore
    const notifySuccess = () =>  toast.success(flash.success);
    // @ts-ignore
    const notifyError = () =>  toast.error(flash.error);
    // @ts-ignore
    let uauth: any = auth;
    useEffect(() => {
        // @ts-ignore
        if(flash.error) notifyError();
        // @ts-ignore
        if(flash.success) notifySuccess();
    }, []);
    return (
        <>
            <Head title={[title, appName].join(" || ")}/>
            <ToastContainer position="top-right" autoClose={5000} hideProgressBar={true} newestOnTop={false} theme="colored" closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover limit={1} />
            {/* begin::Main */}
            {/* begin::Root */}
            <div className="d-flex flex-column flex-root">
                {/* begin::Page */}
			    <div className="page d-flex flex-row flex-column-fluid">
                    {/* begin::Aside */}
                    <AsideDefault user={uauth.user}/>
                    {/* end::Aside */}
                    {/* begin::wrapper */}
                    <div className='wrapper d-flex flex-column flex-row-fluid' id='kt_wrapper'>
                        {/* begin::Header */}
                        <Header user={uauth.user} notifs={uauth.notifs} notifs_count={uauth.notifs_count}/>
                        {/* end::Header */}
                        {/* begin::Content */}
                        <div className="content d-flex flex-column flex-column-fluid" id="kt_content">
                            {/* begin::Toolbar */}
                            {pages && <div className="toolbar" id="kt_toolbar">
                                {/* begin::Container */}
                                <BreadDefault pages={pages} children={breadAction} />
                                {/* end::Container */}
                            </div>}
                            {/* end::Toolbar */}
                            {/* begin::Post */}
                            <div className="post d-flex flex-column-fluid" id="kt_post">
                                {/* begin::Container */}
                                <div id="kt_content_container" className="container-xxl">
                                    {children}
                                </div>
                                {/* end::Container */}
                            </div>
                            {/* end::Post */}
                        </div>
                        {/* end::Content */}
                    </div>
                    {/* end::wrapper */}
                </div>
                {/* end::Page */}
            </div>
            {/* end::Root */}
            {/* end::Main */}
        </>
    );
};
export default AdminLayout;
