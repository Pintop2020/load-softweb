import React, { useEffect } from 'react'
import { Link, usePage } from '@inertiajs/inertia-react'
import { checkPermission } from '../Reuseable/app_config';

type Props = {
    lists: any,
    myKey: number,
    myId: string,
}

function toggleStyle(el: any, styleName: string, value: any) {
    if (el.style[styleName] === '') {
        el.style[styleName] = value;
    } else {
        el.style[styleName] = '';
    }
}

const DropDown: React.FC<Props> = ({
    lists,
    myKey,
    myId
}) => {
    const { auth } = usePage().props
    let sessionData: any = auth
    useEffect(() => {
        let main: any = document.getElementById('dropdowns'+myId+myKey)
        let dd:any = main.querySelector(".btn-dropnow")
        let specifiedElement: any = main.querySelector('.my-ddop')
        dd.addEventListener('click', function(e: any) {
            e.preventDefault()
            dd.classList.toggle('show')
            dd.classList.toggle('menu-dopdown')
            specifiedElement.classList.toggle('show')
            toggleStyle(specifiedElement, 'z-index', 105)
            // toggleStyle(specifiedElement, 'position', 'fixed')
            // toggleStyle(specifiedElement, 'inset', '0px 0px auto auto')
            // toggleStyle(specifiedElement, 'margin', '0px')
            // toggleStyle(specifiedElement, 'transform', 'translate3d(-59px, 338px, 0px)')
            specifiedElement.toggleAttribute('data-popper-placement', 'bottom-end')
        });
        document.addEventListener('click', function(event) {
            let isClickInside:any = specifiedElement.contains(event.target)
            let isClickOn: any = dd.contains(event.target)
            if (!isClickInside && !isClickOn) {
                if(specifiedElement.classList.contains('show')){
                    specifiedElement.classList.remove('show')
                }
            }
        });
    }, []);
    return (
        <div id={'dropdowns'+myId+myKey}>
            <a href="#" className="btn btn-sm btn-light btn-active-light-primary btn-dropnow" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                <span className="svg-icon svg-icon-5 m-0">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                    </svg>
                </span>
            </a>
            {/* begin::Menu */}
            <div className="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4 my-ddop" data-kt-menu="true">
                {/* begin::Menu item */}
                {lists.map((item: any, i: number)=>{
                    return checkPermission(sessionData.user, item.permission) && <div className="menu-item px-3" key={i}>
                        {item.click ? <Link href={item.href} onClick={item.click} className="menu-link px-3">{item.name}</Link> : <Link href={item.href} className="menu-link px-3">{item.name}</Link>}
                    </div>
                })}
                {/* end::Menu item */}
            </div>
            {/* end::Menu */}
        </div>
    )
}
export {DropDown}
