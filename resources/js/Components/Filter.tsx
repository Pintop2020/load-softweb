import React, { useState } from 'react'
import { useForm } from '@inertiajs/inertia-react'
import { Form, Card, Col, Row } from 'react-bootstrap';
import { Select } from '../Reuseable/plugins'

var sortValues = [
    { value: "asc", label: "Ascending Order" },
    { value: "desc", label: "Descending Order" },
]

interface IErrors {
    sortBy: any,
    show: any,
}

const Filter = () => {
    const {data, setData, get, processing} = useForm({
        sortBy: 'desc',
        show: '50',
    })
    const [formErrors, setFormErrors] = useState<IErrors>({
        sortBy: null,
        show: null,
    })
    const validateInput = () => {
        const errorsInit: any = {}
        let fields: any = { ...data }

        for (const key in fields) {
            if (!fields[key]) {
                errorsInit[key] = "This field is required"
            }
            if(fields.show < 50) {
                errorsInit.show = "Minimum filter data is 50 items"
            }
        }
        setFormErrors(errorsInit);
        if (Object.entries(errorsInit).length === 0) {
            return true
        } else {
            return false
        }
    }
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const target: any = e.currentTarget
        const value: string = target.value
        setData(target.name, value)
        let errors: any = {...formErrors}
        errors[target.name] = null
        setFormErrors(errors)
    }
    const handleSubmit = (e: React.SyntheticEvent<HTMLFormElement>) => {
        e.preventDefault()
        const validated = validateInput()
        if (validated) {
            get(window.location.pathname)
        }
    };
    return (
        <Row>
            <Col className="col-12 mb-2">
                <Card className="border-0">
                    <Card.Body>
                        <Form onSubmit={handleSubmit} className="my-2">
                            <Row>
                                <Col lg={4} md={4} sm={12}>
                                    <Select options={sortValues} defaultValue={sortValues[1]} isDisabled={processing} name="sortBy" onChange={(selected: any)=>{
                                        setData({...data, sortBy: selected.value})
                                    }} />
                                    {formErrors.sortBy && <Form.Text className="text-danger">{formErrors.sortBy}</Form.Text>}
                                </Col>
                                <Col lg={6} md={6} sm={12}>
                                    <Form.Control type="number" name="show" value={data.show} onChange={handleChange} disabled={processing}/>
                                    {formErrors.show && <Form.Text className="text-danger">{formErrors.show}</Form.Text>}
                                </Col>
                                <Col lg={2} md={2} sm={12}>
                                    <button type="submit" className="btn btn-sm btn-primary" disabled={processing}>{processing ? ("filtering......") : ("Filter")}</button>
                                </Col>
                            </Row>
                        </Form>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}
export {Filter}
