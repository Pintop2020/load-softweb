import {useTable, Row, useFilters, useGlobalFilter, usePagination, useAsyncDebounce} from 'react-table'
import React, { FC, useMemo, useState } from 'react'
import clsx from 'clsx'
import { Col } from 'react-bootstrap'
import matchSorter from 'match-sorter'
import 'regenerator-runtime/runtime'

type Props = {
    columns?: any,
    data?: any,
}

type FilterProps = {
    preGlobalFilteredRows: any,
    globalFilter: any,
    setGlobalFilter: any,
}

type DefaultFilterProps = {
    column: any,
}

function getPageRows(pageIndex: any, myRows: any){
    let val: any = myRows.slice(pageIndex > 0 ? pageIndex-1 : pageIndex , pageIndex+5 <= myRows.length ? pageIndex+5 : myRows.length)
    return val
}
// @ts-ignore
function fuzzyTextFilterFn(rows, id, filterValue) {
    // @ts-ignore
    return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
}

// Let the table remove the filter if the string is empty
// @ts-ignore
fuzzyTextFilterFn.autoRemove = val => !val

const GlobalFilter: React.FC<FilterProps> = ({preGlobalFilteredRows ,globalFilter, setGlobalFilter}) => {
    const count = preGlobalFilteredRows.length
    const [value, setValue] = useState(globalFilter)
    const onChange = useAsyncDebounce(value => {
      setGlobalFilter(value || undefined)
    }, 200)

    return (
        <input
            value={value || ""}
            onChange={e => {
            setValue(e.target.value);
            onChange(e.target.value);
            }}
            placeholder={`search ${count} records...`}
            className='form-control form-control-sm'
        />
    )
}

const DefaultColumnFilter: FC<DefaultFilterProps> = ({
    column: { filterValue, preFilteredRows, setFilter },
  }) => {
    const count = preFilteredRows.length

    return (
      <input
        value={filterValue || ''}
        className='form-control form-control-sm'
        onChange={e => {
          setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
        }}
        placeholder={`Search ${count} records...`}
      />
    )
}

const DataTable: FC<Props> = ({columns, data}) => {
    const filterTypes = useMemo(
        () => ({
          // Add a new fuzzyTextFilterFn filter type.
          fuzzyText: fuzzyTextFilterFn,
          // Or, override the default text filter to use
          // "startWith"
          text: (rows: any, id: any, filterValue: any) => {
              // @ts-ignore
            return rows.filter(row => {
              const rowValue = row.values[id]
              return rowValue !== undefined
                ? String(rowValue)
                    .toLowerCase()
                    .startsWith(String(filterValue).toLowerCase())
                : true
            })
          },
        }),
        []
    )
    const defaultColumn = useMemo(
        () => ({
          // Let's set up our default Filter UI
          Filter: DefaultColumnFilter,
        }),
        []
    )
    // @ts-ignore
  const { getTableProps, getTableBodyProps, headerGroups, prepareRow, page, canPreviousPage, canNextPage, pageOptions, pageCount,gotoPage, nextPage, previousPage, setPageSize, state: { pageIndex, pageSize, globalFilter }, preGlobalFilteredRows, setGlobalFilter } = useTable({ columns, data, initialState: { pageIndex: 0 }, defaultColumn, filterTypes}, useFilters, useGlobalFilter, usePagination)

  var myRows = [], i = 0, len = pageCount
  while (++i <= len) myRows.push(i)

    // Data Table UI
    return (<>
        <div className="row my-5">
            <Col lg={3} md={6}>
                <select value={pageSize} onChange={e => {
                    setPageSize(Number(e.target.value))
                }} className='form-control form-control-sm'>
                    {[10, 20, 30, 40, 50].map(pageSize => (
                        <option key={pageSize} value={pageSize}>
                        Show {pageSize} entries
                        </option>
                    ))}
                </select>
            </Col>
            <Col lg={6} md={12} className="d-md-none d-lg-block"></Col>
            <Col lg={3} md={6}>
                <GlobalFilter
                    preGlobalFilteredRows={preGlobalFilteredRows}
                    globalFilter={globalFilter}
                    setGlobalFilter={setGlobalFilter}
                />
            </Col>
        </div>
        <div className="table-responsive">
            <table {...getTableProps()} className='table table-striped table-hover gy-7 gs-7'>
                <thead>
                {headerGroups.map(headerGroup => (
                    <tr {...headerGroup.getHeaderGroupProps()} className='fw-bold fs-6 text-gray-800 border-bottom-2 border-gray-200'>
                    {headerGroup.headers.map(column => (
                        <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                    ))}
                    </tr>
                ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                {page.map((row: Row<object>, i: any) => {
                    prepareRow(row)
                    return (
                    <tr {...row.getRowProps()}>
                        {row.cells.map((cell: { getCellProps: () => JSX.IntrinsicAttributes & React.ClassAttributes<HTMLTableDataCellElement> & React.TdHTMLAttributes<HTMLTableDataCellElement>; render: (arg0: string) => boolean | React.ReactChild | React.ReactFragment | React.ReactPortal | null | undefined }) => {
                        return <td {...cell.getCellProps()}>{cell.render('Cell')}</td>
                        })}
                    </tr>
                    )
                })}
                </tbody>
            </table>
        </div>
        <div className="row my-2">
            <Col sm={12} md={3}>
                <div className="text-start text-muted">
                    Showing {(pageIndex*pageSize)+1} to {((pageIndex+1)*(pageSize)) > data.length ? data.length : ((pageIndex+1)*(pageSize))} of {data.length}
                </div>
            </Col>
            <Col sm={12} md={6}>
                <ul className="pagination pagination-circle pagination-outline">
                    <li className={clsx("page-item previous m-1", !canPreviousPage && "disabled")}>
                        <a href="#" onClick={() => gotoPage(0)} className="page-link"><i className="previous"></i><i className="previous"></i></a>
                    </li>
                    <li className={clsx("page-item previous m-1", !canPreviousPage && "disabled")}>
                        <a href="#" onClick={() => previousPage()} className="page-link"><i className="previous"></i></a>
                    </li>
                    {myRows.length > 5 && getPageRows(pageIndex, myRows).map((paging: number, i: number)=>{
                        return <li key={i} className={clsx("page-item m-1", pageIndex+1==paging && "active")}><a href="#" onClick={() => gotoPage(paging - 1)} className="page-link">{paging}</a></li>
                    })}
                    {myRows.length <= 5 && myRows.map((paging: number, i: number)=>{
                        return <li key={i} className={clsx("page-item m-1", pageIndex+1==paging && "active")}><a href="#" onClick={() => gotoPage(paging - 1)} className="page-link">{paging}</a></li>
                    })}
                    <li className={clsx("page-item next m-1", !canNextPage && 'disabled')}>
                        <a href="#" onClick={() => nextPage()} className="page-link"><i className="next"></i></a>
                    </li>
                    <li className={clsx("page-item next m-1", !canNextPage && 'disabled')}>
                        <a href="#" onClick={() => gotoPage(pageCount - 1)} className="page-link"><i className="next"></i><i className="next"></i></a>
                    </li>
                </ul>
            </Col>
            <Col sm={12} md={3}>
                <div className="text-muted text-end">
                    Page <strong>{pageIndex + 1} of {pageOptions.length}</strong> {'  |  '}
                    <span>Jump to</span>
                    <input type="number" defaultValue={pageIndex + 1} onChange={e => {
                        const page = e.target.value ? Number(e.target.value) - 1 : 0
                        gotoPage(page)
                    }} />
                </div>
            </Col>
        </div>
    </>
    )
  }
export {DataTable}
