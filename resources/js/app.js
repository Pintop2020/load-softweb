import React from 'react'
import { render } from 'react-dom'
import { InertiaApp } from '@inertiajs/inertia-react'
import { InertiaProgress } from '@inertiajs/progress'

InertiaProgress.init({
    delay: 250,
    color: '#222222',
    includeCSS: true,
    showSpinner: true,
})

const el = document.getElementById('app');
const App = () => {

    return (
        <InertiaApp
            id='sofweb-powered'
            initialPage={JSON.parse(el.dataset.page)}
            resolveComponent={name => import(`./Pages/${name}`).then(module => module.default)}
        />
    )
}
export default App
if (el) {
    render(<App />, el);
}
