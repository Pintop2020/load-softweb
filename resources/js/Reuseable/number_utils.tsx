import moment from "moment";
import { mainSite } from "./app_config";

export const convertInput = (val: any) => {
  let convertedToNumber = val.split(",");
  convertedToNumber = Number(convertedToNumber.join(""));
  const includesAlphabet = /[^0-9.]/g.test(convertedToNumber);
  return { includesAlphabet, convertedToNumber };
};

export const stripFromNumber = (numberString: string, strip: string) => {
  let convertedToNumber = numberString.split(strip);
  return Number(convertedToNumber.join(""));
};

export const convertUnixDatetoReadable = (unixTime: any) => {
  if(unixTime) {
    return moment.unix(Number(unixTime) / 1000).format("llll")
  }
  return "______"
}

export const ccFormat = (value: any) => {
    return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
}


export const datFormat = (date: any) => {
    return moment(date).fromNow();
}

export const datFormat2 = (date: any) => {
    return moment(date).format('Do MMMM, YYYY');
}

export const datFormat3 = (date: any) => {
    return moment(date).format('Do MMMM, YYYY hh:mma');
}

export const datFormat4 = (date: any) => {
    return moment(date).format('hh:mm A');
}

export const datFormat5 = (date: any) => {
    return moment(date).format('YYYY-MM-DD');
}

export function strPaddingStart(number: number, digits: number = 2, emptyDigit?: string) {
	let length = 0;
	let n = Math.abs(number);
	let absoluteNumber = n;
	do {
		n /= 10;
		length++;
	} while (n >= 1);
	const prefix = Array(Math.max((digits - length) + 1, 0)).join(emptyDigit);
	return number < 0 ? `-${prefix}${absoluteNumber}` : prefix + number;
}

export function capitalizeFirstLetter(string: any) {
    return string.charAt(0).toUpperCase() + string.slice(1)
}

export const toAbsoluteUrl = (pathname: string) => mainSite + pathname
