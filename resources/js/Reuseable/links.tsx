export const dashboardLinks = [
    {
        name: "Dashboard",
        permission: "view dashboard",
        children: [
            {
                name: "Home",
                link: '/',
                icon: 'fas fa-home',
                permission: "view dashboard",
                subs: [],
            },
            {
                name: "Transactions",
                link: "/transactions",
                icon: 'fas fa-chart-line',
                permission: "view transactions",
                subs: [
                    {
                        name: "Successful",
                        link: "/transactions/type/success",
                        icon: "",
                        permission: "view successful transactions",
                        subs: []
                    },
                    {
                        name: "Pending",
                        link: "/transactions/type/pending",
                        icon: "",
                        permission: "view pending transactions",
                        subs: []
                    },
                    {
                        name: "Failed",
                        link: "/transactions/type/failed",
                        icon: "",
                        permission: "view failed transactions",
                        subs: []
                    }
                ],
            },
            {
                name: "Notifications",
                link: "/notifications",
                permission: "view notifications",
                icon: 'far fa-bell',
                subs: []
            },
            {
                name: "SMS",
                link: "/sms",
                permission: "send sms",
                icon: 'fas fa-sms',
                subs: []
            },
            {
                name: "Coupons",
                link: "/coupons",
                permission: "view coupons",
                icon: 'fas fa-tags',
                subs: []
            }
        ],
    },
    {
        name: "ACCOUNTS",
        permission: "view account",
        children: [
            {
                name: "Users",
                link: "/users",
                icon: 'fas fa-users',
                permission: "view users",
                subs: [
                    {
                        name: "Dispatch",
                        link: "/users/type/dispatch",
                        icon: "",
                        permission: "view dispatch",
                        subs: []
                    },
                    {
                        name: "Customers",
                        link: "/users/type/user",
                        icon: "",
                        permission: "view customers",
                        subs: []
                    }
                ],
            },
            {
                name: "Truck Types",
                link: "/vehicle_types",
                icon: 'fas fa-layer-group',
                permission: "view vehicle",
                subs: []
            },
            {
                name: "Trucks",
                link: "/vehicles",
                icon: 'fas fa-truck-moving',
                permission: "view vehicles",
                subs: [
                    {
                        name: "Available",
                        link: "/vehicles/type/available",
                        icon: "",
                        permission: "view available vehicles",
                        subs: []
                    },
                    {
                        name: "Unavailable",
                        link: "/vehicles/type/unavailable",
                        icon: "",
                        permission: "view unavailable vehicles",
                        subs: []
                    },
                    {
                        name: "Blacklisted",
                        link: "/vehicles/type/blacklisted",
                        icon: "",
                        permission: "view blacklisted vehicles",
                        subs: []
                    },
                ],
            },
            {
                name: "Orders",
                link: "/orders",
                icon: 'fas fa-cart-arrow-down',
                permission: "view orders",
                subs: [
                    {
                        name: "Accepted",
                        link: "/orders/type/accepted",
                        icon: "",
                        permission: "view accepted orders",
                        subs: []
                    },
                    {
                        name: "Pending",
                        link: "/orders/type/pending",
                        icon: "",
                        permission: "view pending orders",
                        subs: []
                    },
                    {
                        name: "Cancelled",
                        link: "/orders/type/cancelled",
                        icon: "",
                        permission: "view cancelled orders",
                        subs: []
                    },
                    {
                        name: "Delivered",
                        link: "/orders/type/delivered",
                        icon: "",
                        permission: "view delivered orders",
                        subs: []
                    },
                    {
                        name: "Picking up",
                        link: "/orders/type/arrived",
                        icon: "",
                        permission: "view picking orders",
                        subs: []
                    },
                    {
                        name: "Picked Up",
                        link: "/orders/type/picked",
                        icon: "",
                        permission: "view picked orders",
                        subs: []
                    },
                    {
                        name: "Failed",
                        link: "/orders/type/failed",
                        icon: "",
                        permission: "view failed orders",
                        subs: []
                    },
                ],
            },
            {
                name: "Payouts",
                link: "/payouts",
                icon: 'fas fa-money-bill-wave-alt',
                permission: "view payouts",
                subs: [
                    {
                        name: "Pending",
                        link: "/payouts/type/pending",
                        icon: "",
                        permission: "view pending payouts",
                        subs: []
                    },
                    {
                        name: "Approved",
                        link: "/payouts/type/approved",
                        icon: "",
                        permission: "view approved payouts",
                        subs: []
                    },
                    {
                        name: "Declined",
                        link: "/payouts/type/declined",
                        icon: "",
                        permission: "view declined payouts",
                        subs: []
                    }
                ],
            },
            {
                name: "Ratings",
                link: "/ratings",
                icon: 'fas fa-star-half-alt',
                permission: "view ratings",
                subs: [],
            },
            {
                name: "Complaints",
                link: "/complaints",
                icon: 'fas fa-ticket-alt',
                permission: "view complaints",
                subs: [],
            }
        ],
    },
    {
        name: "Administration",
        permission: "view administration",
        children: [
            {
                name: "Roles",
                link: "/roles",
                icon: 'fas fa-server',
                permission: "view roles",
                subs: [],
            },
            {
                name: "Staff",
                link: "/admins",
                icon: 'fas fa-drum-steelpan',
                permission: "view administrators",
                subs: [],
            },
            {
                name: "Settings",
                link: "/settings",
                icon: 'fas fa-cogs',
                permission: "view settings",
                subs: [],
            }
        ]
    }
];
