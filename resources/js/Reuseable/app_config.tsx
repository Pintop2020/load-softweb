// @ts-ignore
import logoFile from '../../assets/media/logos/logo.png';
import iconFile from '../../assets/media/logos/icon.png';

const checkPermission = (user: any, checking: string) => {
    const permissions = user.roles[0].permissions;
    var response = false;
    if(user.roles[0].name == "super admin"){
        response = true;
    }else {
        for(var i=0; i<permissions.length;i++){
            if(permissions[i].name == checking) {
                response = true;
                break;
            }
        }
    }
    return response;
}

let site = "https://load.test";
let mainSite = "https://load.test";
let classLists = [
    "header-fixed",
    "header-tablet-and-mobile-fixed",
    "toolbar-enabled",
    "toolbar-fixed",
    "aside-enabled",
    "aside-fixed",
];

let bodyStyle = "--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px";

export {
    logoFile,
    iconFile,
    checkPermission,
    site, mainSite,
    classLists,
    bodyStyle,
};
