<?php

use App\Http\Controllers\BankController;
use App\Http\Controllers\CardController;
use App\Http\Controllers\ComplaintController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderPriceController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\VehicleTypeController;
use App\Http\Controllers\VerificationTokenController;
use App\Http\Controllers\PayoutController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=>'v1'], function(){
	Route::group(['prefix'=>'auth'], function(){
		Route::post('register', [UserController::class, 'store']);
		Route::patch('login', [UserController::class, 'login']);
		Route::patch('verify', [VerificationTokenController::class, 'verify']);
		Route::post('generate_token', [VerificationTokenController::class, 'store']);
		Route::patch('reset', [UserController::class, 'reset']);
		Route::patch('forget', [UserController::class, 'forget']);
	});

	Route::group(['middleware'=>'auth:sanctum', 'is_user'], function(){
        Route::get('/user', [UserController::class, 'profile']);
        Route::middleware('is_user_only')->post('/complete_reg', [UserController::class, 'completeUser']);
        Route::middleware('is_rider_only')->post('/dispatches/complete_reg', [UserController::class, 'completeRider']);
        // Route::group(['middleware'=>['is_complete']], function(){
            Route::group(['middleware'=>['is_user_only']], function(){
                Route::resource('orders', OrderController::class);
                Route::post('/order_payment', [OrderController::class, 'payNow']);
                Route::post('/order_confirmation', [OrderController::class, 'validateOrder']);
                Route::resource('ratings', RatingController::class)->only('store');
                Route::post('/initiate_payment_existing_card', [CardController::class, 'payNow']);
                Route::post('/payfrom_wallet', [WalletController::class, 'payNow']);
                Route::get('/favorites/{action}/{id}', [VehicleController::class, 'toggleFavorites']);
                Route::post('/report_truck', [ComplaintController::class, 'store']);
            });
            Route::group(['prefix'=>'dispatches', 'middleware'=>['is_rider_only', 'is_rider_guaranteed']], function(){
                Route::post('/set_location', [VehicleController::class, 'setLocation']);
                Route::post('/accept_order', [VehicleController::class, 'acceptOrder']);
                Route::post('/reject_order', [VehicleController::class, 'rejectOrder']);
                Route::put('/update_order_capacity/{id}', [OrderController::class, 'update']);
                Route::resource('vehicles', VehicleController::class)->only('store', 'update', 'destroy');
                Route::post('/set_availbility', [VehicleController::class, 'setAvailability']);
            });
            Route::put('/update_order/{id}', [OrderController::class, 'update']);
            Route::post('/payouts/request', [PayoutController::class, 'store']);
            Route::post('/negotiate_price', [OrderPriceController::class, 'negotiate']);
            Route::get('/accept_price/{id}', [OrderPriceController::class, 'acceptPrice']);
            Route::post('/change-password', [UserController::class, 'changePassword']);
            Route::post('/edit_profile', [UserController::class, 'editProfile']);
        // });
        Route::group(['prefix'=>'utils'], function(){
            Route::get('/vehicle_types', [VehicleTypeController::class, 'index']);
            Route::get('/system_config', [SettingController::class, 'index']);
            Route::get('/available_orders', [VehicleController::class, 'getClosestAvailableOrder']);
            Route::get('/available_trucks', [VehicleController::class, 'getClosestAvailableTrucks']);
            Route::get('/get_banks', BankController::class);
            Route::post('/bank_validation', [BankController::class, 'bankValidate']);
        });
	});
});
