<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ComplaintController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\GlobalMethods;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PayoutController;
use App\Http\Controllers\SmsController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\WalletController;
use App\Http\Controllers\RatingController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\VehicleTypeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['auth', 'verified', 'is_admin']], function(){
    Route::get('/', DashboardController::class)->name('dashboard');
    Route::get('/transactions/type/{type}', [TransactionController::class, 'single']);
    Route::get('/users/type/{type}', [UserController::class, 'single']);
    Route::get('/orders/type/{type}', [OrderController::class, 'single']);
    Route::get('/vehicles/type/{type}', [VehicleController::class, 'single']);
    Route::get('/payouts/type/{type}', [PayoutController::class, 'single']);
    Route::resource('users', UserController::class)->only('show');
    Route::get('/users/perm/{id}', [UserController::class, 'perm']);
    Route::resource('transactions', TransactionController::class)->only('show');
    Route::resource('payouts', PayoutController::class)->only('show');
    Route::post('/payouts/process', [PayoutController::class, 'processPayout']);
    Route::resource('orders', OrderController::class)->only('show');
    Route::resource('vehicles', VehicleController::class)->only('show');
    Route::resource('vehicle_types', VehicleTypeController::class);
    Route::get('/notifications', [NotificationController::class, 'index']);
    Route::post('/send_notifications', [NotificationController::class, 'store']);
    Route::post('/send_notification_single', [NotificationController::class, 'storeSingle']);
    Route::get('/sms', SmsController::class);
    Route::post('/send_sms', [SmsController::class, 'sendSms']);
    Route::post('/update_wallet', [WalletController::class, 'updateWallet']);
    Route::resource('coupons', CouponController::class);
    Route::get('/guarantors/confirm/{id}', [UserController::class, 'confirmGuarantor']);
    Route::resource('ratings', RatingController::class)->only('index', 'show', 'destroy');
    Route::resource('roles', RoleController::class)->except('show');
    Route::resource('admins', AdminController::class);
    Route::get('/profile', [AdminController::class, 'profile']);
    Route::post('/profile/update', [AdminController::class, 'profileUpdate']);
    Route::get('/settings', [SettingController::class, 'index']);
    Route::post('/settings/update', [SettingController::class, 'update']);
    Route::post('/vehicles/process/validate', [VehicleController::class, 'validateRide']);
    Route::get('/vehicles/blaclist/{action}/{id}', [VehicleController::class, 'toggleBlacklist']);
    Route::get('/complaints', [ComplaintController::class, 'index']);
});
Route::get('/test', [GlobalMethods::class, 'testing']);
